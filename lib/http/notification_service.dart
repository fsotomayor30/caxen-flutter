import 'package:app/interceptors/notification_interceptor_dio.dart';
import 'package:app/modelos/notification_request.dart';
import 'package:app/modelos/notification_response.dart';
import 'package:dio/dio.dart';

class NotificationService {
  final Dio dio = new Dio();

  NotificationService() {
    this.dio.interceptors.add(NotificationInterceptorDio());
  }

  Future<NotificationResponse> sendNotification(
      String to, String title, String body) async {
    NotificationRequest notificationRequest = NotificationRequest();
    notificationRequest.to = to;

    Notification notification = Notification();
    notification.title = title;
    notification.body = body;

    notificationRequest.notification = notification;

    Data data = Data();
    data.clickAction = 'FLUTTER_NOTIFICATION_CLICK';
    data.code = "10";
    data.title = title;
    data.body = body;

    notificationRequest.data = data;

    final Response response = await dio.post(
        'https://fcm.googleapis.com/fcm/send',
        data: notificationRequest.toJson());
    if (response.statusCode != 200) {
      throw Exception('Unexpected HTTP code : ${response.statusCode}');
    }
    return NotificationResponse.fromJson(response.data);
  }
}
