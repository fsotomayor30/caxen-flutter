import 'package:app/modulos/splash/screens/splash_screen.dart';
import 'package:app/servicios/local_push_notification_module.dart';
import 'package:app/servicios/notification_master_module.dart';
import 'package:app/servicios/push_notification_module.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/key_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MaterialApp(
      home: new MyApp(),
    ));
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final pushNotificationModule = new PushNotificationModule();

  @override
  void initState() {
    super.initState();
    LocalPushNotificationModule().initLocalPush();
    pushNotificationModule.initNotifications();
    listenPushNotification();
  }

  @override
  Widget build(BuildContext context) {
    Get.put(GlobalController());
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        navigatorKey: KeyApp.keyAppNavigator,
        home: SplashScreen());
  }

  listenPushNotification() {
    pushNotificationModule.mensajes.listen((args) async {
      print('===================== ${args[0]['listenTo']} ===================');
      NotificationMasterModule().notificationEvents(args);
    });
  }
}
