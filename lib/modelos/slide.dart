import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Slide {
  final Column child;
  Slide({@required this.child});
}

final slideList = [
  Slide(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          "EMPRENDIMIENTOS",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
        AutoSizeText(
          "FREELANCERS",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
        AutoSizeText(
          "OFICIOS",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
      ],
    ),
  ),
  Slide(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          "¿Buscas comprar Productos",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
        AutoSizeText(
          "o",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
        AutoSizeText(
          "Servicios GENIALES?",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
      ],
    ),
  ),
  Slide(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AutoSizeText(
          "Encuéntralos",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
        AutoSizeText(
          "donde TU estás",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 40,
              color: Colors.white,
              fontFamily: 'BalooDa2-Medium',
              height: 1.0),
          maxLines: 1,
        ),
      ],
    ),
  )
];
