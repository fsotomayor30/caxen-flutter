class Oficio {
  String firstName;
  String lastName;
  String region;
  String category;
  String mail;
  String date;
  String experience;
  String foto1;
  String phone;
  bool certification;
  String uuid;
  String source;
  String descripcion;
  String urlInstagram;

  //DATOS USUARIO
  String imagePerfil;
  String name;
  String email;

  bool visible;
  String address;
  double latitude;
  double longitude;

  Oficio({
    this.firstName,
    this.lastName,
    this.region,
    this.category,
    this.email,
    this.date,
    this.experience,
    this.foto1,
    this.phone,
    this.source,
    this.certification,
    this.imagePerfil,
    this.name,
    this.mail,
    this.uuid,
    this.visible,
    this.descripcion,
    this.address,
    this.latitude,
    this.longitude,
    this.urlInstagram,
  });

  Oficio.fromSnapshot(dynamic element)
      : firstName = element["firstName"],
        lastName = element["lastName"],
        email = element["email"],
        region = element["region"],
        category = element["category"],
        date = element["date"],
        experience = element["experience"],
        name = element["name"],
        source = element["source"],
        foto1 = element["foto1"],
        phone = element["phone"],
        certification = element["certification"],
        imagePerfil = element["imagePerfil"],
        mail = element["mail"],
        uuid = element["uuid"],
        visible = element["visible"],
        descripcion = element["descripcion"],
        urlInstagram =
            element["urlInstagram"] == null ? null : element["urlInstagram"],
        address = element["address"] == null ? null : element["address"],
        latitude =
            element["latitude"] == null ? null : element["latitude"].toDouble(),
        longitude = element["longitude"] == null
            ? null
            : element["longitude"].toDouble();

  @override
  String toString() {
    return 'Oficio{firstName: $firstName, lastName: $lastName, region: $region, category: $category, mail: $mail, date: $date, experience: $experience, foto1: $foto1, phone: $phone, certification: $certification, uuid: $uuid, source: $source, descripcion: $descripcion, imagePerfil: $imagePerfil, name: $name, email: $email, visible: $visible, address: $address, latitude: $latitude, longitude: $longitude}';
  }
}
