class ComentarioEmprendimiento {
  final String comment;
  final String email;
  final String entrepreneurshipid;
  final String imagePerfil;
  final String name;
  final String uuid;
  final DateTime date;

  ComentarioEmprendimiento(this.comment, this.email, this.imagePerfil,
      this.entrepreneurshipid, this.name, this.uuid, this.date);

  ComentarioEmprendimiento.fromSnapshot(dynamic element)
      : comment = element["comment"],
        email = element["email"],
        imagePerfil = element["imagePerfil"],
        entrepreneurshipid = element["entrepreneurshipid"],
        name = element["name"],
        uuid = element["uuid"],
        date = DateTime.parse(element["date"]);

  @override
  String toString() {
    return 'ComentarioEmprendimiento{comment: $comment, email: $email, entrepreneurshipid: $entrepreneurshipid, imagePerfil: $imagePerfil, name: $name, uuid: $uuid, date: $date}';
  }
}
