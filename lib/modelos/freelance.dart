class Freelance {
  String category;
  String date;
  String description;
  String email;
  String imagePerfil;
  String mail;
  String name;
  String nameFreelance;
  String phone;
  String source;
  String urlFb;
  String urlInstagram;
  String urlLinkedin;
  String uuid;
  String webSite;
  String region;
  String foto1;
  String foto2;
  String foto3;
  bool visible;
  String address;
  double latitude;
  double longitude;

  Freelance(
      {this.category,
      this.date,
      this.description,
      this.email,
      this.imagePerfil,
      this.mail,
      this.name,
      this.nameFreelance,
      this.phone,
      this.source,
      this.urlFb,
      this.urlInstagram,
      this.urlLinkedin,
      this.uuid,
      this.webSite,
      this.region,
      this.foto1,
      this.foto2,
      this.foto3,
      this.visible,
      this.address,
      this.latitude,
      this.longitude});

  Freelance.fromSnapshot(dynamic element)
      : category = element["category"],
        date = element["date"],
        description = element["description"],
        email = element["email"],
        imagePerfil = element["imagePerfil"],
        mail = element["mail"],
        name = element["name"],
        nameFreelance = element["nameFreelance"],
        phone = element["phone"],
        source = element["source"],
        urlFb = element["urlFb"],
        urlInstagram = element["urlInstagram"],
        urlLinkedin = element["urlLinkedin"],
        uuid = element["uuid"],
        region = element["region"],
        foto1 = element["foto1"],
        foto2 = element["foto2"],
        foto3 = element["foto3"],
        webSite = element["webSite"],
        visible = element["visible"],
        address = element["address"] == null ? null : element["address"],
        latitude =
            element["latitude"] == null ? null : element["latitude"].toDouble(),
        longitude = element["longitude"] == null
            ? null
            : element["longitude"].toDouble();

  @override
  String toString() {
    return 'Freelance{category: $category, date: $date, description: $description, email: $email, imagePerfil: $imagePerfil, mail: $mail, name: $name, nameFreelance: $nameFreelance, phone: $phone, source: $source, urlFb: $urlFb, urlInstagram: $urlInstagram, urlLinkedin: $urlLinkedin, uuid: $uuid, webSite: $webSite, region: $region, foto1: $foto1, foto2: $foto2, foto3: $foto3, visible: $visible, address: $address, latitude: $latitude, longitude: $longitude}';
  }
}
