class BotOficio {
  final String text;
  final String uuid;

  BotOficio(this.text, this.uuid);

  BotOficio.fromSnapshot(dynamic element)
      : text = element["text"],
        uuid = element["uuid"];

  @override
  String toString() {
    return 'BotOficio{text: $text, uuid: $uuid}';
  }
}
