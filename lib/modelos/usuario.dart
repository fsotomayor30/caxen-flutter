class Usuario {
  String uid;
  String displayName;
  String email;
  String photoProfile;
  String tokenRing;

  Usuario(
      {this.uid,
      this.displayName,
      this.email,
      this.photoProfile,
      this.tokenRing});

  Usuario.fromSnapshot(dynamic element)
      : uid = element["uuid"],
        displayName = element["displayName"],
        email = element["email"],
        photoProfile = element["photoProfile"],
        tokenRing = element["tokenRing"] == null ? null : element["tokenRing"];

  @override
  String toString() {
    return 'Usuario{uid: $uid, displayName: $displayName, email: $email, photoProfile: $photoProfile, tokenRing: $tokenRing}';
  }
}
