class Emprendimiento {
  String category;
  String date;
  String description;
  String email;
  String imagePerfil;
  String mail;
  String name;
  String nameEntrepreneurship;
  String phone;
  String source;
  String urlFb;
  String urlInstagram;
  String uuid;
  String webSite;
  String region;
  String foto1;
  String foto2;
  String foto3;
  bool visible;
  String address;
  double latitude;
  double longitude;

  Emprendimiento(
      {this.category,
      this.date,
      this.description,
      this.email,
      this.imagePerfil,
      this.mail,
      this.name,
      this.nameEntrepreneurship,
      this.phone,
      this.source,
      this.urlFb,
      this.urlInstagram,
      this.uuid,
      this.webSite,
      this.region,
      this.foto1,
      this.foto2,
      this.foto3,
      this.visible,
      this.address,
      this.latitude,
      this.longitude});

  Emprendimiento.fromSnapshot(dynamic element)
      : category = element["category"],
        date = element["date"],
        description = element["description"],
        email = element["email"],
        imagePerfil = element["imagePerfil"],
        mail = element["mail"],
        name = element["name"],
        nameEntrepreneurship = element["nameEntrepreneurship"],
        phone = element["phone"],
        source = element["source"],
        urlFb = element["urlFb"],
        urlInstagram = element["urlInstagram"],
        uuid = element["uuid"],
        region = element["region"],
        foto1 = element["foto1"],
        foto2 = element["foto2"],
        foto3 = element["foto3"],
        webSite = element["webSite"],
        visible = element["visible"],
        address = element["address"] == null ? null : element["address"],
        latitude =
            element["latitude"] == null ? null : element["latitude"].toDouble(),
        longitude = element["longitude"] == null
            ? null
            : element["longitude"].toDouble();

  @override
  String toString() {
    return 'Emprendimiento{category: $category, date: $date, description: $description, email: $email, imagePerfil: $imagePerfil, mail: $mail, name: $name, nameEntrepreneurship: $nameEntrepreneurship, phone: $phone, source: $source, urlFb: $urlFb, urlInstagram: $urlInstagram, uuid: $uuid, webSite: $webSite, region: $region, foto1: $foto1, foto2: $foto2, foto3: $foto3, visible: $visible, latitude: $latitude, longitude: $longitude}';
  }
}
