class ComentarioOficio {
  final String comment;
  final String email;
  final String oficioid;
  final String imagePerfil;
  final String name;
  final String uuid;
  final DateTime date;

  ComentarioOficio(this.comment, this.email, this.imagePerfil, this.oficioid,
      this.name, this.uuid, this.date);

  ComentarioOficio.fromSnapshot(dynamic element)
      : comment = element["comment"],
        email = element["email"],
        imagePerfil = element["imagePerfil"],
        oficioid = element["oficioid"],
        name = element["name"],
        uuid = element["uuid"],
        date = DateTime.parse(element["date"]);

  @override
  String toString() {
    return 'ComentarioOficio{comment: $comment, email: $email, oficioid: $oficioid, imagePerfil: $imagePerfil, name: $name, uuid: $uuid, date: $date}';
  }
}
