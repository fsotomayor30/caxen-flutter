class BotEmprendimiento {
  final String text;
  final String uuid;

  BotEmprendimiento(this.text, this.uuid);

  BotEmprendimiento.fromSnapshot(dynamic element)
      : text = element["text"],
        uuid = element["uuid"];

  @override
  String toString() {
    return 'BotEmprendimiento{text: $text, uuid: $uuid}';
  }
}
