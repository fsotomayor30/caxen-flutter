class TerminosYCondiciones {
  final String email;
  final String date;

  const TerminosYCondiciones({this.email, this.date});

  @override
  String toString() {
    return 'TerminosYCondiciones{email: $email, date: $date}';
  }

  TerminosYCondiciones.fromSnapshot(dynamic element)
      : email = element["email"],
        date = element["date"];
}
