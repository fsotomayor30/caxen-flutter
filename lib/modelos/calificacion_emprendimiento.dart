class CalificacionEmprendimiento {
  final String qualify;
  final String entrepreneurshipid;
  final String imagePerfil;
  final String name;
  final String uuid;
  final DateTime date;
  final String email;

  CalificacionEmprendimiento(this.qualify, this.imagePerfil,
      this.entrepreneurshipid, this.name, this.uuid, this.date, this.email);

  CalificacionEmprendimiento.fromSnapshot(dynamic element)
      : qualify = element["qualify"],
        imagePerfil = element["imagePerfil"],
        entrepreneurshipid = element["entrepreneurshipid"],
        name = element["name"],
        uuid = element["uuid"],
        email = element["email"],
        date = DateTime.parse(element["date"]);

  @override
  String toString() {
    return 'CalificacionEmprendimiento{qualify: $qualify, entrepreneurshipid: $entrepreneurshipid, imagePerfil: $imagePerfil, name: $name, uuid: $uuid, date: $date}';
  }
}
