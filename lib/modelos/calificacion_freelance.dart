class CalificacionFreelance {
  final String qualify;
  final String freelanceid;
  final String imagePerfil;
  final String name;
  final String uuid;
  final DateTime date;
  final String email;

  CalificacionFreelance(this.qualify, this.imagePerfil, this.freelanceid,
      this.name, this.uuid, this.date, this.email);

  CalificacionFreelance.fromSnapshot(dynamic element)
      : qualify = element["qualify"],
        imagePerfil = element["imagePerfil"],
        freelanceid = element["freelanceid"],
        name = element["name"],
        uuid = element["uuid"],
        email = element["email"],
        date = DateTime.parse(element["date"]);

  @override
  String toString() {
    return 'CalificacionEmprendimiento{qualify: $qualify, freelanceid: $freelanceid, imagePerfil: $imagePerfil, name: $name, uuid: $uuid, date: $date}';
  }
}
