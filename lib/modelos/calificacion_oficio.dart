class CalificacionOficio {
  final String qualify;
  final String oficioid;
  final String imagePerfil;
  final String name;
  final String uuid;
  final DateTime date;
  final String email;

  CalificacionOficio(this.qualify, this.imagePerfil, this.oficioid, this.name,
      this.uuid, this.date, this.email);

  CalificacionOficio.fromSnapshot(dynamic element)
      : qualify = element["qualify"],
        imagePerfil = element["imagePerfil"],
        oficioid = element["oficioid"],
        name = element["name"],
        uuid = element["uuid"],
        email = element["email"],
        date = DateTime.parse(element["date"]);

  @override
  String toString() {
    return 'CalificacionOficio{qualify: $qualify, oficioid: $oficioid, imagePerfil: $imagePerfil, name: $name, uuid: $uuid, date: $date}';
  }
}
