import 'dart:convert';

NotificationRequest notificationRequestFromJson(String str) =>
    NotificationRequest.fromJson(json.decode(str));

String notificationRequestToJson(NotificationRequest data) =>
    json.encode(data.toJson());

class NotificationRequest {
  NotificationRequest({
    this.to,
    this.notification,
    this.data,
  });

  String to;
  Notification notification;
  Data data;

  factory NotificationRequest.fromJson(Map<String, dynamic> json) =>
      NotificationRequest(
        to: json["to"],
        notification: Notification.fromJson(json["notification"]),
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "to": to,
        "notification": notification.toJson(),
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.clickAction,
    this.code,
    this.title,
    this.body,
  });

  String clickAction;
  String code;
  String title;
  String body;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        clickAction: json["click_action"],
        code: json["code"],
        title: json["title"],
        body: json["body"],
      );

  Map<String, dynamic> toJson() => {
        "click_action": clickAction,
        "code": code,
        "title": title,
        "body": body,
      };
}

class Notification {
  Notification({
    this.title,
    this.body,
  });

  String title;
  String body;

  factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        title: json["title"],
        body: json["body"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "body": body,
      };
}
