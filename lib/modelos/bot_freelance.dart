class BotFreelance {
  final String text;
  final String uuid;

  BotFreelance(this.text, this.uuid);

  BotFreelance.fromSnapshot(dynamic element)
      : text = element["text"],
        uuid = element["uuid"];

  @override
  String toString() {
    return 'BotFreelance{text: $text, uuid: $uuid}';
  }
}
