class ComentarioFreelance {
  final String comment;
  final String email;
  final String freelanceid;
  final String imagePerfil;
  final String name;
  final String uuid;
  final DateTime date;

  ComentarioFreelance(this.comment, this.email, this.imagePerfil,
      this.freelanceid, this.name, this.uuid, this.date);

  ComentarioFreelance.fromSnapshot(dynamic element)
      : comment = element["comment"],
        email = element["email"],
        imagePerfil = element["imagePerfil"],
        freelanceid = element["freelanceid"],
        name = element["name"],
        uuid = element["uuid"],
        date = DateTime.parse(element["date"]);

  @override
  String toString() {
    return 'ComentarioFreelance{comment: $comment, email: $email, freelanceid: $freelanceid, imagePerfil: $imagePerfil, name: $name, uuid: $uuid, date: $date}';
  }
}
