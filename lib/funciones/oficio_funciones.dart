import 'package:app/funciones/calificacion_oficio_funciones.dart';
import 'package:app/funciones/comentario_oficio_funciones.dart';
import 'package:app/funciones/like_oficio_funciones.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/servicios/firebase_storage_service.dart';
import 'package:firebase_database/firebase_database.dart';

class OficiosFunciones {
  final dbRefOficios = FirebaseDatabase.instance.reference().child("oficio");
  final dbRefOficiosVistas =
      FirebaseDatabase.instance.reference().child("oficioView");

  Future<void> agregarVista() async {
    var id = dbRefOficiosVistas.push().key;
    await dbRefOficiosVistas.child(id).set({
      "date": DateTime.now().toString(),
      "uid": id,
    });
  }

  Future<void> eliminar(Oficio oficio) async {
    ComentarioOficioFunciones comentarioOficioFunciones =
        new ComentarioOficioFunciones();
    LikeOficioFunciones likeOficioFunciones = new LikeOficioFunciones();
    CalificacionOficiosFunciones calificacionOficiosFunciones =
        new CalificacionOficiosFunciones();
    FirebaseStorageService firebaseStorageService =
        new FirebaseStorageService();

    await comentarioOficioFunciones.eliminar(oficio.uuid);
    await likeOficioFunciones.eliminar(oficio.uuid);
    await calificacionOficiosFunciones.eliminar(oficio.uuid);

    if (oficio.foto1 != null) {
      await firebaseStorageService.deleteFile(
          "oficioImages/" + oficio.email + "/" + oficio.uuid + "/caxen_foto1");
    }

    await dbRefOficios.child(oficio.uuid).set(null);
  }

  Future<int> obtenerVistasOficios() async {
    int cantidad;
    await dbRefOficiosVistas.once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value != null) {
        cantidad = values.values.toList().length;
      } else {
        cantidad = 0;
      }
    });

    return cantidad;
  }
}
