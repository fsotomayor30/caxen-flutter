import 'package:app/modelos/calificacion_emprendimiento.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class CalificacionEmprendimientosFunciones {
  final dbRef =
      FirebaseDatabase.instance.reference().child("entrepreneurshipQualify");

  Future<bool> calificar(
      String idEmprendimiento, Usuario usuario, int calificacion) async {
    bool respuesta;
    await dbRef.child(idEmprendimiento).push().set({
      'email': usuario.email,
      'qualify': calificacion.toString(),
      'uuid': idEmprendimiento
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<void> eliminar(String idEmprendimiento) async {
    await dbRef.child(idEmprendimiento).set(null);
  }

  Future<int> ultimaCalificacion(
      Emprendimiento emprendimiento, Usuario usuario) async {
    int dias;
    await dbRef
        .child(emprendimiento.uuid)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        dias = -1;
      } else {
        List<dynamic> list = values.values.toList();
        List<CalificacionEmprendimiento> listCalificaciones =
            <CalificacionEmprendimiento>[];
        list.forEach((element) {
          listCalificaciones
              .add(CalificacionEmprendimiento.fromSnapshot(element));
        });
        listCalificaciones = listCalificaciones
            .where((element) => element.email == usuario.email)
            .toList();
        listCalificaciones.sort((a, b) {
          return b.date.compareTo(a.date);
        });

        if (listCalificaciones.length == 0) {
          dias = -1;
        } else {
          listCalificaciones.sort((a, b) {
            return b.date.compareTo(a.date);
          });

          DateTime dateTime = DateTime.now();
          dias = dateTime.difference(listCalificaciones.last.date).inDays;
        }
      }
    });
    return dias;
  }
}
