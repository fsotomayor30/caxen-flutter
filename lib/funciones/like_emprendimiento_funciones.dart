import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class LikeEmprendimientoFunciones {
  final dbRefLike =
      FirebaseDatabase.instance.reference().child("entrepreneurshipLike");

  Future<int> cantidadLike(String idEmprendimientos) async {
    int likes;
    await dbRefLike
        .child(idEmprendimientos)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        likes = 0;
      } else {
        List<dynamic> list = values.values.toList();
        likes = list.length;
      }
    });
    return likes;
  }

  Future<bool> agregarLike(String idEmprendimiento, Usuario usuario) async {
    bool respuesta;
    await dbRefLike
        .child(idEmprendimiento)
        .child(usuario.email.toString().replaceAll(".", "-"))
        .set({
      'uuid': idEmprendimiento,
      'email': usuario.email,
      'entrepreneurshipid': idEmprendimiento
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<bool> quitarLike(String idEmprendimiento, Usuario usuario) async {
    bool respuesta;
    await dbRefLike
        .child(idEmprendimiento)
        .child(usuario.email.toString().replaceAll(".", "-"))
        .set(null)
        .then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<int> conLike(Usuario usuario, String idEmprendimiento) async {
    int respuesta;

    await dbRefLike
        .child(idEmprendimiento)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (values == null) {
        respuesta = -1;
      } else {
        respuesta = values.values
            .toList()
            .where((element) => element['email'] == usuario.email)
            .toList()
            .length;
      }
    });
    return respuesta;
  }

  Future<void> eliminar(String idEmprendimiento) async {
    await dbRefLike.child(idEmprendimiento).set(null);
  }
}
