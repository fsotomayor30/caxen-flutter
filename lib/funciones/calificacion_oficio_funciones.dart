import 'package:app/modelos/calificacion_oficio.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class CalificacionOficiosFunciones {
  final dbRef = FirebaseDatabase.instance.reference().child("oficiosQualify");

  Future<bool> calificar(
      String idOficio, Usuario usuario, int calificacion) async {
    bool respuesta;
    await dbRef.child(idOficio).push().set({
      'email': usuario.email,
      'qualify': calificacion.toString(),
      'uuid': idOficio
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<void> eliminar(String idOficio) async {
    await dbRef.child(idOficio).set(null);
  }

  Future<int> ultimaCalificacion(Oficio oficio, Usuario usuario) async {
    int dias;
    await dbRef.child(oficio.uuid).once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;

      if (dataSnapshot.value == null) {
        dias = -1;
      } else {
        List<dynamic> list = values.values.toList();
        List<CalificacionOficio> listCalificaciones = <CalificacionOficio>[];
        list.forEach((element) {
          listCalificaciones.add(CalificacionOficio.fromSnapshot(element));
        });
        listCalificaciones = listCalificaciones
            .where((element) => element.email == usuario.email)
            .toList();
        listCalificaciones.sort((a, b) {
          return b.date.compareTo(a.date);
        });

        if (listCalificaciones.length == 0) {
          dias = -1;
        } else {
          listCalificaciones.sort((a, b) {
            return b.date.compareTo(a.date);
          });

          DateTime dateTime = DateTime.now();
          dias = dateTime.difference(listCalificaciones.last.date).inDays;
        }
      }
    });
    return dias;
  }
}
