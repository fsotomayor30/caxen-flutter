import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class LikeFreelanceFunciones {
  final dbRefLike =
      FirebaseDatabase.instance.reference().child("freelanceLike");

  Future<int> cantidadLike(String idFreelances) async {
    int likes;
    await dbRefLike
        .child(idFreelances)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        likes = 0;
      } else {
        List<dynamic> list = values.values.toList();
        likes = list.length;
      }
    });
    return likes;
  }

  Future<bool> agregarLike(String idFreelance, Usuario usuario) async {
    bool respuesta;
    await dbRefLike
        .child(idFreelance)
        .child(usuario.email.toString().replaceAll(".", "-"))
        .set({
      'uuid': idFreelance,
      'email': usuario.email,
      'freelanceid': idFreelance
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<bool> quitarLike(String idFreelance, Usuario usuario) async {
    bool respuesta;
    await dbRefLike
        .child(idFreelance)
        .child(usuario.email.toString().replaceAll(".", "-"))
        .set(null)
        .then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<int> conLike(Usuario usuario, String idFreelance) async {
    int respuesta;

    await dbRefLike.child(idFreelance).once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (values == null) {
        respuesta = -1;
      } else {
        respuesta = values.values
            .toList()
            .where((element) => element['email'] == usuario.email)
            .toList()
            .length;
      }
    });
    return respuesta;
  }

  Future<void> eliminar(String idFreelance) async {
    await dbRefLike.child(idFreelance).set(null);
  }
}
