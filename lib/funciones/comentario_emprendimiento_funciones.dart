import 'package:app/modelos/comentario_emprendimiento.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class ComentarioEmprendimientoFunciones {
  final dbRefComentario =
      FirebaseDatabase.instance.reference().child("entrepreneurshipComment");

  Future<void> eliminar(String idEmprendimiento) async {
    await dbRefComentario.child(idEmprendimiento).set(null);
  }

  Future<int> ultimoCommentario(
      Emprendimiento emprendimiento, Usuario usuario) async {
    int dias;
    await dbRefComentario
        .child(emprendimiento.uuid)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        dias = -1;
      } else {
        List<dynamic> list = values.values.toList();
        List<ComentarioEmprendimiento> listComentarios =
            <ComentarioEmprendimiento>[];
        list.forEach((element) {
          listComentarios.add(ComentarioEmprendimiento.fromSnapshot(element));
        });
        listComentarios = listComentarios
            .where((element) => element.email == usuario.email)
            .toList();
        listComentarios.sort((a, b) {
          return b.date.compareTo(a.date);
        });

        if (listComentarios.length == 0) {
          dias = -1;
        } else {
          listComentarios.sort((a, b) {
            return b.date.compareTo(a.date);
          });

          DateTime dateTime = DateTime.now();
          dias = dateTime.difference(listComentarios.last.date).inDays;
        }
      }
    });
    return dias;
  }
}
