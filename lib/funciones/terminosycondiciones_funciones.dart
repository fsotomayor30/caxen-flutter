import 'package:app/modelos/terminosycondiciones.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class TerminosYCondicionesFunciones {
  final dbRefTerminosYCondiciones =
      FirebaseDatabase.instance.reference().child("terminosYCondiciones");

  Future<List<TerminosYCondiciones>> obtenerTodos(String idFreelance) async {
    List<TerminosYCondiciones> terminosYCondicionesList =
        <TerminosYCondiciones>[];
    await dbRefTerminosYCondiciones.once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        List<dynamic> list = values.values.toList();

        list.forEach((element) => {
              terminosYCondicionesList
                  .add(TerminosYCondiciones.fromSnapshot(element))
            });
      }
    });
    return terminosYCondicionesList;
  }

  Future<TerminosYCondiciones> obtenerPorEmail(String email) async {
    TerminosYCondiciones terminosYCondiciones = new TerminosYCondiciones();

    await dbRefTerminosYCondiciones
        .orderByChild("email")
        .equalTo(email)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value != null) {
        terminosYCondiciones =
            TerminosYCondiciones.fromSnapshot(values.values.toList()[0]);
      } else {
        terminosYCondiciones = null;
      }
    });
    return terminosYCondiciones;
  }

  Future<bool> agregarTerminosYCondiciones(Usuario usuario) async {
    bool respuesta;
    var id = dbRefTerminosYCondiciones.push().key;

    await dbRefTerminosYCondiciones.child(id).set({
      'email': usuario.email,
      'date': new DateTime.now().toLocal().toString()
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }
}
