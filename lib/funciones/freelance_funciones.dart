import 'package:app/funciones/calificacion_freelance_funciones.dart';
import 'package:app/funciones/comentario_freelance_funciones.dart';
import 'package:app/funciones/like_freelance_funciones.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/servicios/firebase_storage_service.dart';
import 'package:firebase_database/firebase_database.dart';

class FreelancesFunciones {
  final dbRefFreelances =
      FirebaseDatabase.instance.reference().child("freelance");
  final dbRefFreelancesVistas =
      FirebaseDatabase.instance.reference().child("freelanceView");

  Future<void> agregarVista() async {
    var id = dbRefFreelancesVistas.push().key;
    await dbRefFreelancesVistas.child(id).set({
      "date": DateTime.now().toString(),
      "uid": id,
    });
  }

  Future<void> eliminar(Freelance freelance) async {
    ComentarioFreelanceFunciones comentarioFreelanceFunciones =
        new ComentarioFreelanceFunciones();
    LikeFreelanceFunciones likeFreelanceFunciones =
        new LikeFreelanceFunciones();
    CalificacionFreelanceFunciones calificacionFreelanceFunciones =
        new CalificacionFreelanceFunciones();
    FirebaseStorageService firebaseStorageService =
        new FirebaseStorageService();

    await comentarioFreelanceFunciones.eliminar(freelance.uuid);
    await likeFreelanceFunciones.eliminar(freelance.uuid);
    await calificacionFreelanceFunciones.eliminar(freelance.uuid);
    await firebaseStorageService.deleteFile("freelanceImages/" +
        freelance.email +
        "/" +
        freelance.uuid +
        "/caxen_foto1");
    await firebaseStorageService.deleteFile("freelanceImages/" +
        freelance.email +
        "/" +
        freelance.uuid +
        "/caxen_foto2");
    await firebaseStorageService.deleteFile("freelanceImages/" +
        freelance.email +
        "/" +
        freelance.uuid +
        "/caxen_foto3");

    await dbRefFreelances.child(freelance.uuid).set(null);
  }

  Future<int> obtenerVistasFreelances() async {
    int cantidad;
    await dbRefFreelancesVistas.once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value != null) {
        cantidad = values.values.toList().length;
      } else {
        cantidad = 0;
      }
    });

    return cantidad;
  }
}
