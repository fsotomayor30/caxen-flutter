import 'package:app/modelos/comentario_oficio.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class ComentarioOficioFunciones {
  final dbRefComentario =
      FirebaseDatabase.instance.reference().child("oficioComment");

  Future<void> eliminar(String idOficio) async {
    await dbRefComentario.child(idOficio).set(null);
  }

  Future<int> ultimoCommentario(Oficio oficio, Usuario usuario) async {
    int dias;
    await dbRefComentario
        .child(oficio.uuid)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        dias = -1;
      } else {
        List<dynamic> list = values.values.toList();
        List<ComentarioOficio> listComentarios = <ComentarioOficio>[];
        list.forEach((element) {
          listComentarios.add(ComentarioOficio.fromSnapshot(element));
        });
        listComentarios = listComentarios
            .where((element) => element.email == usuario.email)
            .toList();
        listComentarios.sort((a, b) {
          return b.date.compareTo(a.date);
        });

        if (listComentarios.length == 0) {
          dias = -1;
        } else {
          listComentarios.sort((a, b) {
            return b.date.compareTo(a.date);
          });

          DateTime dateTime = DateTime.now();
          dias = dateTime.difference(listComentarios.last.date).inDays;
        }
      }
    });
    return dias;
  }
}
