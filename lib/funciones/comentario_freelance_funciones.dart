import 'package:app/modelos/comentario_freelance.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class ComentarioFreelanceFunciones {
  final dbRefComentario =
      FirebaseDatabase.instance.reference().child("freelanceComment");

  Future<void> eliminar(String idFreelance) async {
    await dbRefComentario.child(idFreelance).set(null);
  }

  Future<int> ultimoCommentario(Freelance freelance, Usuario usuario) async {
    int dias;
    await dbRefComentario
        .child(freelance.uuid)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        dias = -1;
      } else {
        List<dynamic> list = values.values.toList();
        List<ComentarioFreelance> listComentarios = <ComentarioFreelance>[];
        list.forEach((element) {
          listComentarios.add(ComentarioFreelance.fromSnapshot(element));
        });
        listComentarios = listComentarios
            .where((element) => element.email == usuario.email)
            .toList();

        if (listComentarios.length == 0) {
          dias = -1;
        } else {
          listComentarios.sort((a, b) {
            return b.date.compareTo(a.date);
          });

          DateTime dateTime = DateTime.now();
          dias = dateTime.difference(listComentarios.last.date).inDays;
        }
      }
    });
    return dias;
  }
}
