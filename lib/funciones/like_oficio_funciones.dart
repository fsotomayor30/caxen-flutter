import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class LikeOficioFunciones {
  final dbRefLike = FirebaseDatabase.instance.reference().child("oficioLike");

  Future<int> cantidadLike(String idOficio) async {
    int likes;
    await dbRefLike.child(idOficio).once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        likes = 0;
      } else {
        List<dynamic> list = values.values.toList();
        likes = list.length;
      }
    });
    return likes;
  }

  Future<bool> agregarLike(String idOficio, Usuario usuario) async {
    bool respuesta;
    await dbRefLike
        .child(idOficio)
        .child(usuario.email.toString().replaceAll(".", "-"))
        .set({
      'uuid': idOficio,
      'email': usuario.email,
      'oficioid': idOficio
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<bool> quitarLike(String idOficio, Usuario usuario) async {
    bool respuesta;
    await dbRefLike
        .child(idOficio)
        .child(usuario.email.toString().replaceAll(".", "-"))
        .set(null)
        .then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<int> conLike(Usuario usuario, String idOficio) async {
    int respuesta;

    await dbRefLike.child(idOficio).once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (values == null) {
        respuesta = -1;
      } else {
        respuesta = values.values
            .toList()
            .where((element) => element['email'] == usuario.email)
            .toList()
            .length;
      }
    });
    return respuesta;
  }

  Future<void> eliminar(String idOficio) async {
    await dbRefLike.child(idOficio).set(null);
  }
}
