import 'package:app/funciones/calificacion_emprendimiento_funciones.dart';
import 'package:app/funciones/comentario_emprendimiento_funciones.dart';
import 'package:app/funciones/like_emprendimiento_funciones.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/servicios/firebase_storage_service.dart';
import 'package:firebase_database/firebase_database.dart';

class EmprendimientosFunciones {
  final dbRefEmprendimientos =
      FirebaseDatabase.instance.reference().child("entrepreneurship");
  final dbRefEmprendimientosVistas =
      FirebaseDatabase.instance.reference().child("entrepreneurshipView");

  Future<void> agregarVista() async {
    var id = dbRefEmprendimientosVistas.push().key;
    await dbRefEmprendimientosVistas.child(id).set({
      "date": DateTime.now().toString(),
      "uid": id,
    });
  }

  Future<void> eliminar(Emprendimiento emprendimiento) async {
    ComentarioEmprendimientoFunciones comentarioEmprendimientoFunciones =
        new ComentarioEmprendimientoFunciones();
    LikeEmprendimientoFunciones likeEmprendimientoFunciones =
        new LikeEmprendimientoFunciones();
    CalificacionEmprendimientosFunciones calificacionEmprendimientosFunciones =
        new CalificacionEmprendimientosFunciones();
    FirebaseStorageService firebaseStorageService =
        new FirebaseStorageService();

    await comentarioEmprendimientoFunciones.eliminar(emprendimiento.uuid);
    await likeEmprendimientoFunciones.eliminar(emprendimiento.uuid);
    await calificacionEmprendimientosFunciones.eliminar(emprendimiento.uuid);
    await firebaseStorageService.deleteFile("entrepreneurshipImages/" +
        emprendimiento.email +
        "/" +
        emprendimiento.uuid +
        "/caxen_foto1");
    await firebaseStorageService.deleteFile("entrepreneurshipImages/" +
        emprendimiento.email +
        "/" +
        emprendimiento.uuid +
        "/caxen_foto2");
    await firebaseStorageService.deleteFile("entrepreneurshipImages/" +
        emprendimiento.email +
        "/" +
        emprendimiento.uuid +
        "/caxen_foto3");

    await dbRefEmprendimientos.child(emprendimiento.uuid).set(null);
  }

  Future<int> obtenerVistasEmprendimientos() async {
    int cantidad;
    await dbRefEmprendimientosVistas.once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value != null) {
        cantidad = values.values.toList().length;
      } else {
        cantidad = 0;
      }
    });

    return cantidad;
  }
}
