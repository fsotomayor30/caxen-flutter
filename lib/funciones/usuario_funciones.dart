import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class UsuarioFunciones {
  final dbRefUsuario = FirebaseDatabase.instance.reference().child("users");

  Future<bool> agregarUsuario(Usuario usuario) async {
    bool respuesta = false;

    await dbRefUsuario.child(usuario.uid).set({
      'uuid': usuario.uid,
      'displayName': usuario.displayName == null ? "" : usuario.displayName,
      'email': usuario.email,
      'photoProfile': usuario.photoProfile == null ? "" : usuario.photoProfile,
      'tokenRing': usuario.tokenRing
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<Usuario> obtenerUsuario(String uuid) async {
    Usuario usuario;
    await dbRefUsuario
        .orderByChild("uuid")
        .equalTo(uuid)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value != null) {
        usuario = Usuario.fromSnapshot(values.values.toList()[0]);
      } else {
        usuario = null;
      }
    });

    return usuario;
  }

  Future<Usuario> obtenerUsuarioByEmail(String email) async {
    Usuario usuario;
    await dbRefUsuario
        .orderByChild("email")
        .equalTo(email)
        .once()
        .then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value != null) {
        usuario = Usuario.fromSnapshot(values.values.toList()[0]);
      } else {
        usuario = null;
      }
    });

    return usuario;
  }
}
