import 'package:app/modelos/calificacion_freelance.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:firebase_database/firebase_database.dart';

class CalificacionFreelanceFunciones {
  final dbRef = FirebaseDatabase.instance.reference().child("freelanceQualify");

  Future<bool> calificar(
      String idFreelance, Usuario usuario, int calificacion) async {
    bool respuesta;
    await dbRef.child(idFreelance).push().set({
      'email': usuario.email,
      'qualify': calificacion.toString(),
      'uuid': idFreelance
    }).then((value) {
      respuesta = true;
    });
    return respuesta;
  }

  Future<void> eliminar(String idFreelance) async {
    await dbRef.child(idFreelance).set(null);
  }

  Future<int> ultimaCalificacion(Freelance freelance, Usuario usuario) async {
    int dias;
    await dbRef.child(freelance.uuid).once().then((DataSnapshot dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      if (dataSnapshot.value == null) {
        dias = -1;
      } else {
        List<dynamic> list = values.values.toList();
        List<CalificacionFreelance> listCalificaciones =
            <CalificacionFreelance>[];
        list.forEach((element) {
          listCalificaciones.add(CalificacionFreelance.fromSnapshot(element));
        });
        listCalificaciones = listCalificaciones
            .where((element) => element.email == usuario.email)
            .toList();
        listCalificaciones.sort((a, b) {
          return b.date.compareTo(a.date);
        });

        if (listCalificaciones.length == 0) {
          dias = -1;
        } else {
          listCalificaciones.sort((a, b) {
            return b.date.compareTo(a.date);
          });

          DateTime dateTime = DateTime.now();
          dias = dateTime.difference(listCalificaciones.last.date).inDays;
        }
      }
    });
    return dias;
  }
}
