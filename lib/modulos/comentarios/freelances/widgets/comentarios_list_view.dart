import 'dart:async';

import 'package:app/modelos/comentario_freelance.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/comentarios/freelances/widgets/comentarios_list_tile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ComentariosListView extends StatefulWidget {
  final Freelance freelance;

  ComentariosListView({@required this.freelance});

  @override
  ComentariosListViewState createState() => ComentariosListViewState();
}

class ComentariosListViewState extends State<ComentariosListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("freelanceComment");

  List<ComentarioFreelance> lists = <ComentarioFreelance>[];

  StreamSubscription<Event> _onAddedSubscription;
  StreamSubscription<Event> _onChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onAddedSubscription =
        dbRef.child(widget.freelance.uuid).onChildAdded.listen(_childAdded);
    _onChangedSubscription =
        dbRef.child(widget.freelance.uuid).onChildChanged.listen(_childChanged);
  }

  @override
  void dispose() {
    _onAddedSubscription.cancel();
    _onChangedSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(children: <Widget>[
      Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: lists.length,
              itemBuilder: (BuildContext context, int index) {
                return ComentariosListTile(
                  comentarioFreelance: lists[index],
                );
              }))
    ]));
  }

  void _childAdded(Event event) {
    setState(() {
      lists.add(new ComentarioFreelance.fromSnapshot(event.snapshot.value));
    });
  }

  void _childChanged(Event event) {}
}
