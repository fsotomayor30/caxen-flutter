import 'package:app/modelos/comentario_oficio.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComentariosListTile extends StatefulWidget {
  final ComentarioOficio comentarioOficio;

  ComentariosListTile({@required this.comentarioOficio});

  @override
  ComentariosListTileState createState() => ComentariosListTileState();
}

class ComentariosListTileState extends State<ComentariosListTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final format = new DateFormat('yyyy-MM-dd hh:mm');

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          leading: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: (widget.comentarioOficio.imagePerfil != '')
                  ? Image.network(widget.comentarioOficio.imagePerfil)
                  : Container(
                      width: 55,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          border: Border.all(
                              width: 3,
                              color: OFICIOS_COLOR,
                              style: BorderStyle.solid)),
                      child: Center(
                          child: Text(
                        widget.comentarioOficio.email[0].toUpperCase(),
                        style: TextStyle(color: OFICIOS_COLOR, fontSize: 25),
                      )),
                    )),
          title: Text(
            widget.comentarioOficio.name +
                ": " +
                widget.comentarioOficio.comment,
            style: TextStyle(fontSize: 13, color: Colors.black45),
          ),
          subtitle: Text(
            format.format(widget.comentarioOficio.date),
            style: TextStyle(color: Colors.black54, fontSize: 10),
          ),
        ),
      ),
    );
  }
}
