import 'dart:async';

import 'package:app/modelos/comentario_oficio.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/comentarios/oficios/widgets/comentarios_list_tile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ComentariosListView extends StatefulWidget {
  final Oficio oficio;

  ComentariosListView({@required this.oficio});

  @override
  ComentariosListViewState createState() => ComentariosListViewState();
}

class ComentariosListViewState extends State<ComentariosListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("oficioComment");

  List<ComentarioOficio> lists = <ComentarioOficio>[];

  StreamSubscription<Event> _onAddedSubscription;
  StreamSubscription<Event> _onChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onAddedSubscription =
        dbRef.child(widget.oficio.uuid).onChildAdded.listen(_childAdded);
    _onChangedSubscription =
        dbRef.child(widget.oficio.uuid).onChildChanged.listen(_childChanged);
  }

  @override
  void dispose() {
    _onAddedSubscription.cancel();
    _onChangedSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(children: <Widget>[
      Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: lists.length,
              itemBuilder: (BuildContext context, int index) {
                return ComentariosListTile(
                  comentarioOficio: lists[index],
                );
              }))
    ]));
  }

  void _childAdded(Event event) {
    setState(() {
      lists.add(new ComentarioOficio.fromSnapshot(event.snapshot.value));
    });
  }

  void _childChanged(Event event) {}
}
