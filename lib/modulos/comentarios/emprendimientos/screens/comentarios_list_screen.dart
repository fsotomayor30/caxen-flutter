import 'package:app/funciones/comentario_emprendimiento_funciones.dart';
import 'package:app/funciones/usuario_funciones.dart';
import 'package:app/http/notification_service.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/comentarios/emprendimientos/widgets/comentarios_list_view.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ComentariosListScreen extends StatefulWidget {
  final Emprendimiento emprendimiento;

  ComentariosListScreen({@required this.emprendimiento});

  @override
  _ComentariosListState createState() => _ComentariosListState();
}

class _ComentariosListState extends State<ComentariosListScreen> {
  final _formKey = GlobalKey<FormState>();
  final commentController = TextEditingController();
  final dbRef =
      FirebaseDatabase.instance.reference().child("entrepreneurshipComment");
  ComentarioEmprendimientoFunciones comentarioEmprendimientoFunciones =
      new ComentarioEmprendimientoFunciones();

  NotificationService notificationService = NotificationService();
  UsuarioFunciones usuarioFunciones = UsuarioFunciones();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    commentController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
        builder: (GlobalController globalController) => Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text("Comentarios"),
              backgroundColor: EMPRENDIMIENTOS_COLOR,
            ),
            body: Container(
                width: Get.width,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ComentariosListView(
                        emprendimiento: widget.emprendimiento,
                      ),
                      Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(5.0),
                                child: TextFormField(
                                  controller: commentController,
                                  decoration: InputDecoration(
                                    labelText: "Ingresa tu comentario",
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: EMPRENDIMIENTOS_COLOR),
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                  // The validator receives the text that the user has entered.
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Debes ingresar el comentario';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      this._botonComentar(globalController.usuario),
                    ],
                  ),
                ))));
  }

  Widget _botonComentar(Usuario usuario) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Builder(
        builder: (context) => OutlineButton(
            shape: StadiumBorder(),
            textColor: EMPRENDIMIENTOS_COLOR,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Comentar',
                style: TextStyle(fontSize: 30),
              ),
            ),
            borderSide: BorderSide(
                color: EMPRENDIMIENTOS_COLOR,
                style: BorderStyle.solid,
                width: 1),
            onPressed: () async {
              if (_formKey.currentState.validate()) {
                int diasDiferencia = await comentarioEmprendimientoFunciones
                    .ultimoCommentario(widget.emprendimiento, usuario);
//                if(diasDiferencia<30 && diasDiferencia != -1){
//                  final snackBar = SnackBar(
//                    content: Text("Quieres volver a comentar? Envíanos un mail"),
//                  );
//                  Scaffold.of(context).showSnackBar(snackBar);
//                }else{
                dbRef.child(widget.emprendimiento.uuid).push().set({
                  "comment": commentController.text,
                  "email": usuario.email,
                  "entrepreneurshipid": widget.emprendimiento.uuid,
                  "imagePerfil": usuario.photoProfile,
                  "name": usuario.displayName,
                  "uuid": widget.emprendimiento.uuid,
                  "date": DateTime.now().toString()
                }).then((_) async {
                  Usuario usuario = await usuarioFunciones
                      .obtenerUsuarioByEmail(widget.emprendimiento.email);
                  if (usuario != null) {
                    if (usuario.tokenRing != null) {
                      await notificationService.sendNotification(
                          usuario.tokenRing,
                          "Caxen nuevo comentario",
                          'Alguien comentó tu publicación en Caxen');
                    }
                  }

                  final snackBar = SnackBar(
                    content: Text("Comentario realizado con éxito"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);
                  commentController.clear();
                }).catchError((onError) {
                  final snackBar = SnackBar(
                    content: Text("Error: " + onError.toString()),
                  );

                  Scaffold.of(context).showSnackBar(snackBar);
                });
              }
            }
            //},
            ),
      ),
    );
  }

  void _dialogAyuda(BuildContext context, String title, String description) {
    EasyDialog(
      title: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        description,
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
