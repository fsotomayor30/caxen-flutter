import 'package:app/modelos/comentario_emprendimiento.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComentariosListTile extends StatefulWidget {
  final ComentarioEmprendimiento comentarioEmprendimiento;

  ComentariosListTile({@required this.comentarioEmprendimiento});

  @override
  ComentariosListTileState createState() => ComentariosListTileState();
}

class ComentariosListTileState extends State<ComentariosListTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final format = new DateFormat('yyyy-MM-dd hh:mm');

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          leading: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: (widget.comentarioEmprendimiento.imagePerfil != '')
                  ? Image.network(widget.comentarioEmprendimiento.imagePerfil)
                  : Container(
                      width: 55,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          border: Border.all(
                              width: 3,
                              color: EMPRENDIMIENTOS_COLOR,
                              style: BorderStyle.solid)),
                      child: Center(
                          child: Text(
                        widget.comentarioEmprendimiento.email[0].toUpperCase(),
                        style: TextStyle(
                            color: EMPRENDIMIENTOS_COLOR, fontSize: 25),
                      )),
                    )),
          title: Text(
            widget.comentarioEmprendimiento.name +
                ": " +
                widget.comentarioEmprendimiento.comment,
            style: TextStyle(fontSize: 13, color: Colors.black45),
          ),
          subtitle: Text(
            format.format(widget.comentarioEmprendimiento.date),
            style: TextStyle(color: Colors.black54, fontSize: 10),
          ),
        ),
      ),
    );
  }
}
