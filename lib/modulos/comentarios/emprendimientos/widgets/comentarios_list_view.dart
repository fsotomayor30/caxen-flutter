import 'dart:async';

import 'package:app/modelos/comentario_emprendimiento.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/comentarios/emprendimientos/widgets/comentarios_list_tile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ComentariosListView extends StatefulWidget {
  final Emprendimiento emprendimiento;

  ComentariosListView({@required this.emprendimiento});

  @override
  ComentariosListViewState createState() => ComentariosListViewState();
}

class ComentariosListViewState extends State<ComentariosListView> {
  final dbRef =
      FirebaseDatabase.instance.reference().child("entrepreneurshipComment");

  List<ComentarioEmprendimiento> lists = new List<ComentarioEmprendimiento>();

  StreamSubscription<Event> _onAddedSubscription;
  StreamSubscription<Event> _onChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onAddedSubscription = dbRef
        .child(widget.emprendimiento.uuid)
        .onChildAdded
        .listen(_childAdded);
    _onChangedSubscription = dbRef
        .child(widget.emprendimiento.uuid)
        .onChildChanged
        .listen(_childChanged);
  }

  @override
  void dispose() {
    _onAddedSubscription.cancel();
    _onChangedSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(children: <Widget>[
      Expanded(
          child: new ListView.builder(
              shrinkWrap: true,
              itemCount: lists.length,
              itemBuilder: (BuildContext context, int index) {
                return ComentariosListTile(
                  comentarioEmprendimiento: lists[index],
                );
              }))
    ]));
  }

  void _childAdded(Event event) {
    setState(() {
      lists
          .add(new ComentarioEmprendimiento.fromSnapshot(event.snapshot.value));
    });
  }

  void _childChanged(Event event) {}
}
