import 'dart:async';

import 'package:app/modelos/freelance.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:lottie/lottie.dart' as lottie;

class OnlyFreelanceMapScreen extends StatefulWidget {
  final Freelance freelance;

  OnlyFreelanceMapScreen({@required this.freelance});

  @override
  _OnlyFreelanceMapScreenState createState() => _OnlyFreelanceMapScreenState();
}

class _OnlyFreelanceMapScreenState extends State<OnlyFreelanceMapScreen> {
  LatLng _center;
  MapController _mapController = MapController();
  bool pinMap = false;
  StreamSubscription _getPositionSubscription;
  List<Marker>freelanceMarkers = [];

  @override
  void initState() {
    super.initState();
    getCurrentLocation();

    Geolocator().isLocationServiceEnabled().then((myLocation) {
      if (myLocation) {
        _getPositionSubscription = Geolocator().getPositionStream().listen((positionChanged) {
          print('[MAP] POSITION CHANGE $positionChanged');
          setState(() {
            _center = new LatLng(positionChanged.latitude, positionChanged.longitude);
          });
        });
      }});

    Future.delayed(Duration.zero,() {
      initMarker();
      //startTimer();
    });
  }

  @override
  void dispose() {
    _getPositionSubscription?.cancel();
    super.dispose();
  }


  void initMarker() async {
    if(widget.freelance.latitude != null && widget.freelance.longitude != null){
      print(widget.freelance.latitude);
      print(widget.freelance.longitude);
      Marker marker = Marker(
        height: 100,
        width: 100,
        point: LatLng(widget.freelance.latitude,widget.freelance.longitude),
        builder: (ctx) => Container(
            height: 100,
            width: 100,
            child: _buildMarker()
        ),
      );
      setState(() {
        freelanceMarkers.add(marker);
      });
    }
  }

  getCurrentLocation() async {
    Geolocator().isLocationServiceEnabled().then((myLocation) {
      if (myLocation) {
        Geolocator().getCurrentPosition().then((myCurrentLocation) {
          setState(() {
            _center = LatLng(myCurrentLocation.latitude, myCurrentLocation.longitude);
            pinMap = true;
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;


    return Scaffold(
            appBar: AppBar(
              backgroundColor: FREELANCES_COLOR,
              title: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Container(
                    width: mediaQueryData.width/2,
                    child: AutoSizeText("Freelancers",
                      textAlign: TextAlign.center,
                      maxFontSize: 18,
                      minFontSize: 10,)
                ),
              ),
              centerTitle: true,
            ),
            body: (!pinMap) ? Container() : new FlutterMap(
              options: MapOptions(
                center: LatLng(widget.freelance.latitude, widget.freelance.longitude),
                zoom: 12.0,
              ),
              mapController: _mapController,

              layers: [
                new TileLayerOptions(
                    urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c']
                ),
                // ADD THIS
                MarkerLayerOptions(
                    markers: new List.from(freelanceMarkers)..addAll([ Marker(
                      height: 45,
                      width: 45,
                      point: LatLng(_center.latitude,_center.longitude),
                      builder: (ctx) => Container(
                          height: 45,
                          width: 45,
                          child: _buildMeMarker()
                      ),
                    )])
                ),
              ],

            ),
          );

  }

  Widget _buildMarker() {
    return lottie.Lottie.asset('assets/bullhorn.json',width: 100);
  }

  Widget _buildMeMarker() {
    return lottie.Lottie.asset('assets/marker.json',width: 40);
  }
}
