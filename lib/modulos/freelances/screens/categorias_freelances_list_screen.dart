import 'package:app/modulos/freelances/screens/acciones_freelances_list_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CategoriasFreelancesListScreen extends StatefulWidget {
  CategoriasFreelancesListScreen({Key key}) : super(key: key);

  @override
  _CategoriasFreelancesListState createState() =>
      _CategoriasFreelancesListState();
}

class _CategoriasFreelancesListState
    extends State<CategoriasFreelancesListScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
      mainAxisSpacing: 15.0,
      crossAxisSpacing: 15.0,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Todas",
                      )),
            );
        },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.list,
                    size: 60,
                    color: Colors.white,
                  ),
                  Text("Todas", maxLines: 3, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,), textAlign: TextAlign.center,),
                ],
              )),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Mercadeo digital y Diseño gráfico",
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.lightbulb,
                    size: 60,
                    color: Colors.white,
                  ),
                  Text("Mercadeo digital y Diseño gráfico", maxLines: 3, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,), textAlign: TextAlign.center,),
                ],
              )),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Asesoría legal y generales",
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.gavel,
                    size: 60,
                    color: Colors.white,
                  ),
                  Text("Asesoría legal y generales", maxLines: 3, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,), textAlign: TextAlign.center,),
                ],
              )),
        ),

        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Audiovisual y Fotografía",
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.video,
                    size: 60,
                    color: Colors.white,
                  ),
                  Text("Audiovisual y Fotografía", maxLines: 3, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,), textAlign: TextAlign.center,),
                ],
              )),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Contabilidad, Admin y RRHH",
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.calculator,
                    size: 60,
                    color: Colors.white,
                  ),
                  Text("Contabilidad, Admin y RRHH", maxLines: 3, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,), textAlign: TextAlign.center,),
                ],
              )),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Redacción y Contenido",
                      )),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: FREELANCES_COLOR,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FaIcon(
                      FontAwesomeIcons.newspaper,
                      size: 60,
                      color: Colors.white,
                    ),
                    Text(
                      "Redacción y Contenido",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AccionesFreelancesListScreen(
                          categoria: "Informática y Desarrollo",
                        )),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: FREELANCES_COLOR,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FaIcon(
                      FontAwesomeIcons.laptop,
                      size: 60,
                      color: Colors.white,
                    ),
                    Text(
                      "Informática y Desarrollo",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AccionesFreelancesListScreen(
                          categoria: "Traducciones, Cursos y Talleres",
                        )),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: FREELANCES_COLOR,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FaIcon(
                      FontAwesomeIcons.language,
                      size: 60,
                      color: Colors.white,
                    ),
                    Text(
                      "Traducciones, Cursos y Talleres",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AccionesFreelancesListScreen(
                          categoria: "Corredor de propiedades",
                        )),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: FREELANCES_COLOR,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FaIcon(
                      FontAwesomeIcons.fileSignature,
                      size: 60,
                      color: Colors.white,
                    ),
                    Text(
                      "Corredor de propiedades",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AccionesFreelancesListScreen(
                          categoria: "Sicólogos, terapeutas y Coach",
                        )),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: FREELANCES_COLOR,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FaIcon(
                      FontAwesomeIcons.brain,
                      size: 60,
                      color: Colors.white,
                    ),
                    Text(
                      "Sicólogos, terapeutas y Coach",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      AccionesFreelancesListScreen(
                        categoria: "Arquitectos",
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.draftingCompass,
                    size: 60,
                    color: Colors.white,
                  ),
                  Text(
                    "Arquitectos",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),

          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AccionesFreelancesListScreen(
                          categoria: "Otros",
                        )),
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: FREELANCES_COLOR,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FaIcon(
                      FontAwesomeIcons.stream,
                      size: 60,
                      color: Colors.white,
                    ),
                    Text(
                      "Otros",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
          ),
        ],

    );
  }
}
