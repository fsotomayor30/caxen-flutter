import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/freelances/widgets/freelance_form.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/styles/style_app_bar.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FreelanceActualizarScreen extends StatefulWidget {
  final Freelance freelance;

  FreelanceActualizarScreen({@required this.freelance});

  @override
  _FreelanceActualizarScreenState createState() =>
      _FreelanceActualizarScreenState();
}

class _FreelanceActualizarScreenState extends State<FreelanceActualizarScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: FREELANCES_COLOR,
          centerTitle: true,
          title: Text("Freelancers", style: styleAppBar),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                _dialogAyuda();
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(
                  Icons.help,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            )
          ],
        ),
        body: FreelanceForm(freelance: widget.freelance));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aqui puedes actualizar la información de tu freelancer",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
