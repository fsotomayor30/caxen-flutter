import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/freelances/widgets/mis_freelances_list_view.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';

class MisFreelancesListScreen extends StatefulWidget {
  final Usuario usuario;

  MisFreelancesListScreen({@required this.usuario});

  @override
  _MisFreelancesListState createState() => _MisFreelancesListState();
}

class _MisFreelancesListState extends State<MisFreelancesListScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    final double width = MediaQuery.of(context).size.width - 20;
    final double height = MediaQuery.of(context).size.height - 20;
    return Scaffold(
        appBar: AppBar(
            backgroundColor: FREELANCES_COLOR,
            title: Text("Mis Freelances"),
            centerTitle: true,
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  _dialogAyuda();
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Icon(
                    Icons.help,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              ),
            ]),
        body: Container(
            width: mediaQueryData.width,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MisFreelancesListView(
                    usuario: widget.usuario,
                  ),
                ],
              ),
            )));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes ver todos tus freelances disponibles",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
