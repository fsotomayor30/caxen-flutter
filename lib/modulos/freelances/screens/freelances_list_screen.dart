import 'dart:async';

import 'package:app/funciones/freelance_funciones.dart';
import 'package:app/modelos/bot_freelance.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/freelances/screens/freelance_map_screen.dart';
import 'package:app/modulos/freelances/screens/freelancers_detail_screen.dart';
import 'package:app/modulos/freelances/widgets/freelances_list_view.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/styles/style_app_bar.dart';
import 'package:app/shared/widgets/bubble_canvas.dart';
import 'package:app/shared/widgets/region_drop_down.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart' as lottie;

class FreelancesListScreen extends StatefulWidget {
  final String categoria;

  FreelancesListScreen({@required this.categoria});

  @override
  _FreelancesListState createState() => _FreelancesListState();
}

class _FreelancesListState extends State<FreelancesListScreen> {
  FreelancesFunciones freelancesFunciones = FreelancesFunciones();

  String region = "Todas";
  int vistas;
  bool showBot = true;
  BotFreelance botFreelance;

  Timer timer;
  int countSecond = 0;
  int gradeAnimation = 15;
  int gradeTotalAnimation = 360;

  @override
  void initState() {
    super.initState();

    print("[CATEGORIA FREELANCES] " + widget.categoria);

    freelancesFunciones
        .agregarVista()
        .then((value) => print("Se agrego vista"));

    freelancesFunciones.obtenerVistasFreelances().then((value) {
      if (mounted) {
        setState(() {
          vistas = value;
        });
      }
    });
    FirebaseDatabase.instance
        .reference()
        .child("botFreelance/botId")
        .once()
        .then((DataSnapshot snapshot) {
      setState(() {
        botFreelance = BotFreelance.fromSnapshot(snapshot.value);
      });
    });
    //startTimer();
  }

  void startTimer() {
    // Start the periodic timer which prints something every 1 seconds
    timer = new Timer.periodic(new Duration(seconds: 1), (time) {
      print('Something');
      setState(() {
        countSecond++;
      });
      if (countSecond % 2 != 0) {
        setState(() {
          gradeAnimation = 25;
          gradeTotalAnimation = 300;
        });
      } else {
        setState(() {
          gradeAnimation = 15;
          gradeTotalAnimation = 360;
        });
      }
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
        builder: (GlobalController globalController) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: FREELANCES_COLOR,
            title: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Text(
                "Freelancers",
                style: styleAppBar,
              ),
            ),
            centerTitle: true,
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  _dialogAyuda();
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Icon(
                    Icons.help,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              )
            ],
            bottom: PreferredSize(
                child: vistas != null
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FaIcon(
                            FontAwesomeIcons.eye,
                            color: Colors.black45,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            vistas.toString(),
                            style: TextStyle(color: Colors.black45),
                          )
                        ],
                      )
                    : Container(),
                preferredSize: Size.fromHeight(20)),
          ),
          body: Container(
              width: Get.width,
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        this._barraBusqueda(Get.width),
                        FreelancesListView(
                          categoria: widget.categoria,
                          region: region,
                        ),
                      ],
                    ),
                  ),
                  showBot
                      ? Positioned(
                          right: 80,
                          bottom: 100,
                          child: GestureDetector(
                            onTap: () {
                              this.timer?.cancel();

                              FirebaseDatabase.instance
                                  .reference()
                                  .child("freelance/" + botFreelance.uuid)
                                  .once()
                                  .then((DataSnapshot snapshot) {
                                Freelance freelance =
                                    Freelance.fromSnapshot(snapshot.value);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            FreelanceDetailListScreen(
                                              usuario: globalController.usuario,
                                              freelance: freelance,
                                            )));
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.all(10),
                              height: 100,
                              width: MediaQuery.of(context).size.width * .5,
                              child: BubbleMessage(
                                  painter: BubblePainter(),
                                  child: Container(
                                      constraints: BoxConstraints(
                                        maxWidth: 250.0,
                                        minWidth: 50.0,
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15.0, vertical: 6.0),
                                      child: Center(
                                        child: Text(
                                            botFreelance == null
                                                ? ''
                                                : botFreelance.text,
                                            textAlign: TextAlign.center,
                                            maxLines: 3,
                                            softWrap: true,
                                            style: TextStyle(
                                              color: Color(0xFF5A5858),
                                              fontSize: 16.0,
                                            )),
                                      ))),
                              //child: Center(child: Text(botEmprendimiento == null ? '' : botEmprendimiento.text, textAlign: TextAlign.center,)),
                            ),
                          ),
                        )
                      : Container(),
                  showBot
                      ? Positioned(
                          right: 90,
                          bottom: 170,
                          //bottom: 170,
                          //left: MediaQuery.of(context).size.width * .5 + 120,
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                showBot = false;
                              });
                            },
                            child: Container(
                              width: 20,
                              height: 20,
                              decoration: BoxDecoration(
                                color: EMPRENDIMIENTOS_COLOR,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)),
                              ),
                              child: Center(
                                  child: Text(
                                "x",
                                style: TextStyle(color: Colors.white),
                              )),
                            ),
                          ),
                        )
                      : Container(),
                  showBot
                      ? Positioned(
                          bottom: -10,
                          right: 0,
                          child: Image.asset(
                            'assets/bot.png',
                            height: 150,
                            width: 150,
                          ),
                        )
                      : Positioned(
                          bottom: -15,
                          right: 0,
                          child: RotationTransition(
                              turns: new AlwaysStoppedAnimation(
                                  gradeAnimation / gradeTotalAnimation),
                              child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      this.timer?.cancel();
                                      showBot = true;
                                    });
                                  },
                                  child: Image.asset('assets/bot.png',
                                      height: 150, width: 150))),
                        ),
                ],
              )));
    });
  }

  Widget _barraBusqueda(double width) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Card(
          margin: EdgeInsets.all(0),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: width - 16,
                      child: widget.categoria == "Todas"
                          ? AutoSizeText(
                              "Busca por regiones:",
                              maxLines: 1,
                              style: TextStyle(fontSize: 20),
                            )
                          : AutoSizeText(
                              "Busca " + widget.categoria + " por regiones:",
                              maxLines: 1,
                              style: TextStyle(fontSize: 20),
                            ),
                    ),
                  ],
                ),
                this._regionDropDown(),
              ],
            ),
          )),
    );
  }

  Widget _regionDropDown() {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          FreelancersMapScreen()));
            },
            child: Container(
              width: 40,
              child: lottie.Lottie.asset('assets/marker.json', width: 40),
            ),
          ),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    RegionDropDown(onChanged: (String region) {
                      setState(() {
                        this.region = region;
                      });
                    })
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes ver todos los Freelancers disponibles",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
