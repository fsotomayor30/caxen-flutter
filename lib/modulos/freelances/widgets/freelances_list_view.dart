import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/freelances/screens/freelance_crear_screen.dart';
import 'package:app/modulos/freelances/widgets/freelances_list_tile.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class FreelancesListView extends StatefulWidget {
  final String categoria;
  final String region;

  FreelancesListView({@required this.categoria, @required this.region});

  @override
  FreelancesListViewState createState() => FreelancesListViewState();
}

class FreelancesListViewState extends State<FreelancesListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("freelance");
  List<Freelance> lists = <Freelance>[];

  @override
  void initState() {
    print("[CATEGORIA FREELANCE] " + widget.categoria);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) {
        return Expanded(
            child: Column(children: <Widget>[
          Expanded(
              child: FutureBuilder(
                  future: dbRef.once(),
                  builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      lists.clear();
                      Map<dynamic, dynamic> values = snapshot.data.value;

                      if (values == null) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.sadTear,
                                  size: 80,
                                  color: Colors.black45,
                                ),
                                Text("No hay freelancers"),
                                this._botonSubirFreelance()
                              ],
                            ),
                          ),
                        );
                      } else {
                        List<dynamic> list = values.values.toList();

                        list.sort((a, b) {
                          return b["date"].compareTo(a['date']);
                        });

                        list.forEach((element) {
                          Freelance freelance = Freelance.fromSnapshot(element);

                          if (widget.categoria == "Todas" &&
                              freelance.visible) {
                            if (widget.region == "Todas") {
                              lists.add(Freelance.fromSnapshot(element));
                            } else {
                              if (widget.region == freelance.region) {
                                lists.add(Freelance.fromSnapshot(element));
                              }
                            }
                          } else {
                            if (widget.region == "Todas" && freelance.visible) {
                              if (widget.categoria == freelance.category) {
                                lists.add(Freelance.fromSnapshot(element));
                              }
                            } else {
                              if (widget.categoria == freelance.category &&
                                  widget.region == freelance.region) {
                                lists.add(Freelance.fromSnapshot(element));
                              }
                            }
                          }
                        });

                        return (lists.length == 0)
                            ? Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Center(
                                  child: Column(
                                    children: <Widget>[
                                      FaIcon(
                                        FontAwesomeIcons.sadTear,
                                        size: 80,
                                        color: Colors.black45,
                                      ),
                                      Text("No hay freelancers"),
                                      this._botonSubirFreelance()
                                    ],
                                  ),
                                ),
                              )
                            : new ListView.builder(
                                shrinkWrap: true,
                                itemCount: lists.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return FreelancesListTile(
                                    usuario: globalController.usuario,
                                    freelance: lists[index],
                                  );
                                });
                      }
                    }

                    return Center(
                      child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator()),
                    );
                  }))
        ]));
      },
    );
  }

  Widget _botonSubirFreelance() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar Freelance',
            style: TextStyle(fontSize: 20),
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => FreelanceCrearScreen(
                      region: widget.region,
                      categoria: widget.categoria,
                    )),
          );
        },
      ),
    );
  }
}
