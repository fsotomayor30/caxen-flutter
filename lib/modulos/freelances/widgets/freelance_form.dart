import 'dart:io';

import 'package:app/modelos/freelance.dart';
import 'package:app/modulos/freelances/screens/freelances_list_screen.dart';
import 'package:app/servicios/firebase_storage_service.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/pages/camera.page.dart';
import 'package:app/shared/widgets/categorias_freelances_form_drop_down.dart';
import 'package:app/shared/widgets/region_form_drop_down.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:latlong/latlong.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:progress_dialog/progress_dialog.dart';

class FreelanceForm extends StatefulWidget {

  final Freelance freelance;

  FreelanceForm({@required this.freelance});

  @override
  _FreelanceScreenState createState() => _FreelanceScreenState();
}

class _FreelanceScreenState extends State<FreelanceForm> {

  ProgressDialog progressDialog;

  final _formKey = GlobalKey<FormState>();


  File imagen1;
  File imagen2;
  File imagen3;


  final dbRef = FirebaseDatabase.instance.reference().child("freelance");
  FirebaseStorageService firebaseStorageService = FirebaseStorageService();

  Freelance _freelance;

  LatLng _center;


  MapController _mapController = MapController();

  @override
  void initState() {
    super.initState();
    setState(() {
      _freelance = widget.freelance;
      if(_freelance.address!=null){
        _center = LatLng(_freelance.latitude, _freelance.longitude);

      }
    });
  }

  Future escogerImagen1(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen1 = image;
    });

    if (imageSource == ImageSource.gallery) {
      _freelance.source = "gallery";
    } else {
      _freelance.source = "camera";
    }
  }

  Future escogerImagen2(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen2 = image;
    });
  }

  Future escogerImagen3(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen3 = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Publicando...");

    return Container(
          margin: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
              child: Form(
                  key: this._formKey,
                  child: Column(
                    children: <Widget>[
                      this._nombreFreelanceInput(),
                      this._descripcionFreelanceInput(),
                      this._categoriasDropDown(width),
                      SizedBox(height: 20),
                      this._direccionFreelanceInput(),
                      Visibility(
                        visible: _freelance.address != null,
                        child: this._mapAddress(),
                      ),
                      SizedBox(height: 20),
                      this._regionDropDown(width),
                      this._fotosInput(width),
                      this._sitioWebFreelanceInput(),
                      this._facebookFreelanceInput(),
                      this._instagramFreelanceInput(),
                      this._linkedinFreelanceInput(),
                      this._emailFreelanceInput(),
                      this._telefonoFreelanceInput(),
                      this._botonGuardar()
                    ],
                  )))
    );
  }

  Widget _mapAddress(){
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width - 40,
      child: FlutterMap(
        options: MapOptions(
          center: _center,
          zoom: 16.0,
        ),
        mapController: _mapController,
        layers: [
          new TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']
          ),
          MarkerLayerOptions(
              markers: [ Marker(
                height: 45,
                width: 45,
                point: _center,
                builder: (ctx) => Container(
                    height: 45,
                    width: 45,
                    child: _buildMarker()
                ),
              )]
          ),
          // ADD THIS
        ],

      ),
    );
  }

  Widget _buildMarker() {
    return Container(
        width: 300.0,
        height: 300.0,
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(110.0),
        ),
        child: Center(
            child:
            lottie.Lottie.asset('assets/marker.json',width: 40)


        ));
  }

  Widget _direccionFreelanceInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.streetView, size: 25,
                  color: Colors.black45,),
                labelText: 'Dirección del freelance',
                hintText: 'Alameda 412, Santiago'),
            initialValue: _freelance.address,
            onChanged: (String value) {
              setState(() {
                if(value.isEmpty){
                  _freelance.address = null;
                }else{
                  _freelance.address = value;
                }
              });

            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: new OutlineButton(
            shape: StadiumBorder(),
            textColor: FREELANCES_COLOR,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Buscar',
                style: TextStyle(fontSize: 10),
              ),
            ),
            borderSide: BorderSide(
                color: FREELANCES_COLOR,
                style: BorderStyle.solid,
                width: 1),
            onPressed: () async {
              if (_freelance.address==null) {
                final snackBar = SnackBar(
                  content: Text("Error: Para buscar ingresa la dirección"),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              }else{
                try{
                  List<Placemark> placemark = await Geolocator().placemarkFromAddress(_freelance.address);
                  _mapController.move(LatLng(placemark[0].position.latitude, placemark[0].position.longitude), 16);
                  setState(() {
                    _center = LatLng(placemark[0].position.latitude, placemark[0].position.longitude);
                  });
                }catch(e){
                  final snackBar = SnackBar(
                    content: Text("Error: No encontramos ninguna dirección"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);
                }

              }
            },
          ),
        )
      ],
    );
  }

  Widget _categoriasDropDown(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.list,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    CategoriaFreelancesFormDropDown(
                        initValue: _freelance.category,
                        onChanged: (String category) {
                          _freelance.category = category;
                        })
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _fotosInput(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.image,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            width: width - 80,
            child: Card(
              elevation: 0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.black38, width: 1),
                borderRadius: BorderRadius.circular(5),
              ),
              margin: EdgeInsets.all(0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen1 == null)
                              ? (_freelance.foto1 == null)
                              ? Container(
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                            decoration: BoxDecoration(
                                color: Colors.black45,
                                border: Border.all(
                                    color: Colors.black, width: 1),
                                borderRadius:
                                BorderRadius.circular(10)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.image,
                                  size: 40,
                                ),
                                Text("Selfie o marca", style: TextStyle(fontSize: 10,color: Colors.black54),)
                              ],
                            ),
                          )
                              : Image.network(
                            _freelance.foto1,
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                            loadingBuilder: (BuildContext context,
                                Widget child,
                                ImageChunkEvent loadingProgress) {
                              if (loadingProgress == null)
                                return child;
                              return Container(
                                width: ((width - 80) / 2) - 15,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    value: loadingProgress
                                        .expectedTotalBytes !=
                                        null
                                        ? loadingProgress
                                        .cumulativeBytesLoaded /
                                        loadingProgress
                                            .expectedTotalBytes
                                        : null,
                                  ),
                                ),
                              );
                            },
                          )
                              : Image.file(
                            imagen1,
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 1"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: FREELANCES_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: FREELANCES_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Selfie o marca',
                                        orientation: 'vertical',
                                        color: FREELANCES_COLOR,
                                        onChanged: (String path){
                                          setState(() {
                                            imagen1 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: FREELANCES_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: FREELANCES_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen1(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen2 == null)
                              ? (_freelance.foto2 == null)
                              ? Container(
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                            decoration: BoxDecoration(
                                color: Colors.black45,
                                border: Border.all(
                                    color: Colors.black, width: 1),
                                borderRadius:
                                BorderRadius.circular(10)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.image,
                                  size: 40,
                                ),
                                Text("Tus productos o servicios", style: TextStyle(fontSize: 10,color: Colors.black54),)
                              ],
                            ),
                          )
                              : Image.network(
                            _freelance.foto2,
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                            loadingBuilder: (BuildContext context,
                                Widget child,
                                ImageChunkEvent loadingProgress) {
                              if (loadingProgress == null)
                                return child;
                              return Container(
                                width: ((width - 80) / 2) - 15,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    value: loadingProgress
                                        .expectedTotalBytes !=
                                        null
                                        ? loadingProgress
                                        .cumulativeBytesLoaded /
                                        loadingProgress
                                            .expectedTotalBytes
                                        : null,
                                  ),
                                ),
                              );
                            },
                          )
                              : Image.file(
                            imagen2,
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 2"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: FREELANCES_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: FREELANCES_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Tus productos o servicios',
                                        orientation: 'vertical',
                                        color: FREELANCES_COLOR,
                                        onChanged: (String path){
                                          setState(() {
                                            imagen2 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: FREELANCES_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: FREELANCES_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen2(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen3 == null)
                              ? (_freelance.foto3 == null)
                              ? Container(
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                            decoration: BoxDecoration(
                                color: Colors.black45,
                                border: Border.all(
                                    color: Colors.black, width: 1),
                                borderRadius:
                                BorderRadius.circular(10)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.image,
                                  size: 40,
                                ),
                                Text("Tus productos o servicios", style: TextStyle(fontSize: 10,color: Colors.black54),)
                              ],
                            ),
                          )
                              : Image.network(
                            _freelance.foto3,
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                            loadingBuilder: (BuildContext context,
                                Widget child,
                                ImageChunkEvent loadingProgress) {
                              if (loadingProgress == null)
                                return child;
                              return Container(
                                width: ((width - 80) / 2) - 15,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    value: loadingProgress
                                        .expectedTotalBytes !=
                                        null
                                        ? loadingProgress
                                        .cumulativeBytesLoaded /
                                        loadingProgress
                                            .expectedTotalBytes
                                        : null,
                                  ),
                                ),
                              );
                            },
                          )
                              : Image.file(
                            imagen3,
                            width: ((width - 80) / 2) - 15,
                            height: (width - 80) / 2,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 3"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: FREELANCES_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: FREELANCES_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Tus productos o servicios',
                                        orientation: 'vertical',
                                        color: FREELANCES_COLOR,
                                        onChanged: (String path){
                                          setState(() {
                                            imagen3 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: FREELANCES_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: FREELANCES_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen3(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _regionDropDown(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                FontAwesomeIcons.mapMarkerAlt,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    RegionFormDropDown(
                        initValue: _freelance.region,
                        onChanged: (String region) {
                          _freelance.region = region;
                        })
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _nombreFreelanceInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 30,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.rocket),
                labelText: 'Nombre del Freelancer',
                hintText: 'Caxen'),
            initialValue: _freelance.nameFreelance,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (value.length > 30) {
                return 'La cantidad de letras debe ser menor a 30';
              }
              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              return null;
            },
            onSaved: (String value) {
              _freelance.nameFreelance = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _sitioWebFreelanceInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.laptop),
                labelText: 'Sitio Web',
                hintText: 'www.freelance.com'),
            initialValue: _freelance.webSite,
            onSaved: (String value) {
              _freelance.webSite = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left:40,bottom: 10),
          child: Text(
            "El sitio web ingresado debe estar en el siguiente formato www.freelance.com",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _descripcionFreelanceInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLines: 4,
            maxLength: 150,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.pencilAlt),
                labelText: 'Descripción',
                hintText: '...'),
            initialValue: _freelance.description,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }

              if (value.length > 150) {
                return 'La cantidad de letras debe ser menor a 150';
              }

              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              return null;
            },
            onSaved: (String value) {
              _freelance.description = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _facebookFreelanceInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.facebookF),
                labelText: 'Facebook',
                hintText: 'copia aquí el URL de tu página FB'),
            initialValue: _freelance.urlFb,
            onSaved: (String value) {
              _freelance.urlFb = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left:25,bottom: 10),
          child: Text(
            "El usuario ingresado debe estar en el siguiente formato /freelance",
            style: TextStyle(color: Colors.black45, fontSize: 9),
          ),
        )
      ],
    );
  }

  Widget _instagramFreelanceInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.instagram),
                labelText: 'Instagram',
                hintText: '@caxen.app'),
            initialValue: _freelance.urlInstagram,
            onSaved: (String value) {
              _freelance.urlInstagram = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left:15,bottom: 10),
          child: Text(
            "El usuario ingresado debe estar en el siguiente formato @caxen.app",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _linkedinFreelanceInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.linkedin),
                labelText: 'Linkedin',
                hintText: 'www.linkedin.com/in/freelance'),
            initialValue: _freelance.urlLinkedin,
            onSaved: (String value) {
              _freelance.urlLinkedin = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left:40,bottom: 10),
          child: Text(
            "El link ingresado debe estar en el siguiente formato www.linkedin.com/in/freelance",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _emailFreelanceInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.envelope),
                labelText: 'Correo Electrónico',
                hintText: 'freelance@freelance.com'),
            initialValue: _freelance.mail,
            onSaved: (String value) {
              _freelance.mail = value;
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              if(!EmailValidator.validate(value)){
                return 'Ingrese un correo válido';
              }
              return null;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _telefonoFreelanceInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            keyboardType: TextInputType.phone,
            maxLength: 12,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.phone),
                labelText: 'Número de celular',
                hintText: '+569 XX XX XX XX'),
            initialValue: _freelance.phone,
            validator: (value) {
              if (value.length > 12) {
                return 'El largo máximo es 12';
              }
              if (value.length>=1 && value.length < 12) {
                return 'El largo mínimo es 12';
              }

              if (value.length>=1 && value[0] != "+"){
                return 'Debe comenzar con +';
              }

              if (value.length>4 && value.substring(1, 4) != "569"){
                return 'Debe contener 569';
              }

              return null;
            },

            onSaved: (String value) {
              _freelance.phone = value;
            },

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left:40,bottom: 10),
          child: Text(
            "El número ingresado debe contener +569 más 8 digitos, por ejemplo, +569 12345678",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _botonGuardar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: FREELANCES_COLOR,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar',
            style: TextStyle(fontSize: 30),
          ),
        ),
        borderSide:
            BorderSide(color: FREELANCES_COLOR, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();

            await progressDialog.show();

            if (_freelance.uuid == null) {
              var id = dbRef.push().key;

              String foto1;
              if(imagen1 == null){

                  final snackBar = SnackBar(
                    content: Text("Error: Debes subir la imagen 1"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);

                  await progressDialog.hide();
                  return;

              }else{
                foto1 = await firebaseStorageService.uploadFile(
                    imagen1,
                    "caxen_foto1",
                    "freelanceImages/" + _freelance.email + "/" + id);
              }

              String foto2;
              if(imagen2 == null){

                  final snackBar = SnackBar(
                    content: Text("Error: Debes subir la imagen 2"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);

                  await progressDialog.hide();
                  return;

              }else{
                foto2 = await firebaseStorageService.uploadFile(
                    imagen2,
                    "caxen_foto2",
                    "freelanceImages/" + _freelance.email + "/" + id);
              }

              String foto3;
              if(imagen3 == null){

                  final snackBar = SnackBar(
                    content: Text("Error: Debes subir la imagen 3"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);

                  await progressDialog.hide();
                  return;

              }else{
                foto3 = await firebaseStorageService.uploadFile(
                    imagen3,
                    "caxen_foto3",
                    "freelanceImages/" + _freelance.email + "/" + id);
              }

              if(_freelance.address!= null){
                setState(() {
                  _freelance.latitude = _center.latitude;
                  _freelance.longitude = _center.longitude;
                });
              }

              var freelance = {
                "imagePerfil": _freelance.imagePerfil,
                "email": _freelance.email,
                "category": (_freelance.category == "Todas") ? "Mercadeo digital y Diseño gráfico" : _freelance.category,
                "date": Timestamp.now().millisecondsSinceEpoch.toString(),
                "description": _freelance.description,
                "mail": (_freelance.mail == null) ? "" : _freelance.mail,
                "name": _freelance.name,
                "nameFreelance": _freelance.nameFreelance,
                "urlInstagram": (_freelance.urlInstagram == null) ? "" : _freelance.urlInstagram,
                "urlLinkedin": (_freelance.urlLinkedin == null) ? "" : _freelance.urlLinkedin,
                "phone": (_freelance.phone == null) ? "" : _freelance.phone,
                "region": (_freelance.region == "Todas")  ? "Región de Arica y Parinacota" : _freelance.region,
                "source": _freelance.source,
                "webSite": _freelance.webSite,
                "foto1": foto1,
                "foto2": foto2,
                "foto3": foto3,
                "uuid": id,
                "urlFb": (_freelance.urlFb == null) ? "" : _freelance.urlFb,
                "visible": true,
                "address" : _freelance.address == null ? null : _freelance.address,
                "latitude" : _freelance.latitude == null ? null : _freelance.latitude,
                "longitude" : _freelance.longitude == null ? null : _freelance.longitude,
              };

              await dbRef.child(id).set(freelance).then((_) async {
                await progressDialog.hide();
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FreelancesListScreen(
                            categoria: "Todas",
                          )),
                );
              }).catchError((onError) async{
                await progressDialog.hide();

                final snackBar = SnackBar(
                  content: Text("Error: "+onError.toString()),
                );
                Scaffold.of(context).showSnackBar(snackBar);


              });
            } else {
              String foto1;
              (imagen1 == null)
                  ? foto1 = _freelance.foto1
                  : foto1 = await firebaseStorageService.uploadFile(
                  imagen1,
                  "caxen_foto1",
                  "freelanceImages/" +
                      _freelance.email +
                      "/" +
                      _freelance.uuid);

              String foto2;
              (imagen2 == null)
                  ? foto2 = _freelance.foto2
                  : foto2 = await firebaseStorageService.uploadFile(
                  imagen2,
                  "caxen_foto2",
                  "freelanceImages/" +
                      _freelance.email +
                      "/" +
                      _freelance.uuid);

              String foto3;
              (imagen3 == null)
                  ? foto3 = _freelance.foto3
                  : foto3 = await firebaseStorageService.uploadFile(
                  imagen3,
                  "caxen_foto3",
                  "freelanceImages/" +
                      _freelance.email +
                      "/" +
                      _freelance.uuid);

              if(_freelance.address!= null){
                setState(() {
                  _freelance.latitude = _center.latitude;
                  _freelance.longitude = _center.longitude;
                });
              }

              var freelance= {
                "imagePerfil": _freelance.imagePerfil,
                "email": _freelance.email,
                "category": (_freelance.category == "Todas") ? "Mercadeo digital y Diseño gráfico" : _freelance.category,
                "date": _freelance.date,
                "description": _freelance.description,
                "mail": (_freelance.mail == null) ? "" : _freelance.mail,
                "name": _freelance.name,
                "nameFreelance": _freelance.nameFreelance,
                "urlInstagram": (_freelance.urlInstagram == null) ? "" : _freelance.urlInstagram,
                "urlLinkedin": (_freelance.urlLinkedin == null) ? "" : _freelance.urlLinkedin,
                "phone": (_freelance.phone == null) ? "" : _freelance.phone,
                "region": (_freelance.region == "Todas")  ? "Región de Arica y Parinacota" : _freelance.region,
                "source": _freelance.source,
                "webSite": _freelance.webSite,
                "foto1": foto1,
                "foto2": foto2,
                "foto3": foto3,
                "uuid": _freelance.uuid,
                "urlFb": (_freelance.urlFb == null) ? "" : _freelance.urlFb,
                "visible": _freelance.visible,
                "address" : _freelance.address == null ? null : _freelance.address,
                "latitude" : _freelance.latitude == null ? null : _freelance.latitude,
                "longitude" : _freelance.longitude == null ? null : _freelance.longitude,
              };

              await dbRef.child(_freelance.uuid).set({
               freelance
              }).then((_) async {
                await progressDialog.hide();
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FreelancesListScreen(
                        categoria: "Todas",
                      )),
                );
              }).catchError((onError) async{
                await progressDialog.hide();
                final snackBar = SnackBar(
                  content: Text("Error: "+onError.toString()),
                );
                Scaffold.of(context).showSnackBar(snackBar);

              });
            }
          }
        },
      ),
    );
  }


}

void fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}
