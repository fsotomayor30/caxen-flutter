import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/freelances/screens/freelance_crear_screen.dart';
import 'package:app/modulos/freelances/widgets/mis_freelances_list_tile.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class MisFreelancesListView extends StatefulWidget {
  final Usuario usuario;

  MisFreelancesListView({@required this.usuario});

  @override
  _MisFreelancesListViewState createState() => _MisFreelancesListViewState();
}

class _MisFreelancesListViewState extends State<MisFreelancesListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("freelance");
  List<Freelance> lists = <Freelance>[];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) {
        return Expanded(
            child: Column(children: <Widget>[
          Expanded(
              child: FutureBuilder(
                  future: dbRef
                      .orderByChild("email")
                      .equalTo(widget.usuario.email)
                      .once(),
                  builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      lists.clear();
                      Map<dynamic, dynamic> values = snapshot.data.value;

                      if (values == null) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.sadTear,
                                  size: 80,
                                  color: Colors.black45,
                                ),
                                Text("No tienes freelance"),
                                this._botonSubirFreelance()
                              ],
                            ),
                          ),
                        );
                      } else {
                        List<dynamic> list = values.values.toList();

                        list.sort((a, b) {
                          return b["date"].compareTo(a['date']);
                        });

                        list.forEach((element) {
                          Freelance freelance = Freelance.fromSnapshot(element);
                          if (freelance.visible) {
                            lists.add(freelance);
                          }
                        });

                        if (lists.length == 0) {
                          return Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  FaIcon(
                                    FontAwesomeIcons.sadTear,
                                    size: 80,
                                    color: Colors.black45,
                                  ),
                                  Text("No tienes freelance"),
                                  this._botonSubirFreelance()
                                ],
                              ),
                            ),
                          );
                        } else {
                          return new ListView.builder(
                              shrinkWrap: true,
                              itemCount: lists.length,
                              itemBuilder: (BuildContext context, int index) {
                                return MisFreelancesListTile(
                                  usuario: globalController.usuario,
                                  freelance: lists[index],
                                );
                              });
                        }
                      }
                    }

                    return Center(
                      child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator()),
                    );
                  }))
        ]));
      },
    );
  }

  Widget _botonSubirFreelance() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar Freelance',
            style: TextStyle(fontSize: 20),
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => FreelanceCrearScreen(
                      region: "Todas",
                      categoria: "Todas",
                    )),
          );
        },
      ),
    );
  }
}
