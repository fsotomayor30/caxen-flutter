import 'package:app/shared/colors/root_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SlideDotsFreelance extends StatelessWidget {
  bool isActive;

  SlideDotsFreelance(this.isActive);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 10),
      height: isActive ? 12 : 8,
      width: isActive ? 12 : 8,
      decoration: BoxDecoration(
        color: isActive ? FREELANCES_COLOR : Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
}
