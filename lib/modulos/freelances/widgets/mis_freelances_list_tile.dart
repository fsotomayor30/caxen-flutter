import 'dart:io';

import 'package:app/funciones/freelance_funciones.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/freelances/screens/freelance_actualizar_screen.dart';
import 'package:app/modulos/freelances/screens/one_freelance_map_screen.dart';
import 'package:app/modulos/freelances/widgets/slide_dots_freelance.dart';
import 'package:app/modulos/usuario/screens/user_profile_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/pages/payment.page.dart';
import 'package:app/shared/widgets/tiempo_atras.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class MisFreelancesListTile extends StatefulWidget {
  final Freelance freelance;
  final Usuario usuario;

  MisFreelancesListTile({@required this.freelance, @required this.usuario});

  @override
  _MisFreelancesListTileState createState() => _MisFreelancesListTileState();
}

class _MisFreelancesListTileState extends State<MisFreelancesListTile> {
  final FreelancesFunciones freelancesFunciones = FreelancesFunciones();

  ProgressDialog progressDialog;
  int indexImage = 0;

  @override
  void initState() {
    super.initState();
  }

  changeImage(int index, CarouselPageChangedReason reason){
    setState(() {
      indexImage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Editando...");

    final mediaQueryData = MediaQuery.of(context).size;

    return Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Card(
              elevation: 10,
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 8, bottom: 10, left: 8, right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    this._informacionAutor(mediaQueryData.width),
                    this._nombre(),
                    SizedBox(
                      height: 10,
                    ),
                    this._descripcion(),
                    SizedBox(
                      height: 20,
                    ),
                    this._imagenes(mediaQueryData.width),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        for (int i = 0; i < 3; i++)
                          if (i == indexImage)
                            SlideDotsFreelance(true)
                          else
                            SlideDotsFreelance(false)
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        this._seccionEditar(widget.freelance),
                        this._seccionEliminar(widget.freelance, context)
                      ],
                    ),
                    this.divisor(),
                    this._contacto(),

                  ],
                ),
              ),
            ),

        );
  }

  Widget _informacionAutor(double width) {

    return ListTile(
        contentPadding: EdgeInsets.all(0),
        leading: Container(
          width: (width * 0.20) - 20,
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: (widget.freelance.imagePerfil != '') ? Image.network(widget.freelance.imagePerfil)
                  : Container(
                width: 55,
                decoration : BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    border: Border.all(width: 3,color: FREELANCES_COLOR,style: BorderStyle.solid)
                ),
                child: Center(child: Text(widget.freelance.email[0].toUpperCase(), style: TextStyle(color: FREELANCES_COLOR, fontSize: 25),)),)),
        ),
        title: Text(
          widget.freelance.name + ":",
          style: TextStyle(fontSize: 14),
        ),
        subtitle: TiempoAtrasWidget(fecha: widget.freelance.date,),
      trailing:  widget.usuario.email == widget.freelance.email ? OutlineButton(
        shape: StadiumBorder(),
        textColor: FREELANCES_COLOR,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Quiero llegar \na más personas',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 10),
          ),
        ),
        borderSide: BorderSide(
            color: FREELANCES_COLOR,
            style: BorderStyle.solid,
            width: 1),
        onPressed: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PaymentScreen(title: 'Promociona tu freelancer',color: FREELANCES_COLOR,)),
          );

        },
      ) : Container(width: 0, height: 0,),
    );
  }

  Widget _nombre() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.stickyNote,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Nombre: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.freelance.nameFreelance)
        ],
      ),
    );
  }

  Widget _descripcion() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.list,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Descripción: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.freelance.description, style: TextStyle(fontSize: 13))
        ],
      ),
    );
  }

  Widget _imagenes(double width) {
    final List<String> imgList = [
      widget.freelance.foto1,
      widget.freelance.foto2,
      widget.freelance.foto3,
    ];

    return Container(
      constraints: BoxConstraints.expand(height: 300),
      width: width,
      child:
      Builder(
        builder: (context) {
          final double height = MediaQuery.of(context).size.height;
          return CarouselSlider(
            options: CarouselOptions(
              height: height,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              onPageChanged:  changeImage

              // autoPlay: false,
            ),
            items: imgList.map((item) => Container(
              child: Center(
                  child:PinchZoomImage(
                    image: Image.network(item, fit: BoxFit.contain, height: height, width: width,),
                    zoomedBackgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
                    hideStatusBarWhileZooming: true,
                    onZoomStart: () {
                      print('Zoom started');
                    },
                    onZoomEnd: () {
                      print('Zoom finished');
                    },
                  ),

              ),
            )).toList(),
          );
        },
      ),
    );
  }

  Widget _seccionEliminar(Freelance freelance, BuildContext ctx) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/eliminar.png",
                  width: 20,
                ),
              ),
              Text(
                'Eliminar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          final snackBar = SnackBar(
            content: Text('¿Estás seguro de eliminarlo?'),
            action: SnackBarAction(
              label: 'Confirmar',
              onPressed: () {
                this.eliminar(widget.freelance, ctx);
              },
            ),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(ctx).showSnackBar(snackBar);
        },
      ),
    );
  }

  Widget _seccionEditar(Freelance freelance) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/editar.png",
                  width: 20,
                ),
              ),
              Text(
                'Editar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    FreelanceActualizarScreen(
                      freelance: widget.freelance,
                    )),
          );
        },
      ),
    );
  }

  Widget seccionEliminar(Freelance freelance, BuildContext ctx) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/eliminar.png",
                  width: 20,
                ),
              ),
              Text(
                'Eliminar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          final snackBar = SnackBar(
            content: Text('¿Estás seguro de eliminarlo?'),
            action: SnackBarAction(
              label: 'Confirmar',
              onPressed: () {
                this.eliminar(widget.freelance, ctx);
              },
            ),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(ctx).showSnackBar(snackBar);
        },
      ),
    );
  }

  void eliminar(Freelance freelance, BuildContext ctx) async {
    await progressDialog.show();


    try {
      await freelancesFunciones.eliminar(freelance);

      await progressDialog.hide();

      Navigator.of(ctx).pop();
      Navigator.of(ctx).pop();
      Navigator.push(
          context, MaterialPageRoute(builder: (ctx) => UserProfileScreen()));
    } catch (e) {
      await progressDialog.hide();

      final snackBar = SnackBar(
        content: Text("Error: "+e.toString()),
      );


      Scaffold.of(ctx).showSnackBar(snackBar);


    }
  }

  Widget seccionEditar(Freelance freelance) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/editar.png",
                  width: 20,
                ),
              ),
              Text(
                'Editar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    FreelanceActualizarScreen(
                      freelance: widget.freelance,
                    )),
          );
        },
      ),
    );
  }

  Widget _contacto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          SizedBox(
            height: 2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: widget.freelance.webSite != "",
                child: GestureDetector(
                  onTap: () async {
                    if (await canLaunch('https://'+widget.freelance.webSite)) {
                      await launch('https://'+widget.freelance.webSite);
                    } else {
                      throw 'Could not launch ' + 'https://'+widget.freelance.webSite;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/computadora.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.freelance.urlInstagram != "",
                child: GestureDetector(
                  onTap: () async {
                    String usuarioInstagram = widget.freelance.urlInstagram;
                    usuarioInstagram = usuarioInstagram.replaceAll("@", "");
                    if (await canLaunch("https://www.instagram.com/" +
                        usuarioInstagram)) {
                      await launch("https://www.instagram.com/" +
                          usuarioInstagram);
                    } else {
                      throw 'Could not launch ' +
                          "https://www.instagram.com/" +
                          usuarioInstagram;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/instagram.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.freelance.urlFb != "",
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch(widget.freelance.urlFb)) {
                          await launch(widget.freelance.urlFb);
                        } else {
                          throw 'Could not launch ' + widget.freelance.urlFb;
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5, bottom: 5, left: 5, right: 5),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              "assets/facebook.png",
                              width: 27,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: widget.freelance.phone != "",
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () async {
                        var whatsappUrl;
                        if (Platform.isIOS) {
                          whatsappUrl =  "whatsapp://wa.me/"+widget.freelance.phone;
                        } else {
                          whatsappUrl  = "whatsapp://send?phone="+widget.freelance.phone;
                        }

                        if (await canLaunch(whatsappUrl)) {
                          await launch(whatsappUrl);
                        } else {

                          final snackBar = SnackBar(
                            content: Text("Error: Necesitas tener instalado Whatsapp"),
                          );


                          Scaffold.of(context).showSnackBar(snackBar);

                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5, bottom: 5, left: 5, right: 5),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              "assets/whatsapp.png",
                              width: 27,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: widget.freelance.mail != "",
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () async {
                        var correoUrl = 'mailto:' +
                            widget.freelance.mail +
                            '?subject=CaxenApp';

                        if (await canLaunch(correoUrl)) {
                          await launch(correoUrl);
                        } else {
                          throw 'Could not launch ' + widget.freelance.email;
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5, bottom: 5, left: 5, right: 5),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              "assets/gmail.png",
                              width: 27,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: widget.freelance.urlLinkedin != "",
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch('https://'+widget.freelance.urlLinkedin)) {
                          await launch('https://'+widget.freelance.urlLinkedin);
                        } else {
                          throw 'Could not launch ' + 'https://'+widget.freelance.urlLinkedin;
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5, bottom: 5, left: 5, right: 5),
                        child: Image.asset(
                          "assets/linkedin.png",
                          width: 20,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: widget.freelance.latitude != null && widget.freelance.longitude != null,
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                OnlyFreelanceMapScreen(freelance: widget.freelance,)));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/marker.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    );
  }



  Widget divisor() {
    return Divider(
      height: 15,
      color: Colors.black54,
    );
  }
}
