import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/oficios/screens/oficio_crear_screen.dart';
import 'package:app/modulos/oficios/widgets/oficios_list_tile.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class OficiosListView extends StatefulWidget {
  final String categoria;
  final String region;

  OficiosListView({@required this.categoria, @required this.region});

  @override
  OficiosListViewState createState() => OficiosListViewState();
}

class OficiosListViewState extends State<OficiosListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("oficio");
  List<Oficio> lists = <Oficio>[];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) {
        return Expanded(
            child: Column(children: <Widget>[
          Expanded(
              child: FutureBuilder(
                  future: dbRef.once(),
                  builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      lists.clear();
                      Map<dynamic, dynamic> values = snapshot.data.value;

                      if (values == null) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.sadTear,
                                  size: 80,
                                  color: Colors.black45,
                                ),
                                Text("No hay oficios"),
                                this._botonSubirOficio()
                              ],
                            ),
                          ),
                        );
                      } else {
                        List<dynamic> list = values.values.toList();

                        list.sort((a, b) {
                          return b["date"].compareTo(a['date']);
                        });

                        list.forEach((element) {
                          Oficio oficio = Oficio.fromSnapshot(element);

                          if (widget.categoria == "Todas" && oficio.visible) {
                            if (widget.region == "Todas") {
                              lists.add(Oficio.fromSnapshot(element));
                            } else {
                              if (widget.region == oficio.region) {
                                lists.add(Oficio.fromSnapshot(element));
                              }
                            }
                          } else {
                            if (widget.region == "Todas" && oficio.visible) {
                              if (widget.categoria == oficio.category) {
                                print("aqui");
                                lists.add(Oficio.fromSnapshot(element));
                              }
                            } else {
                              if (widget.categoria == oficio.category &&
                                  widget.region == oficio.region) {
                                lists.add(Oficio.fromSnapshot(element));
                              }
                            }
                          }
                        });

                        return (lists.length == 0)
                            ? Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Center(
                                  child: Column(
                                    children: <Widget>[
                                      FaIcon(
                                        FontAwesomeIcons.sadTear,
                                        size: 80,
                                        color: Colors.black45,
                                      ),
                                      Text("No hay oficios"),
                                      this._botonSubirOficio()
                                    ],
                                  ),
                                ),
                              )
                            : new ListView.builder(
                                shrinkWrap: true,
                                itemCount: lists.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return OficiosListTile(
                                    usuario: globalController.usuario,
                                    oficio: lists[index],
                                  );
                                });
                      }
                    }

                    return Center(
                      child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator()),
                    );
                  }))
        ]));
      },
    );
  }

  Widget _botonSubirOficio() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar tu Oficio',
            style: TextStyle(fontSize: 20),
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OficioCrearScreen(
                      region: widget.region,
                      categoria: widget.categoria,
                    )),
          );
        },
      ),
    );
  }
}
