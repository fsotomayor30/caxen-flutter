import 'dart:io';

import 'package:app/modelos/oficio.dart';
import 'package:app/modulos/oficios/screens/oficios_list_screen.dart';
import 'package:app/servicios/firebase_storage_service.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/pages/camera.page.dart';
import 'package:app/shared/widgets/categorias_oficios_form_drop_down.dart';
import 'package:app/shared/widgets/region_form_drop_down.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:latlong/latlong.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:progress_dialog/progress_dialog.dart';

class OficioForm extends StatefulWidget {
  final Oficio oficio;

  OficioForm({@required this.oficio});

  @override
  _OficioScreenState createState() => _OficioScreenState();
}

class _OficioScreenState extends State<OficioForm> {
  ProgressDialog progressDialog;

  final _formKey = GlobalKey<FormState>();

  bool certification = false;

  File imagen1;

  int _radioValueExperiencia = 0;

  final dbRef = FirebaseDatabase.instance.reference().child("oficio");
  FirebaseStorageService firebaseStorageService = FirebaseStorageService();

  Oficio _oficio;

  LatLng _center;

  MapController _mapController = MapController();

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValueExperiencia = value;
      switch (value) {
        case 0:
          _oficio.experience = "0 - 10 años";
          break;
        case 1:
          _oficio.experience = "10 + años";
          break;
        case 2:
          _oficio.experience = "20 + años";
          break;
        case 3:
          _oficio.experience = "30 + años";
          break;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _oficio = widget.oficio;
      if(_oficio.address!=null){
        _center = LatLng(_oficio.latitude, _oficio.longitude);

      }
    });

    if (_oficio.certification == null) {
      _oficio.certification = false;
    }

    if (_oficio.experience == null) {
      setState(() {
        _oficio.experience = "0 - 10 años";
      });
    } else {
      setState(() {
        switch (_oficio.experience) {
          case '0 - 10 años':
            _radioValueExperiencia = 0;
            break;
          case '10 + años':
            _radioValueExperiencia = 1;
            break;
          case '20 + años':
            _radioValueExperiencia = 2;
            break;
          case '30 + años':
            _radioValueExperiencia = 3;
            break;
        }
      });
    }
  }

  Future escogerImagen1(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen1 = image;
    });

    if (imageSource == ImageSource.gallery) {
      _oficio.source = "gallery";
    } else {
      _oficio.source = "camera";
    }
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Publicando...");

    return Container(
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
            child: Form(
                key: this._formKey,
                child: Column(
                  children: <Widget>[
                    this._nombreOficioInput(),
                    this._apellidoOficioInput(),
                    this._regionDropDown(width),
                    SizedBox(height: 20),
                    this._direccionOficioInput(),
                    Visibility(
                      visible: _oficio.address != null,
                      child: this._mapAddress(),
                    ),
                    SizedBox(height: 20),
                    this._categoriasDropDown(width),
                    this._emailOficioInput(),
                    this._telefonoOficioInput(),
                    this._instagramOficioInput(),
                    this._descripcionOficioInput(),
                    this._experienciaRadioButton(width),
                    this._fotosInput(width),
                    this._certificacionInput(width),
                    this._botonGuardar()
                  ],
                ))));
  }

  Widget _direccionOficioInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.streetView, size: 25,
                  color: Colors.black45,),
                labelText: 'Dirección del oficio',
                hintText: 'Alameda 412, Santiago'),
            initialValue: _oficio.address,
            onChanged: (String value) {
              setState(() {
                if(value.isEmpty){
                  _oficio.address = null;
                }else{
                  _oficio.address = value;
                }
              });

            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: new OutlineButton(
            shape: StadiumBorder(),
            textColor: OFICIOS_COLOR,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Buscar',
                style: TextStyle(fontSize: 10),
              ),
            ),
            borderSide: BorderSide(
                color: OFICIOS_COLOR,
                style: BorderStyle.solid,
                width: 1),
            onPressed: () async {
              if (_oficio.address==null) {
                final snackBar = SnackBar(
                  content: Text("Error: Para buscar ingresa la dirección"),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              }else{
                try{
                  List<Placemark> placemark = await Geolocator().placemarkFromAddress(_oficio.address);
                  _mapController.move(LatLng(placemark[0].position.latitude, placemark[0].position.longitude), 16);
                  setState(() {
                    _center = LatLng(placemark[0].position.latitude, placemark[0].position.longitude);
                  });
                }catch(e){
                  final snackBar = SnackBar(
                    content: Text("Error: No encontramos ninguna dirección"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);
                }

              }


            },
          ),
        )
      ],
    );
  }

  Widget _mapAddress(){
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width - 40,
      child: FlutterMap(
        options: MapOptions(
          center: _center,
          zoom: 16.0,
        ),
        mapController: _mapController,
        layers: [
          new TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']
          ),
          MarkerLayerOptions(
              markers: [ Marker(
                height: 45,
                width: 45,
                point: _center,
                builder: (ctx) => Container(
                    height: 45,
                    width: 45,
                    child: _buildMarker()
                ),
              )]
          ),
          // ADD THIS
        ],

      ),
    );
  }

  Widget _buildMarker() {
    return Container(
        width: 300.0,
        height: 300.0,
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(110.0),
        ),
        child: Center(
            child:
            lottie.Lottie.asset('assets/marker.json',width: 40)


        ));
  }

  Widget _experienciaRadioButton(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: FaIcon(
                FontAwesomeIcons.clock,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Text('Experiencia'),
                        )
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Radio(
                          value: 0,
                          groupValue: _radioValueExperiencia,
                          onChanged: _handleRadioValueChange,
                        ),
                        new Text(
                          '0 - 10 años',
                          style: new TextStyle(fontSize: 15.0),
                        ),
                        new Radio(
                          value: 1,
                          groupValue: _radioValueExperiencia,
                          onChanged: _handleRadioValueChange,
                        ),
                        new Text(
                          '10 + años',
                          style: new TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Radio(
                          value: 2,
                          groupValue: _radioValueExperiencia,
                          onChanged: _handleRadioValueChange,
                        ),
                        new Text(
                          '20 + años',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                        new Radio(
                          value: 3,
                          groupValue: _radioValueExperiencia,
                          onChanged: _handleRadioValueChange,
                        ),
                        new Text(
                          '30 + años',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _categoriasDropDown(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.list,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    CategoriaOficiosFormDropDown(
                        initValue: _oficio.category,
                        onChanged: (String category) {
                          _oficio.category = category;
                        })
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _fotosInput(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.image,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            width: width - 80,
            child: Card(
              elevation: 0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.black38, width: 1),
                borderRadius: BorderRadius.circular(5),
              ),
              margin: EdgeInsets.all(0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen1 == null)
                              ? (_oficio.foto1 == null)
                                  ? Container(
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      decoration: BoxDecoration(
                                          color: Colors.black45,
                                          border: Border.all(
                                              color: Colors.black, width: 1),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.image,
                                            size: 40,
                                          ),
                                          Text(
                                            "Tu mejor selfie",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.black54),
                                          )
                                        ],
                                      ),
                                    )
                                  : Image.network(
                                      _oficio.foto1,
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      loadingBuilder: (BuildContext context,
                                          Widget child,
                                          ImageChunkEvent loadingProgress) {
                                        if (loadingProgress == null)
                                          return child;
                                        return Container(
                                          width: ((width - 80) / 2) - 15,
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              value: loadingProgress
                                                          .expectedTotalBytes !=
                                                      null
                                                  ? loadingProgress
                                                          .cumulativeBytesLoaded /
                                                      loadingProgress
                                                          .expectedTotalBytes
                                                  : null,
                                            ),
                                          ),
                                        );
                                      },
                                    )
                              : Image.file(
                                  imagen1,
                                  width: ((width - 80) / 2) - 15,
                                  height: (width - 80) / 2,
                                )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 1"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: Colors.blue,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: Colors.blue,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Tu mejor selfie',
                                        orientation: 'vertical',
                                        color: OFICIOS_COLOR,
                                        onChanged: (String path){
                                          setState(() {
                                            imagen1 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: Colors.blue,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: Colors.blue,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen1(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _certificacionInput(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.insert_drive_file,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            width: width - 80,
            child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 5, right: 5, top: 10),
                      child: AutoSizeText(
                        "Cuento con recomendaciones o certificación: ",
                        textAlign: TextAlign.center,
                        //maxFontSize: 15,
                        //minFontSize: 8 ,
                      ),
                    ),
                    Checkbox(
                      value: _oficio.certification,
                      onChanged: (bool value) {
                        setState(() {
                          certification = value;
                          _oficio.certification = value;
                        });
                      },
                    ),
                  ],
                )),
          )
        ],
      ),
    );
  }

  Widget _regionDropDown(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                FontAwesomeIcons.mapMarkerAlt,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    RegionFormDropDown(
                        initValue: _oficio.region,
                        onChanged: (String region) {
                          _oficio.region = region;
                        })
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _nombreOficioInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 30,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.handPointer),
                labelText: 'Nombre',
                hintText: 'Nombre'),
            initialValue: _oficio.firstName,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (value.length > 30) {
                return 'La cantidad de letras debe ser menor a 30';
              }
              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              return null;
            },
            onSaved: (String value) {
              _oficio.firstName = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _descripcionOficioInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 50,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.handPointer),
                labelText: 'Descripción',
                hintText: 'Descripción'),
            initialValue: _oficio.descripcion,
            validator: (value) {
              if (_oficio.category == "Otros" && value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (_oficio.category == "Otros" && value.length > 50) {
                return 'La cantidad de letras debe ser menor a 50';
              }
              return null;
            },
            onSaved: (String value) {
              _oficio.descripcion = value;
            },
          ),
        ),
        Visibility(
          visible: _oficio.category == "Otros",
          child: Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(
              "* requerido",
              textAlign: TextAlign.right,
              style: TextStyle(color: Colors.red, fontSize: 10),
            ),
          ),
        )
      ],
    );
  }

  Widget _apellidoOficioInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 30,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.handPointer),
                labelText: 'Apellido',
                hintText: 'Apellido'),
            initialValue: _oficio.lastName,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (value.length > 30) {
                return 'La cantidad de letras debe ser menor a 30';
              }
              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              return null;
            },
            onSaved: (String value) {
              _oficio.lastName = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _emailOficioInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            icon: Icon(FontAwesomeIcons.envelope),
            labelText: 'Correo Electrónico',
            hintText: 'oficio@oficio.com'),
        initialValue: _oficio.mail,
        onSaved: (String value) {
          _oficio.mail = value;
        },
        validator: (value) {
          if (value.length > 1 && !EmailValidator.validate(value)) {
            return 'Ingrese un correo válido';
          }
          return null;
        },
      ),
    );
  }

  Widget _telefonoOficioInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            keyboardType: TextInputType.phone,
            maxLength: 12,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.phone),
                labelText: 'Número de celular',
                hintText: '+569 XX XX XX XX'),
            initialValue: _oficio.phone,
            validator: (value) {
              if (value.length > 12) {
                return 'El largo máximo es 12';
              }
              if (value.length < 12) {
                return 'El largo mínimo es 12';
              }

              if (value[0] != "+") {
                return 'Debe comenzar con +';
              }

              if (value.length > 4 && value.substring(1, 4) != "569") {
                return 'Debe contener 569';
              }

              return null;
            },
            onSaved: (String value) {
              _oficio.phone = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 40, bottom: 10),
          child: Text(
            "El número ingresado debe contener +569 más 8 digitos, por ejemplo, +569 12345678",
            style: TextStyle(color: Colors.black45, fontSize: 10),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _instagramOficioInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.instagram),
                labelText: 'Usuario Instagram',
                hintText: '@caxen.app'),
            initialValue: _oficio.urlInstagram,
            onSaved: (String value) {
              _oficio.urlInstagram = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left:15,bottom: 10),
          child: Text(
            "El usuario ingresado debe estar en el siguiente formato @caxen.app",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _botonGuardar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar',
            style: TextStyle(fontSize: 30),
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();

            await progressDialog.show();

            if (_oficio.uuid == null) {
              logger.d("OFICIO A GUARDAR: " + _oficio.toString());

              var id = dbRef.push().key;

              String foto1;
              if (imagen1 != null) {
                foto1 = await firebaseStorageService.uploadFile(imagen1,
                    "caxen_foto1", "oficioImages/" + _oficio.email + "/" + id);
              } else {
                foto1 = null;
              }

              if(_oficio.address!= null){
                setState(() {
                  _oficio.latitude = _center.latitude;
                  _oficio.longitude = _center.longitude;
                });
              }

              var oficio = {
                //USUARIO
                "imagePerfil": _oficio.imagePerfil,
                "name": _oficio.name,
                "email": _oficio.email,
                //OFICIO
                "firstName": _oficio.firstName,
                "lastName": _oficio.lastName,
                "region": (_oficio.region == "Todas")
                    ? "Región de Arica y Parinacota"
                    : _oficio.region,
                "category": (_oficio.category == "Todas")
                    ? "Shows, bandas y cantantes"
                    : _oficio.category,
                "mail": (_oficio.mail == null) ? "" : _oficio.mail,
                "date": Timestamp.now().millisecondsSinceEpoch.toString(),
                "experience": _oficio.experience,
                "foto1": foto1,
                "phone": _oficio.phone,
                "uuid": id,
                "source": _oficio.source,
                "certification": _oficio.certification,
                "visible": true,
                "descripcion": _oficio.descripcion,
                "address" : _oficio.address == null ? null : _oficio.address,
                "latitude" : _oficio.latitude == null ? null : _oficio.latitude,
                "longitude" : _oficio.longitude == null ? null : _oficio.longitude,
                "urlInstagram": (_oficio.urlInstagram == null) ? "" : _oficio.urlInstagram,

              };

              await dbRef.child(id).set(oficio).then((_) async {
                await progressDialog.hide();
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => OficiosListScreen(
                            categoria: "Todas",
                          )),
                );
              }).catchError((onError) async {
                await progressDialog.hide();

                final snackBar = SnackBar(
                  content: Text("Error: " + onError.toString()),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              });
            } else {
              String foto1;
              (imagen1 == null)
                  ? foto1 = _oficio.foto1
                  : foto1 = await firebaseStorageService.uploadFile(
                      imagen1,
                      "caxen_foto1",
                      "oficioImages/" + _oficio.email + "/" + _oficio.uuid);

              if(_oficio.address!= null){
                setState(() {
                  _oficio.latitude = _center.latitude;
                  _oficio.longitude = _center.longitude;
                });
              }

              var oficio = {
                //USUARIO
                "imagePerfil": _oficio.imagePerfil,
                "name": _oficio.name,
                "email": _oficio.email,
                //OFICIO
                "firstName": _oficio.firstName,
                "lastName": _oficio.lastName,
                "region": (_oficio.region == "Todas")
                    ? "Región de Arica y Parinacota"
                    : _oficio.region,
                "category": (_oficio.category == "Todas")
                    ? "Shows, bandas y cantantes"
                    : _oficio.category,
                "mail": (_oficio.mail == null) ? "" : _oficio.mail,
                "date": Timestamp.now().millisecondsSinceEpoch.toString(),
                "experience": _oficio.experience,
                "foto1": foto1,
                "phone": _oficio.phone,
                "uuid": _oficio.uuid,
                "source": _oficio.source,
                "certification": _oficio.certification,
                "visible": _oficio.visible,
                "descripcion": _oficio.descripcion,
                "address" : _oficio.address == null ? null : _oficio.address,
                "latitude" : _oficio.latitude == null ? null : _oficio.latitude,
                "longitude" : _oficio.longitude == null ? null : _oficio.longitude,
                "urlInstagram": (_oficio.urlInstagram == null) ? "" : _oficio.urlInstagram,

              };
              await dbRef.child(_oficio.uuid).set(oficio).then((_) async {
                await progressDialog.hide();
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => OficiosListScreen(
                            categoria: "Todas",
                          )),
                );
              }).catchError((onError) async {
                await progressDialog.hide();
                final snackBar = SnackBar(
                  content: Text("Error: " + onError.toString()),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              });
            }
          }
        },
      ),
    );
  }
}

void fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}
