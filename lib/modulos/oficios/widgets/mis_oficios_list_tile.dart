import 'dart:io';

import 'package:app/funciones/oficio_funciones.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/oficios/screens/oficio_actualizar_screen.dart';
import 'package:app/modulos/oficios/screens/one_oficio_map_screen.dart';
import 'package:app/modulos/usuario/screens/user_profile_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/pages/payment.page.dart';
import 'package:app/shared/widgets/tiempo_atras.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class MisOficiosListTile extends StatefulWidget {
  final Oficio oficio;
  final Usuario usuario;

  MisOficiosListTile({@required this.oficio, @required this.usuario});

  @override
  _MisOficiosListTileState createState() => _MisOficiosListTileState();
}

class _MisOficiosListTileState extends State<MisOficiosListTile> {
  final OficiosFunciones oficiosFunciones = OficiosFunciones();

  ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Editando...");

    final mediaQueryData = MediaQuery.of(context).size;

    return Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Card(
              elevation: 10,
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 8, bottom: 10, left: 8, right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    this._informacionAutor(mediaQueryData.width),
                    this._nombreText(),
                    SizedBox(
                      height: 10,
                    ),
                    this._oficioText(),
                    SizedBox(
                      height: 10,
                    ),
                    Visibility(
                        visible: widget.oficio.descripcion != null,
                        child: this._descripcionText()
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    this._experienciaText(),
                    this._certificacionText(mediaQueryData.width),
                    SizedBox(
                      height: 20,
                    ),
                    Visibility(
                        visible: widget.oficio.foto1 == null,
                        child: this._noImagen(mediaQueryData.width)
                    ),
                    Visibility(
                        visible: widget.oficio.foto1 != null,
                        child: this._imagenes(mediaQueryData.width)
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        this._seccionEditar(widget.oficio),
                        this._seccionEliminar(widget.oficio, context)
                      ],
                    ),
                    this.divisor(),
                    this._contacto(),
                  ],
                ),
              ),
            ),

        );
  }

  Widget _informacionAutor(double width) {
    return ListTile(
        contentPadding: EdgeInsets.all(0),
        leading: Container(
          width: (width * 0.20) - 20,
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: (widget.oficio.imagePerfil != '') ? Image.network(widget.oficio.imagePerfil)
                  : Container(
                width: 55,
                decoration : BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    border: Border.all(width: 3,color: OFICIOS_COLOR,style: BorderStyle.solid)
                ),
                child: Center(child: Text(widget.oficio.email[0].toUpperCase(), style: TextStyle(color: OFICIOS_COLOR, fontSize: 25),)),)),
        ),
        title: Text(
          widget.oficio.name + ":",
          style: TextStyle(fontSize: 14),
        ),
        subtitle: TiempoAtrasWidget(fecha: widget.oficio.date,),
      trailing: widget.usuario.email == widget.oficio.email ? OutlineButton(
        shape: StadiumBorder(),
        textColor: OFICIOS_COLOR,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Quiero llegar \na más personas',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 10),
          ),
        ),
        borderSide: BorderSide(
            color: OFICIOS_COLOR,
            style: BorderStyle.solid,
            width: 1),
        onPressed: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PaymentScreen(title: 'Promociona tu oficio', color: OFICIOS_COLOR,)),
          );

        },
      ) : Container(width: 0, height: 0,),
    );
  }

  Widget _nombreText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.idCard,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Nombre: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.firstName + " "+widget.oficio.lastName)
        ],
      ),
    );
  }

  Widget _oficioText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.bookmark,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Oficio: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.category)
        ],
      ),
    );
  }

  Widget _experienciaText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.clock,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Experiencia: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.experience, style: TextStyle(fontSize: 13))
        ],
      ),
    );
  }

  Widget _certificacionText(double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: 30,
                child: Checkbox(
                  value: widget.oficio.certification,
                  onChanged: (bool value) {

                  },
                ),
              ),
              Container(
                width: width - 94,
                child: Text(
                  "Cuento con recomendaciones o certificación",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _noImagen(double width) {
    return Center(
        child: (widget.oficio.foto1 == null) ? Image.asset('assets/person-oficio.jpg', width: width, height: width*.5,) : Container()
    );
  }

  Widget _imagenes(double width) {

    return widget.oficio.foto1 == null ? Container() :Center(
      child: PinchZoomImage(
        image: Image.network(
          widget.oficio.foto1,
          width: width,
          fit: BoxFit.contain,
          loadingBuilder: (BuildContext context, Widget child,
              ImageChunkEvent loadingProgress) {
            if (loadingProgress == null) return child;
            return Center(
              child: CircularProgressIndicator(
                value: loadingProgress.expectedTotalBytes != null
                    ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes
                    : null,
              ),
            );
          },
        ),
        zoomedBackgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
        hideStatusBarWhileZooming: true,
        onZoomStart: () {
          print('Zoom started');
        },
        onZoomEnd: () {
          print('Zoom finished');
        },
      ),

    );
  }

  Widget _seccionEliminar(Oficio oficio, BuildContext ctx) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/eliminar.png",
                  width: 20,
                ),
              ),
              Text(
                'Eliminar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          final snackBar = SnackBar(
            content: Text('¿Estás seguro de eliminarlo?'),
            action: SnackBarAction(
              label: 'Confirmar',
              onPressed: () {
                this.eliminar(oficio, ctx);
              },
            ),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(ctx).showSnackBar(snackBar);
        },
      ),
    );
  }

  Widget _seccionEditar(Oficio oficio) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/editar.png",
                  width: 20,
                ),
              ),
              Text(
                'Editar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    OficioActualizarScreen(
                      oficio: widget.oficio,
                    )),
          );
        },
      ),
    );
  }

  Widget seccionEliminar(Oficio oficio, BuildContext ctx) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/eliminar.png",
                  width: 20,
                ),
              ),
              Text(
                'Eliminar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          final snackBar = SnackBar(
            content: Text('¿Estás seguro de eliminarlo?'),
            action: SnackBarAction(
              label: 'Confirmar',
              onPressed: () {
                this.eliminar(oficio, ctx);
              },
            ),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(ctx).showSnackBar(snackBar);
        },
      ),
    );
  }

  void eliminar(Oficio oficio, BuildContext ctx) async {
    await progressDialog.show();


    try {
      await oficiosFunciones.eliminar(oficio);
      await progressDialog.hide();

      Navigator.of(ctx).pop();
      Navigator.of(ctx).pop();
      Navigator.push(
          context, MaterialPageRoute(builder: (ctx) => UserProfileScreen()));
    } catch (e) {
      await progressDialog.hide();

      final snackBar = SnackBar(
        content: Text("Error: "+e.toString()),
      );


      Scaffold.of(ctx).showSnackBar(snackBar);


    }
  }

  Widget seccionEditar(Oficio oficio) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/editar.png",
                  width: 20,
                ),
              ),
              Text(
                'Editar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
        BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    OficioActualizarScreen(
                      oficio: oficio,
                    )),
          );
        },
      ),
    );
  }

  Widget _descripcionText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.list,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Descripción: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.descripcion==null ? '': widget.oficio.descripcion)
        ],
      ),
    );
  }

  Widget _contacto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          SizedBox(
            height: 2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: widget.oficio.phone != "",
                child: GestureDetector(
                  onTap: () async {
                    var whatsappUrl;
                    if (Platform.isIOS) {
                      whatsappUrl =  "whatsapp://wa.me/"+widget.oficio.phone;
                    } else {
                      whatsappUrl  = "whatsapp://send?phone="+widget.oficio.phone;
                    }


                    if (await canLaunch(whatsappUrl)) {
                      await launch(whatsappUrl);
                    } else {

                      final snackBar = SnackBar(
                        content: Text("Error: Necesitas tener instalado Whatsapp"),
                      );


                      Scaffold.of(context).showSnackBar(snackBar);

                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Image.asset(
                      "assets/whatsapp.png",
                      width: 27,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.oficio.mail != "",
                child: GestureDetector(
                  onTap: () async {
                    var correoUrl = 'mailto:' +
                        widget.oficio.mail +
                        '?subject=CaxenApp';

                    if (await canLaunch(correoUrl)) {
                      await launch(correoUrl);
                    } else {
                      throw 'Could not launch ' + widget.oficio.email;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Image.asset(
                      "assets/gmail.png",
                      width: 27,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.oficio.latitude != null && widget.oficio.longitude != null,
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                OnlyOficioMapScreen(oficio: widget.oficio,)));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/marker.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.oficio.urlInstagram != null && widget.oficio.urlInstagram != "" ,
                child: GestureDetector(
                  onTap: () async {
                    String usuarioInstagram = widget.oficio.urlInstagram;
                    usuarioInstagram = usuarioInstagram.replaceAll("@", "");

                    if (await canLaunch("https://www.instagram.com/" +
                        usuarioInstagram)) {
                      await launch("https://www.instagram.com/" +
                          usuarioInstagram);
                    } else {
                      throw 'Could not launch ' +
                          "https://www.instagram.com/" +
                          usuarioInstagram;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/instagram.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    );
  }



  Widget divisor() {
    return Divider(
      height: 15,
      color: Colors.black54,
    );
  }
}
