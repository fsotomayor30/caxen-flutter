import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/oficios/widgets/oficio_form.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/styles/style_app_bar.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OficioActualizarScreen extends StatefulWidget {
  final Oficio oficio;

  OficioActualizarScreen({@required this.oficio});

  @override
  _OficioActualizarScreenState createState() => _OficioActualizarScreenState();
}

class _OficioActualizarScreenState extends State<OficioActualizarScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: OFICIOS_COLOR,
          centerTitle: true,
          title: Text("Oficio", style: styleAppBar),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                _dialogAyuda();
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(
                  Icons.help,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            )
          ],
        ),
        body: OficioForm(oficio: widget.oficio));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aqui puedes actualizar la información de tu oficio",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
