import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/oficios/widgets/mis_oficios_list_view.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';

class MisOficiosListScreen extends StatefulWidget {
  final Usuario usuario;

  MisOficiosListScreen({@required this.usuario});

  @override
  _MisOficiosListState createState() => _MisOficiosListState();
}

class _MisOficiosListState extends State<MisOficiosListScreen>{
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    final double width = MediaQuery.of(context).size.width - 20;
    final double height = MediaQuery.of(context).size.height - 20;
    return Scaffold(
        appBar: AppBar(
            backgroundColor: OFICIOS_COLOR,
            title: Text("Mis Oficios"),
            centerTitle: true,
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  _dialogAyuda();
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Icon(
                    Icons.help,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              ),
            ]),
        body: Container(
            width: mediaQueryData.width,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MisOficiosListView(
                    usuario: widget.usuario,
                  ),
                ],
              ),
            )));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes ver todos tus oficios disponibles",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
