import 'dart:io';

import 'package:app/funciones/calificacion_oficio_funciones.dart';
import 'package:app/funciones/like_oficio_funciones.dart';
import 'package:app/funciones/oficio_funciones.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/calificaciones/oficios/screens/calificaciones_list_screen.dart';
import 'package:app/modulos/comentarios/oficios/screens/comentarios_list_screen.dart';
import 'package:app/modulos/oficios/screens/one_oficio_map_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/widgets/tiempo_atras.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:like_button/like_button.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:url_launcher/url_launcher.dart';

class OficioDetailListScreen extends StatefulWidget {
  final Oficio oficio;
  final Usuario usuario;

  OficioDetailListScreen({@required this.oficio, @required this.usuario});

  @override
  _OficioDetailListState createState() => _OficioDetailListState();
}

class _OficioDetailListState extends State<OficioDetailListScreen> {
  final OficiosFunciones oficiosFunciones = OficiosFunciones();
  final CalificacionOficiosFunciones calificacionOficiosFunciones =
      CalificacionOficiosFunciones();
  final LikeOficioFunciones likeOficioFunciones = LikeOficioFunciones();

  int like;
  bool conLike;

  //-1 = no freelance
  //0 = sin like
  // >0 = con like

  @override
  void initState() {
    super.initState();
    likeOficioFunciones.cantidadLike(widget.oficio.uuid).then((result) {
      if (mounted) {
        setState(() {
          like = result;
        });
      }
    });

    likeOficioFunciones
        .conLike(widget.usuario, widget.oficio.uuid)
        .then((result) {
      if (mounted) {
        setState(() {
          if (result > 0) {
            conLike = true;
          } else {
            conLike = false;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: OFICIOS_COLOR,
        title: Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Container(
              width: mediaQueryData.width / 2,
              child: AutoSizeText(
                "Oficios",
                textAlign: TextAlign.center,
                maxFontSize: 18,
                minFontSize: 10,
              )),
        ),
        centerTitle: true,
        actions: <Widget>[],
      ),
      body: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: Card(
          elevation: 5,
          child: Padding(
            padding:
                const EdgeInsets.only(top: 8, bottom: 10, left: 8, right: 8),
            child: ListView(
              children: <Widget>[
                this._informacionAutor(mediaQueryData.width),
                this._nombreText(),
                SizedBox(
                  height: 10,
                ),
                this._oficioText(),
                SizedBox(
                  height: 10,
                ),
                Visibility(
                    visible: widget.oficio.descripcion != null,
                    child: this._descripcionText()),
                SizedBox(
                  height: 10,
                ),
                this._experienciaText(),
                SizedBox(
                  height: 10,
                ),
                this._certificacionText(mediaQueryData.width),
                SizedBox(
                  height: 20,
                ),
                Visibility(
                    visible: widget.oficio.foto1 == null,
                    child: this._noImagen(mediaQueryData.width)),
                Visibility(
                    visible: widget.oficio.foto1 != null,
                    child: this._imagenes(mediaQueryData.width)),
                SizedBox(
                  height: 20,
                ),
                this._acciones(mediaQueryData.width),
                this._divisor(),
                this._contacto(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _informacionAutor(double width) {
    return ListTile(
        contentPadding: EdgeInsets.all(0),
        leading: Container(
          width: (width * 0.20) - 20,
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: (widget.oficio.imagePerfil != '')
                  ? Image.network(widget.oficio.imagePerfil)
                  : Container(
                      width: 55,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          border: Border.all(
                              width: 3,
                              color: OFICIOS_COLOR,
                              style: BorderStyle.solid)),
                      child: Center(
                          child: Text(
                        widget.oficio.email[0].toUpperCase(),
                        style: TextStyle(color: OFICIOS_COLOR, fontSize: 25),
                      )),
                    )),
        ),
        title: Text(
          widget.oficio.name + ":",
          style: TextStyle(fontSize: 14),
        ),
        subtitle: TiempoAtrasWidget(
          fecha: widget.oficio.date,
        ));
  }

  Widget _nombreText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.idCard,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Nombre: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.firstName + " " + widget.oficio.lastName)
        ],
      ),
    );
  }

  Widget _descripcionText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.list,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Descripción: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.descripcion == null
              ? ''
              : widget.oficio.descripcion)
        ],
      ),
    );
  }

  Widget _oficioText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.bookmark,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Oficio: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.category)
        ],
      ),
    );
  }

  Widget _certificacionText(double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: 30,
                child: Checkbox(
                  value: widget.oficio.certification,
                  onChanged: (bool value) {},
                ),
              ),
              Container(
                width: width - 94,
                child: Text(
                  "Cuento con recomendaciones o certificación",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _imagenes(double width) {
    return Center(
      child: (widget.oficio.foto1 == null)
          ? Container()
          : PinchZoomImage(
              image: Image.network(
                widget.oficio.foto1,
                width: width,
                fit: BoxFit.contain,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: CircularProgressIndicator(
                      value: loadingProgress.expectedTotalBytes != null
                          ? loadingProgress.cumulativeBytesLoaded /
                              loadingProgress.expectedTotalBytes
                          : null,
                    ),
                  );
                },
              ),
              zoomedBackgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
              hideStatusBarWhileZooming: true,
              onZoomStart: () {
                print('Zoom started');
              },
              onZoomEnd: () {
                print('Zoom finished');
              },
            ),
    );
  }

  Widget _noImagen(double width) {
    return Center(
        child: (widget.oficio.foto1 == null)
            ? Image.asset(
                'assets/person-oficio.jpg',
                width: width,
                height: width * .5,
              )
            : Container());
  }

  Widget _divisor() {
    return Divider(
      height: 15,
      color: Colors.black54,
    );
  }

  Widget _contacto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: widget.oficio.phone != "",
                child: GestureDetector(
                  onTap: () async {
                    var whatsappUrl;
                    if (Platform.isIOS) {
                      whatsappUrl = "whatsapp://wa.me/" + widget.oficio.phone;
                    } else {
                      whatsappUrl =
                          "whatsapp://send?phone=" + widget.oficio.phone;
                    }

                    if (await canLaunch(whatsappUrl)) {
                      await launch(whatsappUrl);
                    } else {
                      final snackBar = SnackBar(
                        content:
                            Text("Error: Necesitas tener instalado Whatsapp"),
                      );

                      Scaffold.of(context).showSnackBar(snackBar);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Image.asset(
                      "assets/whatsapp.png",
                      width: 27,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.oficio.mail != "",
                child: GestureDetector(
                  onTap: () async {
                    var correoUrl =
                        'mailto:' + widget.oficio.mail + '?subject=CaxenApp';

                    if (await canLaunch(correoUrl)) {
                      await launch(correoUrl);
                    } else {
                      throw 'Could not launch ' + widget.oficio.email;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Image.asset(
                      "assets/gmail.png",
                      width: 27,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.oficio.latitude != null &&
                    widget.oficio.longitude != null,
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OnlyOficioMapScreen(
                                  oficio: widget.oficio,
                                )));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/marker.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.oficio.urlInstagram != null &&
                    widget.oficio.urlInstagram != "",
                child: GestureDetector(
                  onTap: () async {
                    String usuarioInstagram = widget.oficio.urlInstagram;
                    usuarioInstagram = usuarioInstagram.replaceAll("@", "");

                    if (await canLaunch(
                        "https://www.instagram.com/" + usuarioInstagram)) {
                      await launch(
                          "https://www.instagram.com/" + usuarioInstagram);
                    } else {
                      throw 'Could not launch ' +
                          "https://www.instagram.com/" +
                          usuarioInstagram;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/instagram.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _seccionLike() {
    return LikeButton(
      isLiked: conLike,
      size: 30,
      circleColor:
          CircleColor(start: Color(0xff00ddff), end: Color(0xff0099cc)),
      bubblesColor: BubblesColor(
        dotPrimaryColor: Color(0xff33b5e5),
        dotSecondaryColor: Color(0xff0099cc),
      ),
      onTap: onLikeButtonTapped,
      likeBuilder: (bool isLiked) {
        return Icon(
          FontAwesomeIcons.solidHeart,
          color: isLiked ? Colors.red : Colors.grey,
          size: 20,
        );
      },
      likeCount: like,
      countBuilder: (int count, bool isLiked, String text) {
        var color = isLiked ? Colors.red : Colors.grey;
        Widget result;
        if (count == 0) {
          result = Text(
            "0",
            style: TextStyle(color: color, fontSize: 25),
          );
        } else
          result = Text(
            text,
            style: TextStyle(color: color, fontSize: 25),
          );
        return result;
      },
    );
  }

  Widget _seccionComentarios(Oficio oficio) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: GestureDetector(
        child: Image.asset(
          "assets/comentario.png",
          width: 27,
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ComentariosListScreen(
                      oficio: widget.oficio,
                    )),
          );
        },
      ),
    );
  }

  Widget _seccionCalificar(Oficio oficio, double width) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: GestureDetector(
        child: Image.asset(
          "assets/estrella.png",
          width: 27,
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CalificacionesListScreen(
                      oficio: oficio,
                    )),
          );
        },
      ),
    );
  }

  int selectedValue2;

  void onChange2(int value) {
    setState(() {
      selectedValue2 = value;
    });
  }

  Widget _acciones(double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              this._seccionLike(),
              SizedBox(
                width: 20.0,
              ),
              this._seccionComentarios(widget.oficio),
              /*SizedBox(
                width: 20.0,
              ),
              this._seccionCalificar(widget.oficio, width)*/
            ],
          ),
        ],
      ),
    );
  }

  Widget _experienciaText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.clock,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Experiencia: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.oficio.experience, style: TextStyle(fontSize: 13))
        ],
      ),
    );
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    GlobalController _globalController = Get.find();
    Usuario usuario = _globalController.usuario;
    if (!isLiked) {
      await likeOficioFunciones.agregarLike(widget.oficio.uuid, usuario);
    } else {
      await likeOficioFunciones.quitarLike(widget.oficio.uuid, usuario);
    }
    return !isLiked;
  }
}
