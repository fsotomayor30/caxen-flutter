import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/oficios/widgets/oficio_form.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/styles/style_app_bar.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OficioCrearScreen extends StatefulWidget {
  final String categoria;
  final String region;
  OficioCrearScreen({@required this.categoria, @required this.region});

  @override
  _OficioCrearScreenState createState() => _OficioCrearScreenState();
}

class _OficioCrearScreenState extends State<OficioCrearScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: OFICIOS_COLOR,
              centerTitle: true,
              title: Text(
                "Oficios",
                style: styleAppBar,
              ),
              actions: <Widget>[
                GestureDetector(
                  onTap: () {
                    _dialogAyuda();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: Icon(
                      Icons.help,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                )
              ],
            ),
            body: OficioForm(
                oficio: Oficio(
                    category: widget.categoria,
                    email: globalController.usuario.email,
                    region: widget.region,
                    imagePerfil: globalController.usuario.photoProfile,
                    name: globalController.usuario.displayName)));
      },
    );
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes publicar un nuevo oficio",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
