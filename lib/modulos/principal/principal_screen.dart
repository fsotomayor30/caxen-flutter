import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/autenticacion/screens/autenticacion_screen.dart';
import 'package:app/modulos/emprendimientos/screen/categorias_emprendimientos_list_screen.dart';
import 'package:app/modulos/freelances/screens/categorias_freelances_list_screen.dart';
import 'package:app/modulos/oficios/screens/categorias_oficios_list_screen.dart';
import 'package:app/modulos/usuario/screens/user_profile_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/styles/style_app_bar.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import "package:flutter/material.dart";
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrincipalScreen extends StatefulWidget {
  final Key key;

  PrincipalScreen({this.key});

  @override
  PrincipalScreenState createState() => PrincipalScreenState();
}

class PrincipalScreenState extends State<PrincipalScreen> {
  int _page = 0;

  @override
  void initState() {
    super.initState();
  }

  Widget _pageChooser(int page) {
    switch (page) {
      case 0:
        return CategoriasEmprendimientosListScreen();
        break;
      case 1:
        return CategoriasFreelancesListScreen();
        break;
      case 2:
        return CategoriasOficiosListScreen();
        break;
    }
  }

  String _titleChooser(int page) {
    switch (page) {
      case 0:
        return "Emprendimientos";
        break;
      case 1:
        return "Freelancers";
        break;
      case 2:
        return "Oficios";
        break;
    }
  }

  void changePage(int index) {
    setState(() {
      _page = index;
    });
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('¿Estás seguro?'),
            content: new Text('¿Quieres abandonarnos?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Si'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(70.0), // here the desired height
            child: AppBar(
              backgroundColor: (_page == 0)
                  ? EMPRENDIMIENTOS_COLOR
                  : (_page == 1)
                      ? FREELANCES_COLOR
                      : OFICIOS_COLOR,
              elevation: 0,
              centerTitle: true,
              title: Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text(
                  _titleChooser(_page),
                  style: styleAppBar,
                ),
              ),
              actions: <Widget>[
                GestureDetector(
                  onTap: () async {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    String uuid = prefs.getString('uuid');
                    String displayName = prefs.getString('displayName');
                    String email = prefs.getString('email');
                    String photoProfile = prefs.getString('photoProfile') ?? '';

                    if (uuid == null && displayName == null && email == null) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AutenticacionScreen()));
                    } else {
                      Usuario usuario = new Usuario(
                          uid: uuid,
                          displayName: displayName,
                          email: email,
                          photoProfile: photoProfile);
                      GlobalController _globalController = Get.find();
                      _globalController.setUsuario(usuario);

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UserProfileScreen()),
                      );
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30, right: 20),
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                )
              ],
            ),
          ),
          body: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/img_caxen_background.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: _pageChooser(_page)),
          bottomNavigationBar: FFNavigationBar(
            theme: FFNavigationBarTheme(
                barBackgroundColor: (_page == 0)
                    ? EMPRENDIMIENTOS_COLOR
                    : (_page == 1)
                        ? FREELANCES_COLOR
                        : OFICIOS_COLOR,
                selectedItemBackgroundColor: (_page == 0)
                    ? EMPRENDIMIENTOS_COLOR
                    : (_page == 1)
                        ? FREELANCES_COLOR
                        : OFICIOS_COLOR,
                selectedItemIconColor: Colors.white,
                selectedItemLabelColor: Colors.white,
                unselectedItemIconColor: Colors.white,
                unselectedItemLabelColor: Colors.white,
                selectedItemTextStyle: TextStyle(fontSize: 10),
                unselectedItemTextStyle: TextStyle(fontSize: 10)),
            selectedIndex: _page,
            onSelectTab: (index) {
              setState(() {
                _page = index;
              });
              return;
            },
            items: [
              FFNavigationBarItem(
                iconData: FontAwesomeIcons.rocket,
                label: 'Emprendimientos',
              ),
              FFNavigationBarItem(
                iconData: FontAwesomeIcons.bullhorn,
                label: 'Freelancers',
              ),
              FFNavigationBarItem(
                iconData: FontAwesomeIcons.handPointer,
                label: 'Oficios',
              ),
            ],
          ),
        ),
        onWillPop: _onWillPop);
  }
}
