import 'dart:async';

import 'package:app/modelos/calificacion_freelance.dart';
import 'package:app/modelos/freelance.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/calificaciones/frelances/widgets/calificaciones_list_tile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class CalificacionesListView extends StatefulWidget {
  final Freelance freelance;

  CalificacionesListView({@required this.freelance});

  @override
  CalificacionesListViewState createState() => CalificacionesListViewState();
}

class CalificacionesListViewState extends State<CalificacionesListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("freelanceQualify");

  List<CalificacionFreelance> lists = <CalificacionFreelance>[];

  StreamSubscription<Event> _onAddedSubscription;
  StreamSubscription<Event> _onChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onAddedSubscription =
        dbRef.child(widget.freelance.uuid).onChildAdded.listen(_childAdded);
    _onChangedSubscription =
        dbRef.child(widget.freelance.uuid).onChildChanged.listen(_childChanged);
  }

  @override
  void dispose() {
    _onAddedSubscription.cancel();
    _onChangedSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(children: <Widget>[
      Expanded(
          child: new ListView.builder(
              shrinkWrap: true,
              itemCount: lists.length,
              itemBuilder: (BuildContext context, int index) {
                return CalificacionesListTile(
                  calificacionFreelance: lists[index],
                );
              }))
    ]));
  }

  void _childAdded(Event event) {
    setState(() {
      lists.add(new CalificacionFreelance.fromSnapshot(event.snapshot.value));
    });
  }

  void _childChanged(Event event) {}
}
