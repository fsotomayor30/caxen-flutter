import 'package:app/modelos/calificacion_freelance.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class CalificacionesListTile extends StatefulWidget {
  final CalificacionFreelance calificacionFreelance;

  CalificacionesListTile({@required this.calificacionFreelance});

  @override
  CalificacionesListTileState createState() => CalificacionesListTileState();
}

class CalificacionesListTileState extends State<CalificacionesListTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final format = new DateFormat('yyyy-MM-dd hh:mm');

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          leading: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: (widget.calificacionFreelance.imagePerfil != '')
                  ? Image.network(widget.calificacionFreelance.imagePerfil)
                  : Container(
                      width: 55,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          border: Border.all(
                              width: 3,
                              color: FREELANCES_COLOR,
                              style: BorderStyle.solid)),
                      child: Center(
                          child: Text(
                        widget.calificacionFreelance.email[0].toUpperCase(),
                        style: TextStyle(color: FREELANCES_COLOR, fontSize: 25),
                      )),
                    )),
          title: SmoothStarRating(
            isReadOnly: true,
            rating: double.parse(widget.calificacionFreelance.qualify),
            size: 30,
            filledIconData: Icons.star,
            halfFilledIconData: Icons.star_half,
            defaultIconData: Icons.star_border,
            starCount: 5,
            allowHalfRating: false,
            spacing: 2.0,
            borderColor: FREELANCES_COLOR,
            color: FREELANCES_COLOR,
          ),
          subtitle: Text(
            format.format(widget.calificacionFreelance.date),
            style: TextStyle(color: Colors.black54, fontSize: 10),
          ),
        ),
      ),
    );
  }
}
