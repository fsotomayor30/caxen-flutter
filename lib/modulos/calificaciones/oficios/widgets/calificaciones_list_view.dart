import 'dart:async';

import 'package:app/modelos/calificacion_oficio.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/calificaciones/oficios/widgets/calificaciones_list_tile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class CalificacionesListView extends StatefulWidget {
  final Oficio oficio;

  CalificacionesListView({@required this.oficio});

  @override
  CalificacionesListViewState createState() => CalificacionesListViewState();
}

class CalificacionesListViewState extends State<CalificacionesListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("oficiosQualify");

  List<CalificacionOficio> lists = <CalificacionOficio>[];

  StreamSubscription<Event> _onAddedSubscription;
  StreamSubscription<Event> _onChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onAddedSubscription =
        dbRef.child(widget.oficio.uuid).onChildAdded.listen(_childAdded);
    _onChangedSubscription =
        dbRef.child(widget.oficio.uuid).onChildChanged.listen(_childChanged);
  }

  @override
  void dispose() {
    _onAddedSubscription.cancel();
    _onChangedSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(children: <Widget>[
      Expanded(
          child: new ListView.builder(
              shrinkWrap: true,
              itemCount: lists.length,
              itemBuilder: (BuildContext context, int index) {
                return CalificacionesListTile(
                  calificacionOficio: lists[index],
                );
              }))
    ]));
  }

  void _childAdded(Event event) {
    setState(() {
      lists.add(new CalificacionOficio.fromSnapshot(event.snapshot.value));
    });
  }

  void _childChanged(Event event) {}
}
