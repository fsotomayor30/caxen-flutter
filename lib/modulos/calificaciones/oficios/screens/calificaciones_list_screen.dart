import 'package:app/funciones/calificacion_oficio_funciones.dart';
import 'package:app/modelos/oficio.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/calificaciones/oficios/widgets/calificaciones_list_view.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class CalificacionesListScreen extends StatefulWidget {
  final Oficio oficio;

  CalificacionesListScreen({@required this.oficio});

  @override
  _CalificacionesListState createState() => _CalificacionesListState();
}

class _CalificacionesListState extends State<CalificacionesListScreen> {
  final dbRef = FirebaseDatabase.instance.reference().child("oficiosQualify");
  CalificacionOficiosFunciones calificacionOficioFunciones =
      new CalificacionOficiosFunciones();
  double rating = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
        builder: (GlobalController globalController) => Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text("Calificaciones"),
              backgroundColor: OFICIOS_COLOR,
            ),
            body: Container(
                width: Get.width,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CalificacionesListView(
                        oficio: widget.oficio,
                      ),
                      SizedBox(height: 10),
                      Text("Selecciona de 1 a 5 estrellas para calificar"),
                      SizedBox(height: 10),
                      RatingBar.builder(
                        initialRating: rating,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        unratedColor: Colors.black45,
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: OFICIOS_COLOR,
                        ),
                        onRatingUpdate: (rating) {
                          setState(() {
                            this.rating = rating;
                          });
                        },
                      ),
                      this._botonCalificar(globalController.usuario),
                    ],
                  ),
                ))));
  }

  Widget _botonCalificar(Usuario usuario) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Builder(
        builder: (context) => OutlineButton(
          shape: StadiumBorder(),
          textColor: OFICIOS_COLOR,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Calificar',
              style: TextStyle(fontSize: 30),
            ),
          ),
          borderSide: BorderSide(
              color: OFICIOS_COLOR, style: BorderStyle.solid, width: 1),
          onPressed: () async {
            int diasDiferencia = await calificacionOficioFunciones
                .ultimaCalificacion(widget.oficio, usuario);
            print("dias : " + diasDiferencia.toString());
            if (diasDiferencia < 30 && diasDiferencia != -1) {
              final snackBar = SnackBar(
                content: Text("Quieres volver a calificar? Envíanos un mail"),
              );
              Scaffold.of(context).showSnackBar(snackBar);
            } else {
              if (rating < 1) {
                final snackBar = SnackBar(
                  content: Text("La calificación minima es 1 estrella"),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              } else {
                await dbRef.child(widget.oficio.uuid).push().set({
                  "qualify": rating.toString(),
                  "email": usuario.email,
                  "oficioid": widget.oficio.uuid,
                  "imagePerfil": usuario.photoProfile,
                  "name": usuario.displayName,
                  "uuid": widget.oficio.uuid,
                  "date": DateTime.now().toString()
                }).then((_) {
                  setState(() {
                    this.rating = 0.0;
                  });
                  final snackBar = SnackBar(
                    content: Text("Calificación realizada con éxito"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);
                }).catchError((onError) {
                  final snackBar = SnackBar(
                    content: Text("Error: " + onError.toString()),
                  );

                  Scaffold.of(context).showSnackBar(snackBar);
                });
              }
            }
          },
        ),
      ),
    );
  }
}
