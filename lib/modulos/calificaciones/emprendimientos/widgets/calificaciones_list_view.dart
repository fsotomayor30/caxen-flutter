import 'dart:async';

import 'package:app/modelos/calificacion_emprendimiento.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modulos/calificaciones/emprendimientos/widgets/calificaciones_list_tile.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CalificacionesListView extends StatefulWidget {
  final Emprendimiento emprendimiento;

  CalificacionesListView({@required this.emprendimiento});

  @override
  CalificacionesListViewState createState() => CalificacionesListViewState();
}

class CalificacionesListViewState extends State<CalificacionesListView> {
  final dbRef =
      FirebaseDatabase.instance.reference().child("entrepreneurshipQualify");

  List<CalificacionEmprendimiento> lists = <CalificacionEmprendimiento>[];

  StreamSubscription<Event> _onAddedSubscription;
  StreamSubscription<Event> _onChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onAddedSubscription = dbRef
        .child(widget.emprendimiento.uuid)
        .onChildAdded
        .listen(_childAdded);
    _onChangedSubscription = dbRef
        .child(widget.emprendimiento.uuid)
        .onChildChanged
        .listen(_childChanged);
  }

  @override
  void dispose() {
    _onAddedSubscription.cancel();
    _onChangedSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) => Expanded(
          child: Column(children: <Widget>[
        Expanded(
            child: new ListView.builder(
                shrinkWrap: true,
                itemCount: lists.length,
                itemBuilder: (BuildContext context, int index) {
                  return CalificacionesListTile(
                    calificacionEmprendimiento: lists[index],
                  );
                }))
      ])),
    );
  }

  void _childAdded(Event event) {
    setState(() {
      lists.add(
          new CalificacionEmprendimiento.fromSnapshot(event.snapshot.value));
    });
  }

  void _childChanged(Event event) {}
}
