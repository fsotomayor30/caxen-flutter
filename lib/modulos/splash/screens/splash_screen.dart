import 'dart:async';

import 'package:app/modulos/introduccion/screens/introduction_slider_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashPage createState() => _SplashPage();
}

class _SplashPage extends State<SplashScreen> {



  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var duration = new Duration(seconds: 4);
    return new Timer(duration, route);
  }

  route() {

    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (context) => IntroductionSliderScreen()));

    /*SharedPreferences prefs = await SharedPreferences.getInstance();

    bool lookCarrousel = prefs.getBool("lookCarrousel");
    logger.d(lookCarrousel);
    if(lookCarrousel==null){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => IntroductionSliderScreen()));
    }else{
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => AutenticacionScreen()));
    }*/

  }

  @override
  Widget build(BuildContext context) {

    precacheImage(AssetImage("assets/backgroundlogin.png"), context);
    precacheImage(AssetImage("assets/background_uno.png"), context);
    precacheImage(AssetImage("assets/background_dos.png"), context);
    precacheImage(AssetImage("assets/background_tres.png"), context);
    precacheImage(AssetImage("assets/img_caxen_background.png"), context);


    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        child: Container(
          width: mediaQueryData.width,
          height: mediaQueryData.height,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/img_caxen_brand.png",
                width: mediaQueryData.width * 0.5,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
