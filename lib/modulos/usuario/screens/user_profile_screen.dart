import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/autenticacion/screens/autenticacion_email_screen.dart';
import 'package:app/modulos/emprendimientos/screen/mis_emprendimientos_list_screen.dart';
import 'package:app/modulos/freelances/screens/mis_freelances_list_screen.dart';
import 'package:app/modulos/introduccion/screens/introduction_slider_screen.dart';
import 'package:app/modulos/oficios/screens/mis_oficios_list_screen.dart';
import 'package:app/servicios/firebase_auth_service.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _MyProfilePage createState() => _MyProfilePage();
}

class _MyProfilePage extends State<UserProfileScreen> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
        builder: (GlobalController globalController) {
      return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            title: Text("Mi Perfil"),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  _dialogAyuda();
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Icon(
                    Icons.help,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              )
            ],
          ),
          body: Column(children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 16),
              width: MediaQuery.of(context).size.width,
              //height: MediaQuery.of(context).size.height / 3.2,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(32),
                    bottomLeft: Radius.circular(32)),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: globalController.usuario.photoProfile != ''
                          ? BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(
                                      globalController.usuario.photoProfile)))
                          : BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(100)),
                              border: Border.all(
                                  width: 3,
                                  color: Colors.white,
                                  style: BorderStyle.solid)),
                      child: globalController.usuario.photoProfile != ''
                          ? Container()
                          : Center(
                              child: Text(
                              globalController.usuario.email[0],
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 60),
                            )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 140,
                      child: Text(
                        globalController.usuario.displayName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: Get.height * (2 / 3) - 86,
              child: this._opciones(globalController.usuario),
            ),
          ])
          /* Center(
                child: Container(
                    width: mediaQueryData.width,
                    height: mediaQueryData.height,
                    child: ListView(
                      children: <Widget>[
                        InfoProfileWidget(
                            email: usuario.email,
                            photoProfileUrl: usuario.photoProfile),
                        Container(
                          height: mediaQueryData.height * 0.7,
                          child: this._opciones(usuario),
                        ),
                      ],
                    ))),*/
          );
    });
  }

  Widget _opciones(Usuario usuario) {
    return GridView.count(
      crossAxisCount: 3,
      padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      mainAxisSpacing: 15.0,
      crossAxisSpacing: 15.0,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MisEmprendimientosListScreen(
                        usuario: usuario,
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: EMPRENDIMIENTOS_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.rocket,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Mis Emprendimientos",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(color: Colors.white, fontSize: 10),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MisFreelancesListScreen(
                        usuario: usuario,
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: FREELANCES_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.bullhorn,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Mis Freelancers",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(color: Colors.white, fontSize: 10),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MisOficiosListScreen(
                        usuario: usuario,
                      )),
            );
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: OFICIOS_COLOR,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.handPointer,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Mis Oficios",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () async {
            if (await canLaunch('https://caxenapp.com/#/TyC')) {
              await launch('https://caxenapp.com/#/TyC');
            } else {
              throw 'Could not launch ' + 'https://caxenapp.com/#/TyC';
            }
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.grey,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    Icons.title,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Terminos y condiciones",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(color: Colors.white, fontSize: 10),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () async {
            var correoUrl = 'mailto:contacto@caxenapp.com?subject=Soporte';

            if (await canLaunch(correoUrl)) {
              await launch(correoUrl);
            } else {
              throw 'Could not launch ' + "Soporte";
            }
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.lightGreen,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.headset,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Soporte",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(color: Colors.white, fontSize: 10),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () async {
            if (await canLaunch('https://www.instagram.com/caxen.app')) {
              await launch('https://www.instagram.com/caxen.app');
            } else {
              throw 'Could not launch ' + 'https://www.instagram.com/caxen.app';
            }
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xFFf3006a),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.instagram,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Instagram",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () async {
            if (await canLaunch('https://www.facebook.com/caxenapp')) {
              await launch('https://www.facebook.com/caxenapp');
            } else {
              throw 'Could not launch ' + 'https://www.facebook.com/caxenapp';
            }
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xFF1774eb),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.facebookF,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Facebook",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        GestureDetector(
          onTap: () async {
            try {
              final auth = new FirebaseAuthService();
              await auth.signOutAccount();

              SharedPreferences prefs = await SharedPreferences.getInstance();
              await prefs.setString('uuid', null);
              await prefs.setString('displayName', null);
              await prefs.setString('email', null);
              await prefs.setString('photoProfile', '');
              await prefs.setString('tokenRing', null);
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => IntroductionSliderScreen()),
                  (Route<dynamic> route) => false);
            } catch (e) {
              print(e);
            }
          },
          child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.red,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(
                    FontAwesomeIcons.signOutAlt,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "Cerrar Sesión",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
      ],
    );
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes editar o eliminar tus publicaciones",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
