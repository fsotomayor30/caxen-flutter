import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InfoProfileWidget extends StatefulWidget {
  final String photoProfileUrl;
  final String email;

  const InfoProfileWidget({Key key, this.photoProfileUrl, this.email})
      : super(key: key);

  @override
  _InfoProfileWidget createState() => _InfoProfileWidget();
}

class _InfoProfileWidget extends State<InfoProfileWidget> {
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery
        .of(context)
        .size;

    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: (mediaQueryData.width - 10) * 0.2,
            child: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: (widget.photoProfileUrl!='')?
                  Image.network(
                    widget.photoProfileUrl,
                  ):Image.asset('assets/user.png')
              ),
            ),
          ),
          Container(
            width: (mediaQueryData.width - 10) * 0.8,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: (mediaQueryData.width - 10) * 0.7,
                  child: Text(
                    widget.email,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 15, color: Colors.black54),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
