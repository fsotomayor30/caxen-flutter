import 'dart:io';

import 'package:app/funciones/calificacion_emprendimiento_funciones.dart';
import 'package:app/funciones/like_emprendimiento_funciones.dart';
import 'package:app/funciones/usuario_funciones.dart';
import 'package:app/http/notification_service.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/calificaciones/emprendimientos/screens/calificaciones_list_screen.dart';
import 'package:app/modulos/comentarios/emprendimientos/screens/comentarios_list_screen.dart';
import 'package:app/modulos/emprendimientos/screen/one_emprendimiento_map_screen.dart';
import 'package:app/modulos/emprendimientos/widgets/slide_dots_emprendimiento.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/pages/payment.page.dart';
import 'package:app/shared/widgets/tiempo_atras.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:like_button/like_button.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:url_launcher/url_launcher.dart';

class EmprendimientosListTile extends StatefulWidget {
  final Emprendimiento emprendimiento;
  final Usuario usuario;

  EmprendimientosListTile(
      {@required this.emprendimiento, @required this.usuario});

  @override
  EmprendimientosListTileState createState() => EmprendimientosListTileState();
}

class EmprendimientosListTileState extends State<EmprendimientosListTile> {
  final LikeEmprendimientoFunciones likeEmprendimientoFunciones =
      LikeEmprendimientoFunciones();
  final CalificacionEmprendimientosFunciones
      calificacionEmprendimientosFunciones =
      CalificacionEmprendimientosFunciones();

  NotificationService notificationService = NotificationService();
  UsuarioFunciones usuarioFunciones = UsuarioFunciones();

  int like;
  bool conLike;

  int indexImage = 0;
  //-1 = no emprendimientos
  //0 = sin like
  // >0 = con like

  @override
  void initState() {
    super.initState();
    likeEmprendimientoFunciones
        .cantidadLike(widget.emprendimiento.uuid)
        .then((result) {
      if (mounted) {
        setState(() {
          like = result;
        });
      }
    });

    likeEmprendimientoFunciones
        .conLike(widget.usuario, widget.emprendimiento.uuid)
        .then((result) {
      if (mounted) {
        setState(() {
          if (result > 0) {
            conLike = true;
          } else {
            conLike = false;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Card(
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.only(top: 8, bottom: 10, left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              this._informacionAutor(Get.width),
              this._nombre(),
              SizedBox(
                height: 10,
              ),
              this._descripcion(),
              SizedBox(
                height: 20,
              ),
              this._imagenes(Get.width),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  for (int i = 0; i < 3; i++)
                    if (i == indexImage)
                      SlideDotsEmprendimientos(true)
                    else
                      SlideDotsEmprendimientos(false)
                ],
              ),
              SizedBox(
                height: 20,
              ),
              this._acciones(Get.width),
              this._divisor(),
              this._contacto(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _informacionAutor(double width) {
    return ListTile(
      contentPadding: EdgeInsets.all(0),
      leading: Container(
        width: (width * 0.20) - 20,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: (widget.emprendimiento.imagePerfil != '')
                ? Image.network(widget.emprendimiento.imagePerfil)
                : Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        border: Border.all(
                            width: 3,
                            color: EMPRENDIMIENTOS_COLOR,
                            style: BorderStyle.solid)),
                    child: Center(
                        child: Text(
                      widget.emprendimiento.email[0].toUpperCase(),
                      style:
                          TextStyle(color: EMPRENDIMIENTOS_COLOR, fontSize: 25),
                    )),
                  )),
      ),
      title: Text(
        widget.emprendimiento.name + ":",
        style: TextStyle(fontSize: 14),
      ),
      subtitle: TiempoAtrasWidget(
        fecha: widget.emprendimiento.date,
      ),
      trailing: widget.usuario.email == widget.emprendimiento.email
          ? OutlineButton(
              shape: StadiumBorder(),
              textColor: EMPRENDIMIENTOS_COLOR,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Quiero llegar \na más personas',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 10),
                ),
              ),
              borderSide: BorderSide(
                  color: EMPRENDIMIENTOS_COLOR,
                  style: BorderStyle.solid,
                  width: 1),
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PaymentScreen(
                            title: 'Promociona tu emprendimiento',
                            color: EMPRENDIMIENTOS_COLOR,
                          )),
                );
              },
            )
          : Container(
              width: 0,
              height: 0,
            ),
    );
  }

  Widget _nombre() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.stickyNote,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Nombre: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.emprendimiento.nameEntrepreneurship)
        ],
      ),
    );
  }

  Widget _descripcion() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.list,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Descripción: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.emprendimiento.description,
              style: TextStyle(fontSize: 13))
        ],
      ),
    );
  }

  Widget _divisor() {
    return Divider(
      height: 15,
      color: Colors.black54,
    );
  }

  Widget _contacto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: widget.emprendimiento.webSite != "",
                child: GestureDetector(
                  onTap: () async {
                    if (await canLaunch(
                        'https://' + widget.emprendimiento.webSite)) {
                      await launch('https://' + widget.emprendimiento.webSite);
                    } else {
                      throw 'Could not launch ' +
                          'https://' +
                          widget.emprendimiento.webSite;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/computadora.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.urlInstagram != "",
                child: GestureDetector(
                  onTap: () async {
                    String usuarioInstagram =
                        widget.emprendimiento.urlInstagram;
                    usuarioInstagram = usuarioInstagram.replaceAll("@", "");

                    if (await canLaunch(
                        "https://www.instagram.com/" + usuarioInstagram)) {
                      await launch(
                          "https://www.instagram.com/" + usuarioInstagram);
                    } else {
                      throw 'Could not launch ' +
                          "https://www.instagram.com/" +
                          usuarioInstagram;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/instagram.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.urlFb != "",
                child: GestureDetector(
                  onTap: () async {
                    if (await canLaunch(widget.emprendimiento.urlFb)) {
                      await launch(widget.emprendimiento.urlFb);
                    } else {
                      throw 'Could not launch ' + widget.emprendimiento.urlFb;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/facebook.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.phone != "",
                child: GestureDetector(
                  onTap: () async {
                    var whatsappUrl;
                    if (Platform.isIOS) {
                      whatsappUrl =
                          "whatsapp://wa.me/" + widget.emprendimiento.phone;
                    } else {
                      whatsappUrl = "whatsapp://send?phone=" +
                          widget.emprendimiento.phone;
                    }
                    if (await canLaunch(whatsappUrl)) {
                      await launch(whatsappUrl);
                    } else {
                      final snackBar = SnackBar(
                        content:
                            Text("Error: Necesitas tener instalado Whatsapp"),
                      );

                      Scaffold.of(context).showSnackBar(snackBar);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/whatsapp.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.mail != "",
                child: GestureDetector(
                  onTap: () async {
                    var correoUrl = 'mailto:' +
                        widget.emprendimiento.mail +
                        '?subject=CaxenApp';

                    if (await canLaunch(correoUrl)) {
                      await launch(correoUrl);
                    } else {
                      throw 'Could not launch ' + widget.emprendimiento.email;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/gmail.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.latitude != null &&
                    widget.emprendimiento.longitude != null,
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OnlyEmprendimientoMapScreen(
                                  emprendimiento: widget.emprendimiento,
                                )));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/marker.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _acciones(double width) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              this._seccionLike(),
              SizedBox(
                width: 20.0,
              ),
              this._seccionComentarios(widget.emprendimiento),
              /* SizedBox(
                width: 20.0,
              ),
              this._seccionCalificar(widget.emprendimiento, width),*/
              SizedBox(
                width: 20.0,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _seccionLike() {
    return LikeButton(
      isLiked: conLike,
      size: 30,
      circleColor:
          CircleColor(start: Color(0xff00ddff), end: Color(0xff0099cc)),
      bubblesColor: BubblesColor(
        dotPrimaryColor: Color(0xff33b5e5),
        dotSecondaryColor: Color(0xff0099cc),
      ),
      onTap: onLikeButtonTapped,
      likeBuilder: (bool isLiked) {
        return Icon(
          FontAwesomeIcons.solidHeart,
          color: isLiked ? Colors.red : Colors.grey,
          size: 20,
        );
      },
      likeCount: like,
      countBuilder: (int count, bool isLiked, String text) {
        var color = isLiked ? Colors.red : Colors.grey;
        Widget result;
        if (count == 0) {
          result = Text(
            "0",
            style: TextStyle(color: color, fontSize: 15),
          );
        } else
          result = Text(
            text,
            style: TextStyle(color: color, fontSize: 15),
          );
        return result;
      },
    );
  }

  Widget _seccionComentarios(Emprendimiento emprendimiento) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: GestureDetector(
        child: Image.asset(
          "assets/comentario.png",
          width: 27,
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ComentariosListScreen(
                      emprendimiento: emprendimiento,
                    )),
          );
        },
      ),
    );
  }

  Widget _seccionCalificar(Emprendimiento emprendimiento, double width) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: GestureDetector(
        child: Image.asset(
          "assets/estrella.png",
          width: 27,
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CalificacionesListScreen(
                      emprendimiento: emprendimiento,
                    )),
          );
        },
      ),
    );
  }

  int selectedValue2;

  void onChange2(int value) {
    setState(() {
      selectedValue2 = value;
    });
  }

  changeImage(int index, CarouselPageChangedReason reason) {
    setState(() {
      indexImage = index;
    });
  }

  Widget _imagenes(double width) {
    final List<String> imgList = [
      widget.emprendimiento.foto1,
      widget.emprendimiento.foto2,
      widget.emprendimiento.foto3,
    ];

    return Container(
      constraints: BoxConstraints.expand(height: 300),
      width: width,
      child: Builder(
        builder: (context) {
          final double height = MediaQuery.of(context).size.height;
          return CarouselSlider(
            options: CarouselOptions(
                height: height,
                viewportFraction: 1.0,
                enlargeCenterPage: false,
                onPageChanged: changeImage
                // autoPlay: false,
                ),
            items: imgList
                .map((item) => Container(
                      child: Center(
                          child: PinchZoomImage(
                        image: Image.network(
                          item,
                          fit: BoxFit.contain,
                          height: height,
                          width: width,
                        ),
                        zoomedBackgroundColor:
                            Color.fromRGBO(240, 240, 240, 1.0),
                        hideStatusBarWhileZooming: true,
                        onZoomStart: () {
                          print('Zoom started');
                        },
                        onZoomEnd: () {
                          print('Zoom finished');
                        },
                      )
/*                  child: PinchZoomImage(
                    image: Image.network(item, fit: BoxFit.contain, height: height, width: width,),
                    zoomedBackgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
                    hideStatusBarWhileZooming: true,
                    onZoomStart: () {
                      print('Zoom started');
                    },
                    onZoomEnd: () {
                      print('Zoom finished');
                    },
                  ),*/

                          ),
                    ))
                .toList(),
          );
        },
      ),
    );
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    GlobalController _globalController = Get.find();
    Usuario usuario = _globalController.usuario;
    if (!isLiked) {
      await likeEmprendimientoFunciones.agregarLike(
          widget.emprendimiento.uuid, usuario);
      Usuario usuarioToken = await usuarioFunciones
          .obtenerUsuarioByEmail(widget.emprendimiento.email);
      if (usuarioToken != null) {
        if (usuarioToken.tokenRing != null) {
          await notificationService.sendNotification(usuarioToken.tokenRing,
              "Caxen nuevo like", 'A alguien le gustó tu publicación en Caxen');
        }
      }
    } else {
      await likeEmprendimientoFunciones.quitarLike(
          widget.emprendimiento.uuid, usuario);
    }

    return !isLiked;
  }
}
