import 'dart:io';

import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modulos/emprendimientos/screen/emprendimientos_list_screen.dart';
import 'package:app/servicios/firebase_storage_service.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/pages/camera.page.dart';
import 'package:app/shared/widgets/categorias_emprendimientos_form_drop_down.dart';
import 'package:app/shared/widgets/region_form_drop_down.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:latlong/latlong.dart';
import 'package:logger/logger.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:progress_dialog/progress_dialog.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

class EmprendimientoForm extends StatefulWidget {
  final Emprendimiento emprendimiento;

  EmprendimientoForm({@required this.emprendimiento});

  @override
  _EmprendimientoScreenState createState() => _EmprendimientoScreenState();
}

class _EmprendimientoScreenState extends State<EmprendimientoForm> {
  ProgressDialog progressDialog;

  final _formKey = GlobalKey<FormState>();

  File imagen1;
  File imagen2;
  File imagen3;

  final dbRef = FirebaseDatabase.instance.reference().child("entrepreneurship");
  FirebaseStorageService firebaseStorageService = FirebaseStorageService();

  LatLng _center;

  Emprendimiento _emprendimiento;
  MapController _mapController = MapController();

  @override
  void initState() {
    super.initState();
    setState(() {
      _emprendimiento = widget.emprendimiento;
      if (_emprendimiento.address != null) {
        _center = LatLng(_emprendimiento.latitude, _emprendimiento.longitude);
      }
    });
  }

  Future escogerImagen1(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen1 = image;
    });

    if (imageSource == ImageSource.gallery) {
      setState(() {
        _emprendimiento.source = "gallery";
      });
    } else {
      setState(() {
        _emprendimiento.source = "camera";
      });
    }
  }

  Future escogerImagen2(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen2 = image;
    });
  }

  Future escogerImagen3(ImageSource imageSource) async {
    var image = await ImagePicker.pickImage(source: imageSource);
    setState(() {
      imagen3 = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Publicando...");
    return Container(
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
            child: Form(
                key: this._formKey,
                child: Column(
                  children: <Widget>[
                    this._nombreEmprendimientoInput(),
                    this._descripcionEmprendimientoInput(),
                    this._categoriasDropDown(width),
                    SizedBox(height: 20),
                    this._direccionEmprendimientoInput(),
                    Visibility(
                      visible: _emprendimiento.address != null,
                      child: this._mapAddress(),
                    ),
                    SizedBox(height: 20),
                    this._regionDropDown(width),
                    this._fotosInput(width),
                    this._sitioWebEmprendimientoInput(),
                    this._facebookEmprendimientoInput(),
                    this._instagramEmprendimientoInput(),
                    this._emailEmprendimientoInput(),
                    this._telefonoEmprendimientoInput(),
                    this._botonGuardar()
                  ],
                ))));
  }

  Widget _categoriasDropDown(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.list,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    CategoriaEmprendimientosFormDropDown(
                        initValue: _emprendimiento.category,
                        onChanged: (String category) {
                          _emprendimiento.category = category;
                        })
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _fotosInput(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                Icons.image,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            width: width - 80,
            child: Card(
              elevation: 0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.black38, width: 1),
                borderRadius: BorderRadius.circular(5),
              ),
              margin: EdgeInsets.all(0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen1 == null)
                              ? (_emprendimiento.foto1 == null)
                                  ? Container(
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      decoration: BoxDecoration(
                                          color: Colors.black45,
                                          border: Border.all(
                                              color: Colors.black, width: 1),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.image,
                                            size: 40,
                                          ),
                                          Text(
                                            "Tu marca o logo",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.black54),
                                          )
                                        ],
                                      ),
                                    )
                                  : Image.network(
                                      _emprendimiento.foto1,
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      loadingBuilder: (BuildContext context,
                                          Widget child,
                                          ImageChunkEvent loadingProgress) {
                                        if (loadingProgress == null)
                                          return child;
                                        return Container(
                                          width: ((width - 80) / 2) - 15,
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              value: loadingProgress
                                                          .expectedTotalBytes !=
                                                      null
                                                  ? loadingProgress
                                                          .cumulativeBytesLoaded /
                                                      loadingProgress
                                                          .expectedTotalBytes
                                                  : null,
                                            ),
                                          ),
                                        );
                                      },
                                    )
                              : Image.file(
                                  imagen1,
                                  width: ((width - 80) / 2) - 15,
                                  height: (width - 80) / 2,
                                )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 1"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: EMPRENDIMIENTOS_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: EMPRENDIMIENTOS_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Tu marca o logo',
                                        orientation: 'vertical',
                                        color: EMPRENDIMIENTOS_COLOR,
                                        onChanged: (String path) {
                                          setState(() {
                                            imagen1 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: EMPRENDIMIENTOS_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: EMPRENDIMIENTOS_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen1(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen2 == null)
                              ? (_emprendimiento.foto2 == null)
                                  ? Container(
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      decoration: BoxDecoration(
                                          color: Colors.black45,
                                          border: Border.all(
                                              color: Colors.black, width: 1),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.image,
                                            size: 40,
                                          ),
                                          Text(
                                            "Tus productos o servicios",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.black54),
                                          )
                                        ],
                                      ),
                                    )
                                  : Image.network(
                                      _emprendimiento.foto2,
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      loadingBuilder: (BuildContext context,
                                          Widget child,
                                          ImageChunkEvent loadingProgress) {
                                        if (loadingProgress == null)
                                          return child;
                                        return Container(
                                          width: ((width - 80) / 2) - 15,
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              value: loadingProgress
                                                          .expectedTotalBytes !=
                                                      null
                                                  ? loadingProgress
                                                          .cumulativeBytesLoaded /
                                                      loadingProgress
                                                          .expectedTotalBytes
                                                  : null,
                                            ),
                                          ),
                                        );
                                      },
                                    )
                              : Image.file(
                                  imagen2,
                                  width: ((width - 80) / 2) - 15,
                                  height: (width - 80) / 2,
                                )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 2"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: EMPRENDIMIENTOS_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: EMPRENDIMIENTOS_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  //await escogerImagen2(ImageSource.camera);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Tus productos o servicios',
                                        orientation: 'vertical',
                                        color: EMPRENDIMIENTOS_COLOR,
                                        onChanged: (String path) {
                                          setState(() {
                                            imagen2 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: EMPRENDIMIENTOS_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: EMPRENDIMIENTOS_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen2(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 10, bottom: 10, right: 5, left: 10),
                          child: (imagen3 == null)
                              ? (_emprendimiento.foto3 == null)
                                  ? Container(
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      decoration: BoxDecoration(
                                          color: Colors.black45,
                                          border: Border.all(
                                              color: Colors.black, width: 1),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.image,
                                            size: 40,
                                          ),
                                          Text(
                                            "Tus productos o servicios",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.black54),
                                          )
                                        ],
                                      ),
                                    )
                                  : Image.network(
                                      _emprendimiento.foto3,
                                      width: ((width - 80) / 2) - 15,
                                      height: (width - 80) / 2,
                                      loadingBuilder: (BuildContext context,
                                          Widget child,
                                          ImageChunkEvent loadingProgress) {
                                        if (loadingProgress == null)
                                          return child;
                                        return Container(
                                          width: ((width - 80) / 2) - 15,
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              value: loadingProgress
                                                          .expectedTotalBytes !=
                                                      null
                                                  ? loadingProgress
                                                          .cumulativeBytesLoaded /
                                                      loadingProgress
                                                          .expectedTotalBytes
                                                  : null,
                                            ),
                                          ),
                                        );
                                      },
                                    )
                              : Image.file(
                                  imagen3,
                                  width: ((width - 80) / 2) - 15,
                                  height: (width - 80) / 2,
                                )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, right: 10, left: 5),
                        child: Container(
                          width: ((width - 80) / 2) - 15,
                          height: (width - 80) / 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Foto 3"),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: EMPRENDIMIENTOS_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir cámara',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: EMPRENDIMIENTOS_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TakePictureScreen(
                                        message: 'Tus productos o servicios',
                                        orientation: 'vertical',
                                        color: EMPRENDIMIENTOS_COLOR,
                                        onChanged: (String path) {
                                          setState(() {
                                            imagen3 = File(path);
                                          });
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              new OutlineButton(
                                shape: StadiumBorder(),
                                textColor: EMPRENDIMIENTOS_COLOR,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Abrir Galería',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                                borderSide: BorderSide(
                                    color: EMPRENDIMIENTOS_COLOR,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () async {
                                  await escogerImagen3(ImageSource.gallery);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _direccionEmprendimientoInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(
                  FontAwesomeIcons.streetView,
                  size: 25,
                  color: Colors.black45,
                ),
                labelText: 'Dirección del emprendimiento',
                hintText: 'Alameda 412, Santiago'),
            initialValue: _emprendimiento.address,
            onChanged: (String value) {
              setState(() {
                if (value.isEmpty) {
                  _emprendimiento.address = null;
                } else {
                  _emprendimiento.address = value;
                }
              });
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: new OutlineButton(
            shape: StadiumBorder(),
            textColor: EMPRENDIMIENTOS_COLOR,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Buscar',
                style: TextStyle(fontSize: 10),
              ),
            ),
            borderSide: BorderSide(
                color: EMPRENDIMIENTOS_COLOR,
                style: BorderStyle.solid,
                width: 1),
            onPressed: () async {
              if (_emprendimiento.address == null) {
                final snackBar = SnackBar(
                  content: Text("Error: Para buscar ingresa la dirección"),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              } else {
                try {
                  List<Placemark> placemark = await Geolocator()
                      .placemarkFromAddress(_emprendimiento.address);
                  _mapController.move(
                      LatLng(placemark[0].position.latitude,
                          placemark[0].position.longitude),
                      16);
                  setState(() {
                    _center = LatLng(placemark[0].position.latitude,
                        placemark[0].position.longitude);
                  });
                } catch (e) {
                  final snackBar = SnackBar(
                    content: Text("Error: No encontramos ninguna dirección"),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);
                }
              }
            },
          ),
        )
      ],
    );
  }

  Widget _mapAddress() {
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width - 40,
      child: FlutterMap(
        options: MapOptions(
          center: _center,
          zoom: 16.0,
        ),
        mapController: _mapController,
        layers: [
          new TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']),
          MarkerLayerOptions(markers: [
            Marker(
              height: 45,
              width: 45,
              point: _center,
              builder: (ctx) =>
                  Container(height: 45, width: 45, child: _buildMarker()),
            )
          ]),
          // ADD THIS
        ],
      ),
    );
  }

  Widget _buildMarker() {
    return Container(
        width: 300.0,
        height: 300.0,
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(110.0),
        ),
        child: Center(
            child: lottie.Lottie.asset('assets/marker.json', width: 40)));
  }

  Widget _regionDropDown(double width) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Row(
        children: <Widget>[
          Container(
              width: 40,
              child: Icon(
                FontAwesomeIcons.mapMarkerAlt,
                size: 35,
                color: Colors.black45,
              )),
          Container(
            child: Flexible(
              child: Card(
                elevation: 0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black38, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(0),
                child: RegionFormDropDown(
                    initValue: _emprendimiento.region,
                    onChanged: (String region) {
                      _emprendimiento.region = region;
                      logger.i(
                          "[REGION CATEGORY CHANGE] " + _emprendimiento.region);
                    }),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _nombreEmprendimientoInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 30,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.rocket),
                labelText: 'Nombre del Emprendimiento',
                hintText: 'Caxen'),
            initialValue: _emprendimiento.nameEntrepreneurship,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }

              if (value.length > 30) {
                return 'La cantidad de letras debe ser menor a 30';
              }

              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              return null;
            },
            onSaved: (String value) {
              _emprendimiento.nameEntrepreneurship = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _descripcionEmprendimientoInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 150,
            maxLines: 4,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.pencilAlt),
                labelText: 'Descripción',
                hintText: '...'),
            initialValue: _emprendimiento.description,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              return null;
            },
            onSaved: (String value) {
              _emprendimiento.description = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _sitioWebEmprendimientoInput() {
    return Column(
      //crossAxisAlignment: CrossAxisAlignment.s,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.laptop),
                labelText: 'Sitio Web',
                hintText: 'www.caxenapp.com'),
            initialValue: _emprendimiento.webSite,
            onSaved: (String value) {
              _emprendimiento.webSite = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 40, bottom: 10),
          child: Text(
            "El sitio web ingresado debe estar en el siguiente formato www.caxenapp.com",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _facebookEmprendimientoInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.facebookF),
                labelText: 'Facebook',
                hintText: 'copia aquí el URL de tu página FB'),
            initialValue: _emprendimiento.urlFb,
            onSaved: (String value) {
              _emprendimiento.urlFb = value;
            },
          ),
        ),
        Text(
          "El usuario ingresado debe estar en el siguiente formato /caxen",
          style: TextStyle(color: Colors.black45, fontSize: 8),
        )
      ],
    );
  }

  Widget _instagramEmprendimientoInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.instagram),
                labelText: 'Usuario Instagram',
                hintText: '@caxen.app'),
            initialValue: _emprendimiento.urlInstagram,
            onSaved: (String value) {
              _emprendimiento.urlInstagram = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15, bottom: 10),
          child: Text(
            "El usuario ingresado debe estar en el siguiente formato @caxen.app",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _emailEmprendimientoInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.envelope),
                labelText: 'Correo Electrónico',
                hintText: 'contacto@caxenapp.com'),
            initialValue: _emprendimiento.mail,
            validator: (value) {
              if (value.isEmpty) {
                return 'Este campo debe ser completado';
              }
              if (value.length < 3) {
                return 'La cantidad de letras debe ser mayor a 3';
              }
              if (!EmailValidator.validate(value)) {
                return 'Ingrese un correo válido';
              }
              return null;
            },
            onSaved: (String value) {
              _emprendimiento.mail = value;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(
            "* requerido",
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.red, fontSize: 10),
          ),
        )
      ],
    );
  }

  Widget _telefonoEmprendimientoInput() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: TextFormField(
            maxLength: 12,
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon: Icon(FontAwesomeIcons.phone),
                labelText: 'Número de celular',
                hintText: '+569 XX XX XX XX'),
            initialValue: _emprendimiento.phone,
            onSaved: (String value) {
              _emprendimiento.phone = value;
            },
            validator: (value) {
              if (value.length > 12) {
                return 'El largo máximo es 12';
              }
              if (value.length >= 1 && value.length < 12) {
                return 'El largo mínimo es 12';
              }

              if (value.length >= 1 && value[0] != "+") {
                return 'Debe comenzar con +';
              }

              if (value.length > 4 && value.substring(1, 4) != "569") {
                return 'Debe contener 569';
              }

              return null;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 40, bottom: 10),
          child: Text(
            "El número ingresado debe contener +569 más 8 digitos, por ejemplo, +569 12345678",
            style: TextStyle(color: Colors.black45, fontSize: 8),
          ),
        )
      ],
    );
  }

  Widget _botonGuardar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: EMPRENDIMIENTOS_COLOR,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar',
            style: TextStyle(fontSize: 30),
          ),
        ),
        borderSide: BorderSide(
            color: EMPRENDIMIENTOS_COLOR, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();
            await progressDialog.show();
            //VERIFICAR SI SE CREA O ACTUALIZA EL EMPRENDIMIENTO
            if (_emprendimiento.uuid == null) {
              logger.d("[Creando un emprendimiento nuevo]");

              //CREANDO UN NUEVO ID
              var id = dbRef.push().key;

              String foto1;

              //VERIFICACION SI NO SELECCIONO IMAGEN
              if (imagen1 == null) {
                final snackBar = SnackBar(
                  content: Text("Error: Debes subir la imagen 1"),
                );
                Scaffold.of(context).showSnackBar(snackBar);
                await progressDialog.hide();
                return;
              } else {
                foto1 = await firebaseStorageService.uploadFile(
                    imagen1,
                    "caxen_foto1",
                    "entrepreneurshipImages/" +
                        _emprendimiento.email +
                        "/" +
                        id);
              }

              String foto2;
              //VERIFICACION SI NO SELECCIONO IMAGEN
              if (imagen2 == null) {
                final snackBar = SnackBar(
                  content: Text("Error: Debes subir la imagen 2"),
                );
                Scaffold.of(context).showSnackBar(snackBar);
                await progressDialog.hide();
                return;
              } else {
                foto2 = await firebaseStorageService.uploadFile(
                    imagen2,
                    "caxen_foto2",
                    "entrepreneurshipImages/" +
                        _emprendimiento.email +
                        "/" +
                        id);
              }

              String foto3;

              //VERIFICACION SI NO SELECCIONO IMAGEN
              if (imagen3 == null) {
                final snackBar = SnackBar(
                  content: Text("Error: Debes subir la imagen 3"),
                );
                Scaffold.of(context).showSnackBar(snackBar);

                await progressDialog.hide();

                return;
              } else {
                foto3 = await firebaseStorageService.uploadFile(
                    imagen3,
                    "caxen_foto3",
                    "entrepreneurshipImages/" +
                        _emprendimiento.email +
                        "/" +
                        id);
              }

              if (_emprendimiento.address != null) {
                setState(() {
                  _emprendimiento.latitude = _center.latitude;
                  _emprendimiento.longitude = _center.longitude;
                });
              }

              logger.d("======================");
              logger.d("Preparando datos");
              logger.d("======================");

              var emprendimiento = {
                "imagePerfil": _emprendimiento.imagePerfil,
                "email": _emprendimiento.email,
                "category": (_emprendimiento.category == "Todas")
                    ? "Mascotas"
                    : _emprendimiento.category,
                "date": Timestamp.now().millisecondsSinceEpoch.toString(),
                "description": _emprendimiento.description,
                "mail":
                    (_emprendimiento.mail == null) ? "" : _emprendimiento.mail,
                "name": _emprendimiento.name,
                "nameEntrepreneurship": _emprendimiento.nameEntrepreneurship,
                "urlInstagram": (_emprendimiento.urlInstagram == null)
                    ? ""
                    : _emprendimiento.urlInstagram,
                "phone": (_emprendimiento.phone == null)
                    ? ""
                    : _emprendimiento.phone,
                "region": (_emprendimiento.region == "Todas")
                    ? "Región de Arica y Parinacota"
                    : _emprendimiento.region,
                "source": _emprendimiento.source,
                "webSite": _emprendimiento.webSite,
                "foto1": foto1,
                "foto2": foto2,
                "foto3": foto3,
                "uuid": id,
                "urlFb": (_emprendimiento.urlFb == null)
                    ? ""
                    : _emprendimiento.urlFb,
                "visible": true,
                "address": _emprendimiento.address == null
                    ? null
                    : _emprendimiento.address,
                "latitude": _emprendimiento.latitude == null
                    ? null
                    : _emprendimiento.latitude,
                "longitude": _emprendimiento.longitude == null
                    ? null
                    : _emprendimiento.longitude,
              };

              logger.d("[DATOS A GUARDAR] " + emprendimiento.toString());

              //GUARDANDO LOS DATOS
              await dbRef.child(id).set(emprendimiento).then((_) async {
                await progressDialog.hide();

                Navigator.pop(context);
                Navigator.pop(context);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EmprendimientosListScreen(
                            categoria: "Todas",
                          )),
                );
              }).catchError((onError) async {
                await progressDialog.hide();

                final snackBar =
                    SnackBar(content: Text("Error: " + onError.toString()));
                Scaffold.of(context).showSnackBar(snackBar);
              });

              //ACTUALIZACIÓN DE EMPRENDIMIENTO*/
            } else {
              String foto1;
              //VERIFICACION SI NO SELECCIONO IMAGEN
              if (imagen1 == null) {
                foto1 = _emprendimiento.foto1;
              } else {
                foto1 = await firebaseStorageService.uploadFile(
                    imagen1,
                    "caxen_foto1",
                    "entrepreneurshipImages/" +
                        _emprendimiento.email +
                        "/" +
                        _emprendimiento.uuid);
              }

              String foto2;
              //VERIFICACION SI NO SELECCIONO IMAGEN
              if (imagen2 == null) {
                foto2 = _emprendimiento.foto2;
              } else {
                foto2 = await firebaseStorageService.uploadFile(
                    imagen2,
                    "caxen_foto2",
                    "entrepreneurshipImages/" +
                        _emprendimiento.email +
                        "/" +
                        _emprendimiento.uuid);
              }

              String foto3;
              //VERIFICACION SI NO SELECCIONO IMAGEN
              if (imagen3 == null) {
                foto3 = _emprendimiento.foto3;
              } else {
                foto3 = await firebaseStorageService.uploadFile(
                    imagen3,
                    "caxen_foto3",
                    "entrepreneurshipImages/" +
                        _emprendimiento.email +
                        "/" +
                        _emprendimiento.uuid);
              }

              if (_emprendimiento.address != null) {
                setState(() {
                  _emprendimiento.latitude = _center.latitude;
                  _emprendimiento.longitude = _center.longitude;
                });
              }

              logger.d("======================");
              logger.d("Preparando datos");
              logger.d("======================");

              var emprendimiento = {
                "imagePerfil": _emprendimiento.imagePerfil,
                "email": _emprendimiento.email,
                "category": (_emprendimiento.category == "Todas")
                    ? "Mascotas"
                    : _emprendimiento.category,
                "date": Timestamp.now().millisecondsSinceEpoch.toString(),
                "description": _emprendimiento.description,
                "mail":
                    (_emprendimiento.mail == null) ? "" : _emprendimiento.mail,
                "name": _emprendimiento.name,
                "nameEntrepreneurship": _emprendimiento.nameEntrepreneurship,
                "urlInstagram": (_emprendimiento.urlInstagram == null)
                    ? ""
                    : _emprendimiento.urlInstagram,
                "phone": (_emprendimiento.phone == null)
                    ? ""
                    : _emprendimiento.phone,
                "region": (_emprendimiento.region == "Todas")
                    ? "Región de Arica y Parinacota"
                    : _emprendimiento.region,
                "source": _emprendimiento.source,
                "webSite": _emprendimiento.webSite,
                "foto1": foto1,
                "foto2": foto2,
                "foto3": foto3,
                "uuid": _emprendimiento.uuid,
                "urlFb": (_emprendimiento.urlFb == null)
                    ? ""
                    : _emprendimiento.urlFb,
                "visible": true,
                "address": _emprendimiento.address == null
                    ? null
                    : _emprendimiento.address,
                "latitude": _emprendimiento.latitude == null
                    ? null
                    : _emprendimiento.latitude,
                "longitude": _emprendimiento.longitude == null
                    ? null
                    : _emprendimiento.longitude,
              };

              logger.d("[DATOS A GUARDAR] " + emprendimiento.toString());

              //GUARDANDO LOS DATOS
              await dbRef
                  .child(_emprendimiento.uuid)
                  .set(emprendimiento)
                  .then((_) async {
                await progressDialog.hide();

                Navigator.pop(context);
                Navigator.pop(context);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EmprendimientosListScreen(
                            categoria: "Todas",
                          )),
                );
              }).catchError((onError) async {
                await progressDialog.hide();

                final snackBar =
                    SnackBar(content: Text("Error: " + onError.toString()));
                Scaffold.of(context).showSnackBar(snackBar);
              });
            }
          }
        },
      ),
    );
  }
}
