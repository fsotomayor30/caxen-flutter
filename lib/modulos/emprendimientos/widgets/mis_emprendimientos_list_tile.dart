import 'dart:io';

import 'package:app/funciones/emprendimiento_funciones.dart';
import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/emprendimientos/screen/emprendimiento_actualizar_screen.dart';
import 'package:app/modulos/emprendimientos/screen/one_emprendimiento_map_screen.dart';
import 'package:app/modulos/emprendimientos/widgets/slide_dots_emprendimiento.dart';
import 'package:app/modulos/usuario/screens/user_profile_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/pages/payment.page.dart';
import 'package:app/shared/widgets/tiempo_atras.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class MisEmprendimientosListTile extends StatefulWidget {
  final Emprendimiento emprendimiento;
  final Usuario usuario;

  MisEmprendimientosListTile(
      {@required this.emprendimiento, @required this.usuario});

  @override
  _MisEmprendimientosListTileState createState() =>
      _MisEmprendimientosListTileState();
}

class _MisEmprendimientosListTileState
    extends State<MisEmprendimientosListTile> {
  final EmprendimientosFunciones emprendimientosFunciones =
      EmprendimientosFunciones();

  ProgressDialog progressDialog;
  int indexImage = 0;

  @override
  void initState() {
    super.initState();
  }

  changeImage(int index, CarouselPageChangedReason reason) {
    setState(() {
      indexImage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    progressDialog = new ProgressDialog(context);

    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Card(
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.only(top: 8, bottom: 10, left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              this._informacionAutor(mediaQueryData.width),
              this._nombre(),
              SizedBox(
                height: 10,
              ),
              this._descripcion(),
              SizedBox(
                height: 20,
              ),
              this._imagenes(mediaQueryData.width),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  for (int i = 0; i < 3; i++)
                    if (i == indexImage)
                      SlideDotsEmprendimientos(true)
                    else
                      SlideDotsEmprendimientos(false)
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  this._seccionEditar(widget.emprendimiento),
                  this._seccionEliminar(widget.emprendimiento, context)
                ],
              ),
              this.divisor(),
              this._contacto(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _informacionAutor(double width) {
    return ListTile(
      contentPadding: EdgeInsets.all(0),
      leading: Container(
        width: (width * 0.20) - 20,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: (widget.emprendimiento.imagePerfil != '')
                ? Image.network(widget.emprendimiento.imagePerfil)
                : Container(
                    width: 55,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        border: Border.all(
                            width: 3,
                            color: EMPRENDIMIENTOS_COLOR,
                            style: BorderStyle.solid)),
                    child: Center(
                        child: Text(
                      widget.emprendimiento.email[0].toUpperCase(),
                      style:
                          TextStyle(color: EMPRENDIMIENTOS_COLOR, fontSize: 25),
                    )),
                  )),
      ),
      title: Text(
        widget.emprendimiento.name + ":",
        style: TextStyle(fontSize: 14),
      ),
      trailing: widget.usuario.email == widget.emprendimiento.email
          ? OutlineButton(
              shape: StadiumBorder(),
              textColor: EMPRENDIMIENTOS_COLOR,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Quiero llegar \na más personas',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 10),
                ),
              ),
              borderSide: BorderSide(
                  color: EMPRENDIMIENTOS_COLOR,
                  style: BorderStyle.solid,
                  width: 1),
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PaymentScreen(
                            title: 'Promociona tu emprendimiento',
                            color: EMPRENDIMIENTOS_COLOR,
                          )),
                );
              },
            )
          : Container(
              width: 0,
              height: 0,
            ),
      subtitle: TiempoAtrasWidget(
        fecha: widget.emprendimiento.date,
      ),
    );
  }

  Widget _nombre() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.stickyNote,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Nombre: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.emprendimiento.nameEntrepreneurship)
        ],
      ),
    );
  }

  Widget _descripcion() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              FaIcon(
                FontAwesomeIcons.list,
                size: 15,
                color: Colors.black38,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  "Descripción: ",
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.black38),
                ),
              ),
            ],
          ),
          Text(widget.emprendimiento.description,
              style: TextStyle(fontSize: 13))
        ],
      ),
    );
  }

  Widget _imagenes(double width) {
    final List<String> imgList = [
      widget.emprendimiento.foto1,
      widget.emprendimiento.foto2,
      widget.emprendimiento.foto3,
    ];

    return Container(
      constraints: BoxConstraints.expand(height: 300),
      width: width,
      child: Builder(
        builder: (context) {
          final double height = MediaQuery.of(context).size.height;
          return CarouselSlider(
            options: CarouselOptions(
                height: height,
                viewportFraction: 1.0,
                enlargeCenterPage: false,
                onPageChanged: changeImage

                // autoPlay: false,
                ),
            items: imgList
                .map((item) => Container(
                      child: Center(
                        child: PinchZoomImage(
                          image: Image.network(
                            item,
                            fit: BoxFit.contain,
                            height: height,
                            width: width,
                          ),
                          zoomedBackgroundColor:
                              Color.fromRGBO(240, 240, 240, 1.0),
                          hideStatusBarWhileZooming: true,
                          onZoomStart: () {
                            print('Zoom started');
                          },
                          onZoomEnd: () {
                            print('Zoom finished');
                          },
                        ),
                      ),
                    ))
                .toList(),
          );
        },
      ),
    );
  }

  Widget _contacto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: widget.emprendimiento.webSite != "",
                child: GestureDetector(
                  onTap: () async {
                    if (await canLaunch(
                        'https://' + widget.emprendimiento.webSite)) {
                      await launch('https://' + widget.emprendimiento.webSite);
                    } else {
                      throw 'Could not launch ' +
                          'https://' +
                          widget.emprendimiento.webSite;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/computadora.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.urlInstagram != "",
                child: GestureDetector(
                  onTap: () async {
                    String usuarioInstagram =
                        widget.emprendimiento.urlInstagram;
                    usuarioInstagram = usuarioInstagram.replaceAll("@", "");
                    if (await canLaunch(
                        "https://www.instagram.com/" + usuarioInstagram)) {
                      await launch(
                          "https://www.instagram.com/" + usuarioInstagram);
                    } else {
                      throw 'Could not launch ' +
                          "https://www.instagram.com/" +
                          usuarioInstagram;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/instagram.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.urlFb != "",
                child: GestureDetector(
                  onTap: () async {
                    if (await canLaunch(widget.emprendimiento.urlFb)) {
                      await launch(widget.emprendimiento.urlFb);
                    } else {
                      throw 'Could not launch ' + widget.emprendimiento.urlFb;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/facebook.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.phone != "",
                child: GestureDetector(
                  onTap: () async {
                    var whatsappUrl;
                    if (Platform.isIOS) {
                      whatsappUrl =
                          "whatsapp://wa.me/" + widget.emprendimiento.phone;
                    } else {
                      whatsappUrl = "whatsapp://send?phone=" +
                          widget.emprendimiento.phone;
                    }

                    if (await canLaunch(whatsappUrl)) {
                      await launch(whatsappUrl);
                    } else {
                      final snackBar = SnackBar(
                        content:
                            Text("Error: Necesitas tener instalado Whatsapp"),
                      );

                      Scaffold.of(context).showSnackBar(snackBar);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/whatsapp.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.mail != "",
                child: GestureDetector(
                  onTap: () async {
                    var correoUrl = 'mailto:' +
                        widget.emprendimiento.mail +
                        '?subject=CaxenApp';

                    if (await canLaunch(correoUrl)) {
                      await launch(correoUrl);
                    } else {
                      throw 'Could not launch ' + widget.emprendimiento.email;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/gmail.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.emprendimiento.latitude != null &&
                    widget.emprendimiento.longitude != null,
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OnlyEmprendimientoMapScreen(
                                  emprendimiento: widget.emprendimiento,
                                )));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 5, right: 5),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/marker.png",
                          width: 27,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget divisor() {
    return Divider(
      height: 15,
      color: Colors.black54,
    );
  }

  Widget _seccionEliminar(Emprendimiento emprendimiento, BuildContext ctx) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/eliminar.png",
                  width: 20,
                ),
              ),
              Text(
                'Eliminar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () async {
          final snackBar = SnackBar(
            content: Text('¿Estás seguro de eliminarlo?'),
            action: SnackBarAction(
              label: 'Confirmar',
              onPressed: () {
                this.eliminar(widget.emprendimiento, ctx);
              },
            ),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(ctx).showSnackBar(snackBar);
        },
      ),
    );
  }

  Widget _seccionEditar(Emprendimiento emprendimiento) {
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Image.asset(
                  "assets/editar.png",
                  width: 20,
                ),
              ),
              Text(
                'Editar',
                style: TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EmprendimientoActualizarScreen(
                      emprendimiento: widget.emprendimiento,
                    )),
          );
        },
      ),
    );
  }

  void eliminar(Emprendimiento emprendimiento, BuildContext ctx) async {
    progressDialog.style(message: "Eliminando...");
    await progressDialog.show();

    try {
      await emprendimientosFunciones.eliminar(emprendimiento);

      await progressDialog.hide();

      Navigator.of(ctx).pop();
      Navigator.of(ctx).pop();
      Navigator.push(
          ctx, MaterialPageRoute(builder: (ctx) => UserProfileScreen()));
    } catch (e) {
      await progressDialog.hide();

      final snackBar = SnackBar(
        content: Text("Error: " + e.toString()),
      );

      Scaffold.of(ctx).showSnackBar(snackBar);
    }
  }
}
