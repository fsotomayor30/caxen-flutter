import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/emprendimientos/screen/emprendimiento_crear_screen.dart';
import 'package:app/modulos/emprendimientos/widgets/mis_emprendimientos_list_tile.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class MisEmprendimientostListView extends StatefulWidget {
  final Usuario usuario;

  MisEmprendimientostListView({@required this.usuario});

  @override
  _MisEmprendimientosListViewState createState() =>
      _MisEmprendimientosListViewState();
}

class _MisEmprendimientosListViewState
    extends State<MisEmprendimientostListView> {
  final dbRef = FirebaseDatabase.instance.reference().child("entrepreneurship");
  List<Emprendimiento> lists = <Emprendimiento>[];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) {
        return Expanded(
            child: Column(children: <Widget>[
          Expanded(
              child: FutureBuilder(
                  future: dbRef
                      .orderByChild("email")
                      .equalTo(widget.usuario.email)
                      .once(),
                  builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      lists.clear();
                      Map<dynamic, dynamic> values = snapshot.data.value;

                      if (values == null) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.sadTear,
                                  size: 80,
                                  color: Colors.black45,
                                ),
                                Text("No tienes emprendimientos"),
                                this._botonSubirEmprendimiento()
                              ],
                            ),
                          ),
                        );
                      } else {
                        List<dynamic> list = values.values.toList();

                        list.sort((a, b) {
                          return b["date"].compareTo(a['date']);
                        });

                        list.forEach((element) {
                          Emprendimiento emprendimiento =
                              Emprendimiento.fromSnapshot(element);
                          if (emprendimiento.visible) {
                            lists.add(emprendimiento);
                          }
                        });
                        print(lists.length);
                        if (lists.length == 0) {
                          return Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  FaIcon(
                                    FontAwesomeIcons.sadTear,
                                    size: 80,
                                    color: Colors.black45,
                                  ),
                                  Text("No tienes emprendimientos"),
                                  this._botonSubirEmprendimiento()
                                ],
                              ),
                            ),
                          );
                        } else {
                          return new ListView.builder(
                              shrinkWrap: true,
                              itemCount: lists.length,
                              itemBuilder: (BuildContext context, int index) {
                                return MisEmprendimientosListTile(
                                  usuario: globalController.usuario,
                                  emprendimiento: lists[index],
                                );
                              });
                        }
                      }
                    }

                    return Center(
                      child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator()),
                    );
                  }))
        ]));
      },
    );
  }

  Widget _botonSubirEmprendimiento() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new OutlineButton(
        shape: StadiumBorder(),
        textColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Publicar Emprendimiento',
            style: TextStyle(fontSize: 20),
          ),
        ),
        borderSide:
            BorderSide(color: Colors.blue, style: BorderStyle.solid, width: 1),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EmprendimientoCrearScreen(
                      region: "Todas",
                      categoria: "Todas",
                    )),
          );
        },
      ),
    );
  }
}
