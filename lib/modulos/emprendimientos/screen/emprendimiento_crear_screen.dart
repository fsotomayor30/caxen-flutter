import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/emprendimientos/widgets/emprendimiento_form.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EmprendimientoCrearScreen extends StatefulWidget {
  final String categoria;
  final String region;

  EmprendimientoCrearScreen({@required this.categoria, @required this.region});

  @override
  _EmprendimientoCrearScreenState createState() =>
      _EmprendimientoCrearScreenState();
}

class _EmprendimientoCrearScreenState extends State<EmprendimientoCrearScreen> {
  @override
  void initState() {
    super.initState();
    print("[CREAR EMPRENDIMIENTO] [CATEGORIA] " + widget.categoria);
    print("[CREAR EMPRENDIMIENTO] [REGION] " + widget.region);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (GlobalController globalController) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: EMPRENDIMIENTOS_COLOR,
              centerTitle: true,
              title: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: AutoSizeText(
                    "Emprendimientos",
                    textAlign: TextAlign.center,
                    maxFontSize: 18,
                    minFontSize: 10,
                  )),
              actions: <Widget>[
                GestureDetector(
                  onTap: () {
                    _dialogAyuda();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: Icon(
                      Icons.help,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                )
              ],
            ),
            body:
                //MyPages()
                EmprendimientoForm(
                    emprendimiento: Emprendimiento(
                        category: widget.categoria,
                        region: widget.region,
                        email: globalController.usuario.email,
                        imagePerfil: globalController.usuario.photoProfile,
                        name: globalController.usuario.displayName)));
      },
    );
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes publicar un nuevo emprendimiento",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
