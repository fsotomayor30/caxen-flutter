import 'package:app/modelos/emprendimiento.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/emprendimientos/widgets/emprendimiento_form.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmprendimientoActualizarScreen extends StatefulWidget {
  final Emprendimiento emprendimiento;

  EmprendimientoActualizarScreen({@required this.emprendimiento});

  @override
  _EmprendimientoActualizarScreenState createState() =>
      _EmprendimientoActualizarScreenState();
}

class _EmprendimientoActualizarScreenState
    extends State<EmprendimientoActualizarScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: EMPRENDIMIENTOS_COLOR,
          centerTitle: true,
          title: Container(
              width: MediaQuery.of(context).size.width / 2,
              child: AutoSizeText(
                "Emprendimientos",
                textAlign: TextAlign.center,
                maxFontSize: 18,
                minFontSize: 10,
              )),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                _dialogAyuda();
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(
                  Icons.help,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            )
          ],
        ),
        body: EmprendimientoForm(emprendimiento: widget.emprendimiento));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aqui puedes actualizar la información de tu emprendimiento",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
