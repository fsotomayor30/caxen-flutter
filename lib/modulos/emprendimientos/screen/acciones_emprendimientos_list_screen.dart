import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/autenticacion/screens/autenticacion_screen.dart';
import 'package:app/modulos/emprendimientos/screen/emprendimiento_crear_screen.dart';
import 'package:app/modulos/emprendimientos/screen/emprendimientos_list_screen.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccionesEmprendimientosListScreen extends StatefulWidget {
  final String categoria;

  AccionesEmprendimientosListScreen({@required this.categoria});

  @override
  _AccionesEmprendimientosListState createState() =>
      _AccionesEmprendimientosListState();
}

class _AccionesEmprendimientosListState
    extends State<AccionesEmprendimientosListScreen> {
  @override
  void initState() {
    print("[CATEGORIA EMPRENDIMIENTOS] " + widget.categoria);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      backgroundColor: EMPRENDIMIENTOS_COLOR,
      title: Container(
          width: Get.width * .7,
          child: AutoSizeText(
            "¿Que deseas hacer?",
            textAlign: TextAlign.center,
            maxFontSize: 18,
            minFontSize: 10,
          )),
      centerTitle: true,
      actions: <Widget>[
        GestureDetector(
          onTap: () {
            _dialogAyuda();
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Icon(
              Icons.help,
              color: Colors.white,
              size: 30,
            ),
          ),
        )
      ],
    );

    return GetBuilder<GlobalController>(
        builder: (GlobalController globalController) => Scaffold(
              appBar: appBar,
              body: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/img_caxen_background.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 48.0,
                    left: 10.0,
                    right: 10.0,
                    child: Container(
                      height: (Get.height - appBar.preferredSize.height) * .35,
                      child: GestureDetector(
                        onTap: () async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          String uuid = prefs.getString('uuid');
                          String displayName = prefs.getString('displayName');
                          String email = prefs.getString('email');
                          String photoProfile =
                              prefs.getString('photoProfile') ?? '';

                          if (uuid == null &&
                              displayName == null &&
                              email == null) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AutenticacionScreen()));
                          } else {
                            Usuario usuario = new Usuario(
                                uid: uuid,
                                displayName: displayName,
                                email: email,
                                photoProfile: photoProfile);
                            globalController.setUsuario(usuario);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      EmprendimientosListScreen(
                                        categoria: widget.categoria,
                                      )),
                            );
                          }
                        },
                        child: Card(
                          elevation: 8.0,
                          color: EMPRENDIMIENTOS_COLOR,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FaIcon(
                                FontAwesomeIcons.rocket,
                                size: 60,
                                color: Colors.white,
                              ),
                              Text(
                                "Ver Emprendimientos",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 48.0,
                    left: 10.0,
                    right: 10.0,
                    child: Container(
                      height: (Get.height - appBar.preferredSize.height) * .35,
                      child: GestureDetector(
                        onTap: () async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          String uuid = prefs.getString('uuid');
                          String displayName = prefs.getString('displayName');
                          String email = prefs.getString('email');
                          String photoProfile =
                              prefs.getString('photoProfile') ?? '';

                          if (uuid == null &&
                              displayName == null &&
                              email == null) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AutenticacionScreen()));
                          } else {
                            Usuario usuario = new Usuario(
                                uid: uuid,
                                displayName: displayName,
                                email: email,
                                photoProfile: photoProfile);
                            globalController.setUsuario(usuario);

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      EmprendimientoCrearScreen(
                                        categoria: widget.categoria,
                                        region: "Todas",
                                      )),
                            );
                          }
                        },
                        child: Card(
                          color: EMPRENDIMIENTOS_COLOR,
                          elevation: 8.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FaIcon(
                                FontAwesomeIcons.plus,
                                size: 60,
                                color: Colors.white,
                              ),
                              Text(
                                "Publica tu emprendimiento",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes decidir si quieres conocer los emprendimientos publicados o publicar uno nuevo.",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
