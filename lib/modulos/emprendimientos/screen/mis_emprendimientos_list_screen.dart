import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/emprendimientos/widgets/mis_emprendimientos_list_view.dart';
import 'package:app/shared/colors/root_colors.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';

class MisEmprendimientosListScreen extends StatefulWidget {
  final Usuario usuario;

  MisEmprendimientosListScreen({@required this.usuario});

  @override
  _MisEmprendimientosListState createState() => _MisEmprendimientosListState();
}

class _MisEmprendimientosListState extends State<MisEmprendimientosListScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    final double width = MediaQuery.of(context).size.width - 20;
    final double height = MediaQuery.of(context).size.height - 20;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: EMPRENDIMIENTOS_COLOR,
          title: Container(
              width: mediaQueryData.width / 2,
              child: AutoSizeText(
                "Mis Emprendimientos",
                maxLines: 2,
                textAlign: TextAlign.center,
                maxFontSize: 18,
                minFontSize: 10,
              )),
          centerTitle: true,
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                this._dialogAyuda();
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(
                  Icons.help,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            )
          ],
        ),
        body: Container(
            width: mediaQueryData.width,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MisEmprendimientostListView(
                    usuario: widget.usuario,
                  ),
                ],
              ),
            )));
  }

  void _dialogAyuda() {
    EasyDialog(
      title: Text(
        "Información",
        style: TextStyle(fontWeight: FontWeight.bold),
        textScaleFactor: 1.2,
      ),
      description: Text(
        "Aquí puedes ver todos tus emprendimientos disponibles",
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
      ),
      topImage: AssetImage("assets/bannerdialog.png"),
      height: 180,
    ).show(context);
  }
}
