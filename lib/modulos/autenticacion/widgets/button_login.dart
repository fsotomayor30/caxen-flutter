import 'package:app/funciones/terminosycondiciones_funciones.dart';
import 'package:app/modulos/autenticacion/screens/autenticacion_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonLogin extends StatefulWidget {
  @override
  _ButtonLoginState createState() => _ButtonLoginState();
}

class _ButtonLoginState extends State<ButtonLogin> {
  final TerminosYCondicionesFunciones terminosYCondicionesFunciones =
      TerminosYCondicionesFunciones();
  bool cargandoLogin = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 5),
      child: Container(
        width: mediaQueryData.width * 0.8,
        height: 50,
        child: FlatButton(
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(25.0),
              side: BorderSide(color: Color(0xFFFFFFFF))),
          color: Colors.white70,
          textColor: Colors.black54,
          padding: EdgeInsets.all(8.0),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AutenticacionScreen()));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Ingresar",
                style: TextStyle(
                  fontFamily: "SFProTextSemibold",
                  fontSize: 20.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
