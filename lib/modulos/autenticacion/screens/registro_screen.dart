import 'package:app/funciones/usuario_funciones.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/autenticacion/screens/autenticacion_screen.dart';
import 'package:app/servicios/firebase_auth_service.dart';
import 'package:app/shared/error_catch.dart';
import 'package:app/shared/widgets/background_gradient.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:logger/logger.dart';
import 'package:progress_dialog/progress_dialog.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

class RegistroScreen extends StatefulWidget {
  @override
  _RegistroPageState createState() => _RegistroPageState();
}

class _RegistroPageState extends State<RegistroScreen> {
  FirebaseAuthService firebaseAuthService = new FirebaseAuthService();
  UsuarioFunciones usuarioFunciones = new UsuarioFunciones();

  ProgressDialog progressDialog;

  String nombre = "";
  String correo = "";
  String contrasena = "";
  String contrasenaRepetida = "";

  final nombreController = TextEditingController();
  final correoController = TextEditingController();
  final passwordController = TextEditingController();
  final repeticionPasswordController = TextEditingController();

  final _formKeyRegistro = GlobalKey<FormState>();

  @override
  void dispose() {
    nombreController.dispose();
    correoController.dispose();
    passwordController.dispose();
    repeticionPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Registrando...");

    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    Widget _welcomeText() {
      return Text(
        "Ingresa tu datos!",
        textAlign: TextAlign.left,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: backgroundGradient(),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Stack(children: <Widget>[
                Container(
                  width: width,
                  height: height * .25,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/backgroundlogin.png"),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100.0),
                        bottomRight: Radius.circular(100.0)),
                    color: Colors.white,
                  ),
                ),
                Positioned(
                  top: (height * .28) - 160,
                  left: 45,
                  right: 45,
                  child: Image.asset(
                    "assets/caxen_intro.png",
                    height: 130,
                  ),
                ),
                Positioned(
                  top: 60,
                  left: 20,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: FaIcon(
                        FontAwesomeIcons.arrowLeft,
                        color: Colors.white,
                      )),
                ),
              ]),
              Container(
                  height: height * .75,
                  width: width * .9,
                  child: Form(
                    key: _formKeyRegistro,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 5),
                        _welcomeText(),
                        SizedBox(height: 5),
                        _inputNombre(),
                        SizedBox(height: 5),
                        _inputEmail(),
                        SizedBox(height: 15),
                        _inputPassword(width),
                        SizedBox(
                          height: 15,
                        ),
                        _inputRepetirPassword(width),
                        SizedBox(
                          height: 15,
                        ),
                        _botonRegistrarme()
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget _inputNombre() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Nombre",
          style: TextStyle(color: Colors.white),
        ),
        SizedBox(
          height: 0,
        ),
        TextFormField(
          keyboardType: TextInputType.text,
          controller: nombreController,
          style: TextStyle(color: Colors.white),
          cursorColor: Colors.white,
          decoration: InputDecoration(
            hintText: 'Ingrese su nombre',
            prefixIcon: Icon(Icons.person, color: Colors.white),
            hintStyle: TextStyle(color: Colors.white),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
          ),
          onSaved: (String value) {
            setState(() {
              nombre = value;
            });
          },
          validator: (String name) {
            if (name.length < 2) {
              return 'El nombre debe contener más de 2 caracteres';
            }
            return null;
          },
        ),
      ],
    );
  }

  Widget _inputEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Correo",
          style: TextStyle(color: Colors.white),
        ),
        SizedBox(
          height: 0,
        ),
        TextFormField(
          keyboardType: TextInputType.emailAddress,
          controller: correoController,
          style: TextStyle(color: Colors.white),
          cursorColor: Colors.white,
          decoration: InputDecoration(
            hintText: 'Ingrese su correo',
            prefixIcon: Icon(Icons.email, color: Colors.white),
            hintStyle: TextStyle(color: Colors.white),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
          ),
          onSaved: (String value) {
            setState(() {
              correo = value;
            });
          },
          validator: (String correo) {
            if (!EmailValidator.validate(correo)) {
              return 'Ingrese un correo válido';
            }
            return null;
          },
        ),
      ],
    );
  }

  Widget _inputPassword(double width) {
    return Padding(
      padding: const EdgeInsets.only(right: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Contraseña",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 0,
          ),
          TextFormField(
            obscureText: true,
            style: TextStyle(color: Colors.white),
            controller: passwordController,
            cursorColor: Colors.white,
            decoration: InputDecoration(
              hintText: 'Ingrese su contraseña',
              prefixIcon: Icon(Icons.lock, color: Colors.white),
              hintStyle: TextStyle(color: Colors.white),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            onChanged: (String value) {
              setState(() {
                contrasena = value;
              });
            },
            onSaved: (String value) {
              setState(() {
                contrasena = value;
              });
            },
            validator: (String password) {
              if (password.length < 7) {
                return 'La contraseña debe contener más de 6 caracteres';
              }
              return null;
            },
          ),
        ],
      ),
    );
  }

  Widget _inputRepetirPassword(double width) {
    return Padding(
      padding: const EdgeInsets.only(left: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Repite la Contraseña",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 0,
          ),
          TextFormField(
            controller: repeticionPasswordController,
            obscureText: true,
            style: TextStyle(color: Colors.white),
            cursorColor: Colors.white,
            decoration: InputDecoration(
              hintText: 'Repita su contraseña',
              prefixIcon: Icon(Icons.lock, color: Colors.white),
              hintStyle: TextStyle(color: Colors.white),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            onChanged: (String value) {
              setState(() {
                contrasenaRepetida = value;
              });
            },
            onSaved: (String value) {
              setState(() {
                contrasenaRepetida = value;
              });
            },
            validator: (String password) {
              if (password.length < 7) {
                return 'La contraseña debe contener más de 6 caracteres';
              }
              if (password != contrasena) {
                return 'Las contraseñas deben coincidir';
              }
              return null;
            },
          ),
        ],
      ),
    );
  }

  Widget _botonRegistrarme() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15),
      width: double.infinity,
      child: Builder(
        builder: (context) => RaisedButton(
          elevation: 5.0,
          onPressed: () async {
            if (_formKeyRegistro.currentState.validate()) {
              _formKeyRegistro.currentState.save();

              try {
                await progressDialog.show();
                FirebaseUser firebaseUser =
                    await firebaseAuthService.signUp(correo, contrasena);
                Usuario usuario = Usuario(
                    uid: firebaseUser.uid,
                    displayName: nombre,
                    email: firebaseUser.email,
                    photoProfile: "");

                await usuarioFunciones.agregarUsuario(usuario).then((value) {
                  logger.d("RESPUESTA REGISTRO-> " + value.toString());
                  nombreController.clear();
                  correoController.clear();
                  passwordController.clear();
                  repeticionPasswordController.clear();
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                });

                await progressDialog.hide();
                await firebaseAuthService.sendEmailVerify(firebaseUser);

                final snackBar = SnackBar(
                    content: Text(
                        "Te hemos un enviado un correo de validación, revísalo"),
                    action: SnackBarAction(
                      label: 'Iniciar Sesión',
                      onPressed: () {
                        Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AutenticacionScreen()));
                      },
                    ));
                Scaffold.of(context).showSnackBar(snackBar);
              } catch (e) {
                final snackBar = SnackBar(
                  content: Text(errorCatch(e.code)),
                );
                Scaffold.of(context).showSnackBar(snackBar);
                await progressDialog.hide();
              }
            }
          },
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.white,
          child: Text(
            "REGISTRARME",
            style: TextStyle(
                color: Color(0xFF527DAA),
                letterSpacing: 1.5,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
