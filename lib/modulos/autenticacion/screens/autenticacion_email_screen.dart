import 'package:app/funciones/terminosycondiciones_funciones.dart';
import 'package:app/funciones/usuario_funciones.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/autenticacion/screens/olvide_password_screen.dart';
import 'package:app/modulos/autenticacion/screens/registro_screen.dart';
import 'package:app/modulos/principal/principal_screen.dart';
import 'package:app/servicios/firebase_auth_service.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/error_catch.dart';
import 'package:app/shared/widgets/background_gradient.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

class AutenticacionEmailScreen extends StatefulWidget {
  @override
  _AutenticacionEmailPageState createState() => _AutenticacionEmailPageState();
}

class _AutenticacionEmailPageState extends State<AutenticacionEmailScreen> {
  bool cargandoLogin = false;

  final _formKey = GlobalKey<FormState>();

  String correo;
  String password;

  final FirebaseAuthService _firebaseAuthService = new FirebaseAuthService();
  final TerminosYCondicionesFunciones terminosYCondicionesFunciones =
      TerminosYCondicionesFunciones();
  final UsuarioFunciones usuarioFunciones = new UsuarioFunciones();

  ProgressDialog progressDialog;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Iniciando Sesión...");

    Widget _inputEmail() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Correo",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(color: Colors.white),
            cursorColor: Colors.white,
            decoration: InputDecoration(
              hintText: 'Ingrese su correo',
              prefixIcon: Icon(Icons.email, color: Colors.white),
              hintStyle: TextStyle(color: Colors.white),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            validator: (String correo) {
              if (!EmailValidator.validate(correo)) {
                return "Ingrese un correo válido";
              }
              return null;
            },
            onSaved: (String correo) {
              setState(() {
                this.correo = correo;
              });
            },
          ),
        ],
      );
    }

    Widget _inputPassword() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Contraseña",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            obscureText: true,
            style: TextStyle(color: Colors.white),
            cursorColor: Colors.white,
            decoration: InputDecoration(
              hintText: 'Ingrese su contraseña',
              prefixIcon: Icon(Icons.lock, color: Colors.white),
              hintStyle: TextStyle(color: Colors.white),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            validator: (String password) {
              if (password.isEmpty) {
                return "Ingrese la contraseña";
              }
              return null;
            },
            onSaved: (String password) {
              setState(() {
                this.password = password;
              });
            },
          ),
        ],
      );
    }

    Widget _botonOlvidePassword() {
      return Container(
        alignment: Alignment.centerRight,
        child: FlatButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => OlvidePasswordScreen()));
          },
          padding: EdgeInsets.only(right: 0),
          child: Text(
            "¿Olvidaste la contraseña?",
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }

    Widget _botonRegistrate() {
      return GestureDetector(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => RegistroScreen()));
        },
        child: RichText(
          text: TextSpan(children: [
            TextSpan(
                text: '¿No estás registrado?',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w400)),
            TextSpan(
                text: ' Regístrate',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold))
          ]),
        ),
      );
    }

    Widget _botonLogin() {
      GlobalController _globalController = Get.find();
      return Container(
          padding: EdgeInsets.symmetric(vertical: 15),
          width: double.infinity,
          child: Builder(
              builder: (context) => RaisedButton(
                    elevation: 5.0,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        await progressDialog.show();
                        try {
                          FirebaseUser user = await _firebaseAuthService.signIn(
                              correo, password);
                          if (user.isEmailVerified) {
                            Usuario usuario =
                                await usuarioFunciones.obtenerUsuario(user.uid);

                            String gcmToken =
                                await this._firebaseMessaging.getToken();
                            usuario.tokenRing = gcmToken;

                            await usuarioFunciones.agregarUsuario(usuario);

                            logger.d("[USUARIO] " + usuario.toString());

                            _globalController.setUsuario(usuario);

                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            await prefs.setString('uuid', usuario.uid);
                            await prefs.setString(
                                'displayName', usuario.displayName);
                            await prefs.setString('email', usuario.email);
                            await prefs.setString(
                                'photoProfile',
                                usuario.photoProfile == null
                                    ? ''
                                    : usuario.photoProfile);
                            await prefs.setString(
                                'tokenRing', usuario.tokenRing);

                            await terminosYCondicionesFunciones
                                .obtenerPorEmail(user.email)
                                .then((value) async {
                              if (value == null) {
                                await terminosYCondicionesFunciones
                                    .agregarTerminosYCondiciones(usuario);
                                logger.d(
                                    "Terminos y condiciones agregados para usuario: " +
                                        usuario.toString());
                              } else {
                                logger.d("Terminos y condiciones ya obtenidos");
                              }
                            });

                            await progressDialog.hide();
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => PrincipalScreen()),
                                (Route<dynamic> route) => false);
                          } else {
                            await progressDialog.hide();

                            final snackBar = SnackBar(
                              content: Text('Necesitas válidar tu correo'),
                              action: SnackBarAction(
                                label: 'Reenviar',
                                onPressed: () async {
                                  _firebaseAuthService.sendEmailVerify(user);
                                },
                              ),
                            );

                            Scaffold.of(context).showSnackBar(snackBar);
                          }
                        } catch (e) {
                          logger.d(e.code);

                          await progressDialog.hide();
                          final snackBar = SnackBar(
                            content: Text(errorCatch(e.code)),
                            action: SnackBarAction(
                              label: 'Cerrar',
                              onPressed: () {},
                            ),
                          );

                          Scaffold.of(context).showSnackBar(snackBar);
                        }
                      }
                    },
                    padding: EdgeInsets.all(15.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    color: Colors.white,
                    child: Text(
                      "INICIAR SESIÓN",
                      style: TextStyle(
                          color: Color(0xFF527DAA),
                          letterSpacing: 1.5,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )));
    }

    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    Widget _welcomeText() {
      return Text(
        "Ingresa tus datos!",
        textAlign: TextAlign.left,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
      );
    }

    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        decoration: backgroundGradient(),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: width,
                  height: height * .3,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/backgroundlogin.png"),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100.0),
                        bottomRight: Radius.circular(100.0)),
                    color: Colors.white,
                  ),
                ),
                Positioned(
                  top: (height * .34) - 160,
                  left: 40,
                  right: 40,
                  child: Image.asset(
                    "assets/caxen_intro.png",
                    height: 130,
                  ),
                ),
                Positioned(
                  top: 60,
                  left: 20,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: FaIcon(
                        FontAwesomeIcons.arrowLeft,
                        color: Colors.white,
                      )),
                ),
              ],
            ),
            Container(
                height: height * .6,
                width: width * .9,
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 5),
                        _welcomeText(),
                        SizedBox(
                          height: 10,
                        ),
                        _inputEmail(),
                        SizedBox(
                          height: 10,
                        ),
                        _inputPassword(),
                        _botonOlvidePassword(),
                        _botonLogin(),
                        _botonRegistrate()
                      ],
                    ),
                  ),
                )),
          ],
        ),
      ),
    ));
  }
}
