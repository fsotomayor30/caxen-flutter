import 'dart:io';

import 'package:app/funciones/terminosycondiciones_funciones.dart';
import 'package:app/funciones/usuario_funciones.dart';
import 'package:app/modelos/usuario.dart';
import 'package:app/modulos/autenticacion/screens/autenticacion_email_screen.dart';
import 'package:app/modulos/principal/principal_screen.dart';
import 'package:app/servicios/firebase_auth_service.dart';
import 'package:app/shared/controllers/global.controller.dart';
import 'package:app/shared/error_catch.dart';
import 'package:app/shared/widgets/background_gradient.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

class AutenticacionScreen extends StatefulWidget {
  @override
  _AutenticacionPageState createState() => _AutenticacionPageState();
}

class _AutenticacionPageState extends State<AutenticacionScreen> {
  bool cargandoLogin = false;

  final _formKey = GlobalKey<FormState>();

  String correo;
  String password;

  final FirebaseAuthService _firebaseAuthService = new FirebaseAuthService();
  final TerminosYCondicionesFunciones terminosYCondicionesFunciones =
      TerminosYCondicionesFunciones();
  final UsuarioFunciones usuarioFunciones = new UsuarioFunciones();

  ProgressDialog progressDialog;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool isIOS13 = false;

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      //check for ios if developing for both android & ios
//      AppleSignIn.onCredentialRevoked.listen((_) {
//        print("Credentials revoked");
//      });
      DeviceInfoPlugin().iosInfo.then((IosDeviceInfo iosDeviceInfo) {
        var version = iosDeviceInfo.systemVersion;
        if (version.contains('13') == true) {
          setState(() {
            isIOS13 = true;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Iniciando Sesión...");

    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return GetBuilder<GlobalController>(
        builder: (GlobalController globalController) => Scaffold(
                body: Stack(
              children: <Widget>[
                Positioned(
                  child: Container(decoration: backgroundGradient()),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: width,
                    height: height * .5,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/backgroundlogin.png"),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(100.0),
                          bottomRight: Radius.circular(100.0)),
                      color: Colors.white,
                    ),
                  ),
                ),
                Positioned(
                  top: (height * .5) - 140,
                  left: 40,
                  right: 40,
                  child: Image.asset(
                    "assets/caxen_intro.png",
                    height: 140,
                  ),
                ),
                Positioned(
                  top: 90,
                  left: 20,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: FaIcon(
                        FontAwesomeIcons.arrowLeft,
                        size: 50,
                        color: Colors.white,
                      )),
                ),
                Positioned(
                  top: (height * .5) + 10,
                  left: 20,
                  right: 20,
                  child: Container(
                      width: width * .9,
                      child: Column(
                        children: <Widget>[
                          _welcomeText(),
                          SizedBox(
                            height: 25,
                          ),
                          //_googleButton(width),
                          //SizedBox(height: 10,),
                          _emailButton(width),
                          SizedBox(height: 10),
                          Text(
                            "-   O   -",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 10),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
//                          isIOS13 ? Builder(
//                            builder: (context) => GestureDetector(
//                                onTap:() async{
//                                  await progressDialog.show();
//                                  try {
//                                    AuthService authService = AuthService();
//                                    Usuario user = await authService.signInWithApple(context, progressDialog);
//
//                                    if(user!=null){
//                                      String gcmToken = await this._firebaseMessaging.getToken();
//                                      Usuario usuario = await usuarioFunciones.obtenerUsuario(user.uid);
//
//                                      Usuario usuarioSave;
//                                      if(usuario==null){
//                                        usuarioSave = usuario;
//                                      }else{
//                                        usuarioSave = user;
//                                      }
//
//                                      usuarioSave.tokenRing = gcmToken;
//
//                                      usuarioFunciones.agregarUsuario(usuarioSave).then((bool respuesta) async {
//                                        if(respuesta){
//                                          StoreProvider.of<AppState>(context).dispatch(UpdateUsuarioAction(usuario: usuarioSave));
//
//                                          logger.d("[USUARIO] "+ usuarioSave.toString());
//
//                                          SharedPreferences prefs = await SharedPreferences.getInstance();
//                                          await prefs.setString('uuid', usuarioSave.uid);
//                                          await prefs.setString('displayName', usuarioSave.displayName == null ? 'Usuario Caxen' :usuarioSave.displayName);
//                                          await prefs.setString('email', usuarioSave.email);
//                                          await prefs.setString('photoProfile', usuarioSave.photoProfile == null ? '' : usuarioSave.photoProfile);
//                                          await prefs.setString('tokenRing', usuarioSave.tokenRing);
//
//                                          await terminosYCondicionesFunciones
//                                              .obtenerPorEmail(user.email)
//                                              .then((value) async {
//                                            if (value == null) {
//                                              await terminosYCondicionesFunciones.agregarTerminosYCondiciones(user);
//                                              logger.d("Terminos y condiciones agregados para usuario: " + user.toString());
//                                            } else {
//                                              logger.d("Terminos y condiciones ya obtenidos");
//                                            }
//                                          });
//                                          setState(() {
//                                            cargandoLogin = false;
//                                          });
//                                          await progressDialog.hide();
//                                          Navigator.of(context).pushAndRemoveUntil(
//                                              MaterialPageRoute(
//                                                  builder: (context) => PrincipalScreen()),
//                                                  (Route<dynamic> route) => false);
//                                        }
//                                      });
//                                    }else{
//                                      setState(() {
//                                        cargandoLogin = false;
//                                      });
//
//                                      final snackBar = SnackBar(
//                                        content: Text("No se pudo iniciar sesión"),
//                                        action: SnackBarAction(
//                                          label: 'Cerrar',
//                                          onPressed: () {},
//                                        ),
//                                      );
//                                      Scaffold.of(context).showSnackBar(snackBar);
//
//                                    }
//
//                                  } catch (e) {
//                                    setState(() {
//                                      cargandoLogin = false;
//                                    });
//
//                                    final snackBar = SnackBar(
//                                      content: Text("No se pudo iniciar sesión"),
//                                      action: SnackBarAction(
//                                        label: 'Cerrar',
//                                        onPressed: () {},
//                                      ),
//                                    );
//                                    Scaffold.of(context).showSnackBar(snackBar);
//                                  }
//                                },
//                                child: FaIcon(FontAwesomeIcons.apple, color: Colors.white, size: 35,)
//                            ),
//                          ) : Container(),
                              SizedBox(width: 15),

                              Platform.isAndroid
                                  ? Builder(
                                      builder: (context) => GestureDetector(
                                          onTap: () async {
                                            try {
                                              setState(() {
                                                cargandoLogin = true;
                                              });

                                              await progressDialog.show();

                                              FirebaseUser user =
                                                  await _firebaseAuthService
                                                      .singInWithGoogleAccount();
                                              Usuario usuario = Usuario(
                                                  uid: user.uid,
                                                  displayName: user.displayName,
                                                  email: user.email,
                                                  photoProfile: user.photoUrl);

                                              String gcmToken = await this
                                                  ._firebaseMessaging
                                                  .getToken();
                                              usuario.tokenRing = gcmToken;

                                              logger
                                                  .i("TOKEN RING " + gcmToken);
                                              usuarioFunciones
                                                  .agregarUsuario(usuario)
                                                  .then((bool respuesta) async {
                                                if (respuesta) {
                                                  globalController
                                                      .setUsuario(usuario);

                                                  logger.d("[USUARIO] " +
                                                      usuario.toString());

                                                  SharedPreferences prefs =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  await prefs.setString(
                                                      'uuid', usuario.uid);
                                                  await prefs.setString(
                                                      'displayName',
                                                      usuario.displayName);
                                                  await prefs.setString(
                                                      'email', usuario.email);
                                                  await prefs.setString(
                                                      'photoProfile',
                                                      usuario.photoProfile ==
                                                              null
                                                          ? ''
                                                          : usuario
                                                              .photoProfile);
                                                  await prefs.setString(
                                                      'tokenRing',
                                                      usuario.tokenRing);

                                                  await terminosYCondicionesFunciones
                                                      .obtenerPorEmail(
                                                          user.email)
                                                      .then((value) async {
                                                    if (value == null) {
                                                      await terminosYCondicionesFunciones
                                                          .agregarTerminosYCondiciones(
                                                              usuario);
                                                      logger.d(
                                                          "Terminos y condiciones agregados para usuario: " +
                                                              usuario
                                                                  .toString());
                                                    } else {
                                                      logger.d(
                                                          "Terminos y condiciones ya obtenidos");
                                                    }
                                                  });

                                                  setState(() {
                                                    cargandoLogin = false;
                                                  });
                                                  await progressDialog.hide();

                                                  Navigator.of(context)
                                                      .pushAndRemoveUntil(
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  PrincipalScreen()),
                                                          (Route<dynamic>
                                                                  route) =>
                                                              false);
                                                }
                                              });
                                            } catch (e) {
                                              setState(() {
                                                cargandoLogin = false;
                                              });
                                              await progressDialog.hide();
                                              logger.i(e.toString());
                                              final snackBar = SnackBar(
                                                content:
                                                    Text(errorCatch(e.code)),
                                                action: SnackBarAction(
                                                  label: 'Cerrar',
                                                  onPressed: () {},
                                                ),
                                              );

                                              Scaffold.of(context)
                                                  .showSnackBar(snackBar);
                                            }
                                          },
                                          child: FaIcon(
                                            FontAwesomeIcons.google,
                                            color: Colors.white,
                                            size: 28,
                                          )),
                                    )
                                  : Container()
                            ],
                          ),
                          _textoTerminosYCondiciones()
                        ],
                      )),
                ),
              ],
            )));
  }

  Widget _textoTerminosYCondiciones() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 15),
      child: RichText(
        text: TextSpan(
          text: '¿LeÍste nuestros',
          style: TextStyle(color: Colors.white, fontSize: 12),
          children: <TextSpan>[
            TextSpan(
                text: ' Términos y Condiciones',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    if (await canLaunch('https://caxenapp.com/#/TyC')) {
                      await launch('https://caxenapp.com/#/TyC');
                    } else {
                      throw 'Could not launch ' + 'https://caxenapp.com/#/TyC';
                    }
                  }),
            TextSpan(
              text: '? al registrarte los estás aceptando',
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ],
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _welcomeText() {
    return Text(
      "Bienvenido a la vitrina digital de Chile",
      textAlign: TextAlign.center,
      style: TextStyle(
          fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
    );
  }

  Widget _emailButton(double width) {
    return Container(
      width: width - 40,
      height: 48,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.white)),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AutenticacionEmailScreen()),
          );
        },
        color: Color(0xFF528DFA),
        textColor: Colors.white,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FaIcon(FontAwesomeIcons.envelope),
            SizedBox(
              width: 5,
            ),
            AutoSizeText(
              "Conecta con correo electrónico",
              minFontSize: 5,
              maxFontSize: 15,
            ),
          ],
        ),
      ),
    );
  }
}
