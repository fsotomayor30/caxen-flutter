import 'package:app/servicios/firebase_auth_service.dart';
import 'package:app/shared/error_catch.dart';
import 'package:app/shared/widgets/background_gradient.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';

class OlvidePasswordScreen extends StatefulWidget {
  @override
  _OlvidePasswordPageState createState() => _OlvidePasswordPageState();
}

class _OlvidePasswordPageState extends State<OlvidePasswordScreen> {
  FirebaseAuthService firebaseAuthService = new FirebaseAuthService();

  ProgressDialog progressDialog;
  final correoController = TextEditingController();

  String correo;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    correoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: "Recuperando contraseña...");

    Widget _inputEmail() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Ingresa tu Correo",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(color: Colors.white),
            cursorColor: Colors.white,
            controller: correoController,
            decoration: InputDecoration(
              hintText: 'Ingrese su correo',
              prefixIcon: Icon(Icons.email, color: Colors.white),
              hintStyle: TextStyle(color: Colors.white),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            validator: (String correo) {
              if (!EmailValidator.validate(correo)) {
                return 'Ingrese un correo válido';
              }
              return null;
            },
            onSaved: (String correo) {
              setState(() {
                this.correo = correo;
              });
            },
          ),
        ],
      );
    }

    Widget _botonRecuperar() {
      return Container(
          padding: EdgeInsets.symmetric(vertical: 25),
          width: double.infinity,
          child: Builder(
            builder: (context) => RaisedButton(
              elevation: 5.0,
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  await progressDialog.show();

                  try {
                    await firebaseAuthService.resetPassoword(correo);
                    await progressDialog.hide();
                    correoController.clear();
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    final snackBar = SnackBar(
                        content: Text(
                            "Revisa tu correo, te enviamos los pasos para reestablecer la contraseña"));
                    Scaffold.of(context).showSnackBar(snackBar);
                  } catch (e) {
                    await progressDialog.hide();
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    final snackBar =
                        SnackBar(content: Text(errorCatch(e.code)));
                    print(e.toString());
                    Scaffold.of(context).showSnackBar(snackBar);
                  }
                }
              },
              padding: EdgeInsets.all(15.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: Colors.white,
              child: Text(
                "RECUPERAR",
                style: TextStyle(
                    color: Color(0xFF527DAA),
                    letterSpacing: 1.5,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ));
    }

    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        decoration: backgroundGradient(),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: width,
                  height: height * .35,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/backgroundlogin.png"),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100.0),
                        bottomRight: Radius.circular(100.0)),
                    color: Colors.white,
                  ),
                ),
                Positioned(
                  top: (height * .38) - 160,
                  left: 45,
                  right: 45,
                  child: Image.asset(
                    "assets/caxen_intro.png",
                    height: 130,
                  ),
                ),
                Positioned(
                  top: 60,
                  left: 20,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: FaIcon(
                        FontAwesomeIcons.arrowLeft,
                        color: Colors.white,
                      )),
                ),
              ],
            ),
            Container(
                height: height * .6,
                width: width * .9,
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      _inputEmail(),
                      SizedBox(
                        height: 5,
                      ),
                      _botonRecuperar()
                    ],
                  ),
                )),
          ],
        ),
      ),
    ));
  }
}
