import 'package:app/modelos/slide.dart';
import 'package:app/modulos/introduccion/screens/slide_dots.dart';
import 'package:app/modulos/introduccion/screens/widgets/slide_item.dart';
import 'package:app/modulos/principal/principal_screen.dart';
import 'package:app/shared/widgets/background_gradient.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:shared_preferences/shared_preferences.dart';

class IntroductionSliderScreen extends StatefulWidget {
  IntroductionSliderScreen({Key key}) : super(key: key);

  @override
  IntroductionSliderScreenState createState() =>
      new IntroductionSliderScreenState();
}

class IntroductionSliderScreenState extends State<IntroductionSliderScreen> {
  int _currentPage = 0;
  final PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  DecorationImage backgroundChooser(int index) {
    switch (index) {
      case 0:
        return DecorationImage(
          image: AssetImage("assets/background_uno.png"),
          fit: BoxFit.cover,
        );
        break;
      case 1:
        return DecorationImage(
            image: AssetImage("assets/background_dos.png"), fit: BoxFit.cover);
        break;
      case 2:
        return DecorationImage(
            image: AssetImage("assets/background_tres.png"), fit: BoxFit.cover);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
            body: Stack(
              children: <Widget>[
                Positioned(
                  child: Container(decoration: backgroundGradient()),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: width,
                    height: height * .7,
                    decoration: BoxDecoration(
                      image: backgroundChooser(_currentPage),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(100.0),
                          bottomRight: Radius.circular(100.0)),
                      color: Colors.white,
                    ),
                  ),
                ),
                Positioned(
                  top: (height * .7) - 140,
                  left: 40,
                  right: 40,
                  child: Image.asset(
                    "assets/caxen_intro.png",
                    height: 140,
                  ),
                ),
                Positioned(
                  bottom: 40,
                  left: 20,
                  right: 20,
                  child: Container(
                    height: height * .2,
                    width: width * .9,
                    child: PageView.builder(
                      scrollDirection: Axis.horizontal,
                      onPageChanged: _onPageChanged,
                      controller: _pageController,
                      itemCount: slideList.length,
                      itemBuilder: (context, i) => SlideItem(i),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 15,
                  right: 100,
                  left: 100,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      for (int i = 0; i < slideList.length; i++)
                        if (i == _currentPage)
                          SlideDots(true)
                        else
                          SlideDots(false)
                    ],
                  ),
                ),
                Visibility(
                  visible: _currentPage == 2,
                  child: Positioned(
                      bottom: 10,
                      right: 15,
                      child: GestureDetector(
                          onTap: () async {

                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => PrincipalScreen()),
                                    (Route<dynamic> route) => false);


/*                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            String uuid = prefs.getString('uuid');
                            String displayName = prefs.getString('displayName');
                            String email = prefs.getString('email');
                            String photoProfile = prefs.getString('photoProfile')??'';

                            if(uuid == null && displayName==null && email == null){

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          AutenticacionScreen()));
                            }else{
                              Usuario usuario = new Usuario(uid: uuid, displayName: displayName, email: email, photoProfile: photoProfile);
                              StoreProvider.of<AppState>(context).dispatch(UpdateUsuarioAction(usuario: usuario));

                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                      builder: (context) => PrincipalScreen()),
                                      (Route<dynamic> route) => false);
                            }*/
                          },
                          child: FaIcon(
                            FontAwesomeIcons.arrowRight,
                            color: Colors.white,
                          ))),
                ),
              ],
            ),
          );

  }
}
