import 'dart:io' as Io;

import 'package:app/shared/image/image_information.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:logger/logger.dart';


var logger = Logger(
  printer: PrettyPrinter(),
);

class FirebaseStorageService {
  StorageReference storageReference;

  Future<String> uploadFile(Io.File file, String filename, String route, {bool comprimir = true}) async {

    logger.d("SUBIENDO DOCUMENTO");

    storageReference = FirebaseStorage.instance.ref().child(route + "/" + filename);
    StorageUploadTask uploadTask;

    if(comprimir){
      final archivoComprimido = await testCompressAndGetFile(file, file.path+"_compressed.jpg");
      uploadTask = storageReference.putFile(archivoComprimido);
    }else{
      uploadTask = storageReference.putFile(file);
    }


    final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
    final String url = (await downloadUrl.ref.getDownloadURL());
    logger.d("DOCUMENTO SUBIDO nombre: " + filename);
    logger.d("DOCUMENTO SUBIDO URL: " + url);
    return url;
  }

  Future<void> deleteFile(String name) async {
    print("========================");
    print("ELIMINANDO DOCUMENTO: " + name);
    print("========================");

    try {
      await FirebaseStorage.instance.ref().child(name).delete();
      print("========================");
      print("DOCUMENTO ELIMINADO");
      print("DOCUMENTO ELIMINADO name: " + name);
      print("========================");
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<Io.File> testCompressAndGetFile(Io.File file, String targetPath) async {
    logger.d("ARCHIVO COMPRIMIDO QUALITY: "+COMPRESSION_IMAGE.toString());

    var result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path, targetPath,
      quality: COMPRESSION_IMAGE
    );

    print(file.lengthSync());
    print(result.lengthSync());

    return result;
  }

}
