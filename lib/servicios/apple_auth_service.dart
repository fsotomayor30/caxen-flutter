import 'package:app/modelos/usuario.dart';
import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AuthService {

  Future<Usuario> signInWithApple(BuildContext context, ProgressDialog progressDialog) async {
    if(await AppleSignIn.isAvailable()){
      AuthResult _res;

      // 1. perform the sign-in request
      AuthorizationResult result = await AppleSignIn.performRequests(
          [AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])]);
      // 2. check the result
      switch (result.status) {
        case AuthorizationStatus.authorized:
          final AppleIdCredential appleIdCredential = result.credential;
          OAuthProvider oAuthProvider = new OAuthProvider(providerId: "apple.com");
          final AuthCredential credential = oAuthProvider.getCredential(
            idToken: String.fromCharCodes(appleIdCredential.identityToken),
            accessToken: String.fromCharCodes(appleIdCredential.authorizationCode),
          );
          _res = await FirebaseAuth.instance.signInWithCredential(credential);
          if(_res.user.displayName == null || _res.user.displayName.contains("null")){
            return null;
          }else{
            return Usuario(photoProfile: '', displayName: _res.user.displayName, email: appleIdCredential.email, uid: _res.user.uid);
          }
        break;
        case AuthorizationStatus.error:
          print("Sign in failed: ${result.error.localizedDescription}");
//          throw PlatformException(
//            code: 'ERROR_AUTHORIZATION_DENIED',
//            message: result.error.toString(),
//          );
          await progressDialog.hide();
          break;

        case AuthorizationStatus.cancelled:
          throw PlatformException(
            code: 'ERROR_ABORTED_BY_USER',
            message: 'Sign in aborted by user',
          );
          break;

      }
      return null;
    }else{
      print("LOGIN NO DISPONIBLE");
      return null;
    }
  }
}