import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationModule {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final _mensajeStreamController = StreamController<List>.broadcast();
  Stream<List> get mensajes => _mensajeStreamController.stream;

  initNotifications() {
    _firebaseMessaging.requestNotificationPermissions();

    _firebaseMessaging.configure(
      onMessage: (message) async {
        List args = [];
        if (Platform.isAndroid) {
          args.add({
            'listenTo' : 'onMessage',
            'code': message['data']['code'] ?? 'no-data',
            'title': message['data']['title'] ?? 'no-data',
            'body': message['data']['body'] ?? 'no-data',
            'notificationId': message['data']['notificationId'] ?? 'no-data'
          });
        } else {
          args.add({
            'listenTo' : 'onMessage',
            'code': message['code'] ?? 'no-data',
            'title': message['title'] ?? 'no-data',
            'body': message['body'] ?? 'no-data',
            'notificationId': message['notificationId'] ?? 'no-data'
          });
        }

        _mensajeStreamController.sink.add(args);
      },
      onLaunch: (message) async {
        List args = [];
        if (Platform.isAndroid) {
          args.add({
            'listenTo' : 'onLaunch',
            'code': message['data']['code'] ?? 'no-data',
            'title': message['data']['title'] ?? 'no-data',
            'body': message['data']['body'] ?? 'no-data',
            'notificationId': message['data']['notificationId'] ?? 'no-data'
          });
        } else {
          args.add({
            'listenTo' : 'onLaunch',
            'code': message['code'] ?? 'no-data',
            'title': message['title'] ?? 'no-data',
            'body': message['body'] ?? 'no-data',
            'notificationId': message['notificationId'] ?? 'no-data'
          });
        }

        _mensajeStreamController.sink.add(args);
      },
      onResume: (message) async {
        List args = [];
        if (Platform.isAndroid) {
          args.add({
            'listenTo' : 'onResume',
            'code': message['data']['code'] ?? 'no-data',
            'title': message['data']['title'] ?? 'no-data',
            'body': message['data']['body'] ?? 'no-data',
            'notificationId': message['data']['notificationId'] ?? 'no-data'
          });
        } else {
          args.add({
            'listenTo' : 'onResume',
            'code': message['code'] ?? 'no-data',
            'title': message['title'] ?? 'no-data',
            'body': message['body'] ?? 'no-data',
            'notificationId': message['notificationId'] ?? 'no-data'
          });
        }

        _mensajeStreamController.sink.add(args);
      },
    );
  }

  dispose() {
    _mensajeStreamController?.close();
  }
}
