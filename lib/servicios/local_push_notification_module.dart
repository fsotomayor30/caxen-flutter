import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


class   LocalPushNotificationModule {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  initLocalPush() async {
    var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: _onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _selectNotification);
  }

  showNotification(BuildContext context, int code,String title, String body, String payload) async {
    var android = AndroidNotificationDetails('ID', 'NAME', 'DESCRIPTION');
    var ios = IOSNotificationDetails();
    var platform = NotificationDetails(android, ios);
    await flutterLocalNotificationsPlugin.show(0, title, body, platform, payload: payload == null ? 'payload' : payload);
  }

  Future _selectNotification(String payload) async {
    /*final context = KeyApp.keyAppNavigator.currentState.overlay.context;

    switch(payload) {
      case REQUEST_CONTACT:

        StoreProvider.of<AppState>(context).dispatch(UpdateIndexAction(index: 2));
        StoreProvider.of<AppState>(context).dispatch(UpdateContentAction(content: UserContactTabBarScreen(initialIndex: 2)));
        StoreProvider.of<AppState>(context).dispatch(UpdateTitleAction(title: "Contactos"));

        break;
      case ACCEPT_REQUEST:

        StoreProvider.of<AppState>(context).dispatch(UpdateIndexAction(index: 2));
        StoreProvider.of<AppState>(context).dispatch(UpdateContentAction(content: UserContactTabBarScreen(initialIndex: 0)));
        StoreProvider.of<AppState>(context).dispatch(UpdateTitleAction(title: "Contactos"));

        break;
      case REJECT_REQUEST:

        StoreProvider.of<AppState>(context).dispatch(UpdateIndexAction(index: 2));
        StoreProvider.of<AppState>(context).dispatch(UpdateContentAction(content: UserContactTabBarScreen(initialIndex: 2)));
        StoreProvider.of<AppState>(context).dispatch(UpdateTitleAction(title: "Contactos"));

        break;
      case PAYMENTS_APPROVED:
        //TODO: AGREGAR ACCION
        break;
      case PAYMENTS_REJECTED:

        //TODO: AGREGAR ACCION
        break;
      case REQUEST_DOCUMENT:

        //TODO: AGREGAR ACCION
        break;
      case ACCEPT_DOCUMENT:

        //TODO: AGREGAR ACCION
        break;
      case REJECT_DOCUMENT:

        //TODO: AGREGAR ACCION
        break;
      case SINNOTARIA_SERVICE:

        //TODO: AGREGAR ACCION
        break;
      case EDIT_DOCUMENT:

        //TODO: AGREGAR ACCION
        break;
      case FINISHED_DOCUMENT:

        //TODO: AGREGAR ACCION
        break;
      case REQUEST_FINAL_DOCUMENT:

      //TODO: AGREGAR ACCION
        break;
      case ACCEPT_FINAL_DOCUMENT:

      //TODO: AGREGAR ACCION
        break;
      case REJECT_FINAL_DOCUMENT:

      //TODO: AGREGAR ACCION
        break;
      case PREVIEW_DOCUMENT:

      //TODO: AGREGAR ACCION
        break;

    }*/
  }

  Future _onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    print('OnDid');
    print('$id $title $body $payload');
  }
}
