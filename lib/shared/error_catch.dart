String errorCatch(String code) {
  String error;
  switch (code) {
    case "ERROR_WRONG_PASSWORD":
      error = "Contraseña Incorrecta";
      break;
    case "ERROR_USER_NOT_FOUND":
      error = "Correo Incorrecto";
      break;
    case "ERROR_NETWORK_REQUEST_FAILED":
      error = "Necesitas estar conectado a internet para validar tu correo";
      break;
    case "network_error":
      error = "Necesitas estar conectado a internet para validar tu correo";
      break;
    case "ERROR_EMAIL_ALREADY_IN_USE":
      error = "Este correo ya esta registrado en Caxen";
      break;
  }

  return error;
}
