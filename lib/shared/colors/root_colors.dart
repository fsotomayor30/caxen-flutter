import 'package:flutter/material.dart';

const Color EMPRENDIMIENTOS_COLOR = Color(0xFFFF7043);
const Color FREELANCES_COLOR = Color(0xFFAB47BC);
const Color OFICIOS_COLOR = Color(0xFF1565c0);
