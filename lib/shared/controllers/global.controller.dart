import 'package:get/get.dart';

import '../../modelos/usuario.dart';

class GlobalController extends GetxController {
  Usuario _usuario;
  Usuario get usuario => _usuario;

  setUsuario(Usuario usuario) {
    _usuario = usuario;
    update();
  }
}
