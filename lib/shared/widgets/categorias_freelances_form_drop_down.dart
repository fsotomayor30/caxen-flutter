import 'package:flutter/material.dart';

class CategoriaFreelancesFormDropDown extends StatefulWidget {
  final Function onChanged;
  final String initValue;

  CategoriaFreelancesFormDropDown(
      {@required this.onChanged, @required this.initValue});

  @override
  _CategoriaFreelancesFormDropDownState createState() =>
      _CategoriaFreelancesFormDropDownState();
}

class _CategoriaFreelancesFormDropDownState
    extends State<CategoriaFreelancesFormDropDown> {
  String hint = 'Categoría';
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    dropdownValue = (widget.initValue == null)
        ? 'Mercadeo digital y Diseño gráfico'
        : (widget.initValue == "Todas")
            ? "Mercadeo digital y Diseño gráfico"
            : widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DropdownButton(
        isExpanded: true,
        items: <String>[
          'Mercadeo digital y Diseño gráfico',
          'Asesoría legal y generales',
          'Audiovisual',
          'Contabilidad, Admin y RRHH',
          'Redacción y Contenido',
          'Informática y Desarrollo',
          'Traducciones, Cursos y Talleres',
          'Corredor de propiedades',
          'Sicólogos, terapeutas y Coach',
          'Arquitectos',
          'Otros',
        ].map<DropdownMenuItem<String>>(
          (String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          },
        ).toList(),
        value: dropdownValue,
        onChanged: (String newValue) {
          this.widget.onChanged(newValue);

          setState(() {
            dropdownValue = newValue;
            this.hint = newValue;
          });
        },
      ),
    );
  }
}
