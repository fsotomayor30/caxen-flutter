import 'package:flutter/material.dart';

class CategoriaOficiosFormDropDown extends StatefulWidget {
  final Function onChanged;
  final String initValue;

  CategoriaOficiosFormDropDown(
      {@required this.onChanged, @required this.initValue});

  @override
  _CategoriaOficiosFormDropDownState createState() =>
      _CategoriaOficiosFormDropDownState();
}

class _CategoriaOficiosFormDropDownState
    extends State<CategoriaOficiosFormDropDown> {
  String hint = 'Categoría';
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    dropdownValue = (widget.initValue == null)
        ? 'Mercadeo digital y Diseño gráfico'
        : (widget.initValue == "Todas")
            ? "Shows, bandas y cantantes"
            : widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DropdownButton(
        isExpanded: true,
        items: <String>[
          'Shows, bandas y cantantes',
          'Electricistas, Albañiles y Pintores',
          'Servicio Técnico',
          'Mecánicos automotrices',
          'Gasfiter',
          'Asesoras del hogar y Limpieza',
          'Mueblistas, Tapiceros y Cerrajeros',
          'Cocineros y Ayudantes',
          'Estilistas, peluqueros y afines',
          'Joyeros y Orfebres',
          'Tarot y Astrología',
          'Sastres, Modistas y Zapateros',
          'Enfermeras y Cuidadores de adultos y niños',
          'Otros',
        ].map<DropdownMenuItem<String>>(
          (String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          },
        ).toList(),
        value: dropdownValue,
        onChanged: (String newValue) {
          this.widget.onChanged(newValue);

          setState(() {
            dropdownValue = newValue;
            this.hint = newValue;
          });
        },
      ),
    );
  }
}
