import 'package:flutter/material.dart';

class RegionDropDown extends StatefulWidget {
  final Function onChanged;

  RegionDropDown({@required this.onChanged});

  @override
  _RegionDropDownState createState() => _RegionDropDownState();
}

class _RegionDropDownState extends State<RegionDropDown> {
  String hint = 'Región';
  String dropdownValue = 'Todas';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DropdownButton(
          value: dropdownValue,
          hint: Text(this.hint),
          isExpanded: true,
          items: <String>[
            'Todas',
            'Región de Arica y Parinacota',
            'Región de Tarapacá',
            'Región de Antofagasta',
            'Región de Atacama',
            'Región de Coquimbo',
            'Región de Valparaíso',
            'Región Metropolitana de Santiago',
            'Región del Libertador General Bernardo O`Higgins',
            'Región del Maule',
            'Región de Ñuble',
            'Región del Biobío',
            'Región de La Araucanía',
            'Región de Los Ríos',
            'Región de Los Lagos',
            'Región de Aysén del General Carlos Ibáñez del Campo',
            'Región de Magallanes y de la Antártica Chilena'
          ].map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value, overflow: TextOverflow.ellipsis),
            );
          }).toList(),
          onChanged: (String newValue) {
            this.widget.onChanged(newValue);

            setState(() {
              dropdownValue = newValue;
              this.hint = newValue;
            });
          }),
    );
  }
}
