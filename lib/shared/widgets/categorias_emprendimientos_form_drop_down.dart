import 'package:flutter/material.dart';

class CategoriaEmprendimientosFormDropDown extends StatefulWidget {
  final Function onChanged;
  final String initValue;

  CategoriaEmprendimientosFormDropDown(
      {@required this.onChanged, @required this.initValue});

  @override
  _CategoriaEmprendimientosFormDropDownState createState() =>
      _CategoriaEmprendimientosFormDropDownState();
}

class _CategoriaEmprendimientosFormDropDownState
    extends State<CategoriaEmprendimientosFormDropDown> {
  String hint = 'Categoría';
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    dropdownValue = (widget.initValue == null)
        ? 'Mascotas'
        : (widget.initValue == "Todas")
            ? "Mascotas"
            : widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DropdownButton(
        isExpanded: true,
        items: <String>[
          'Mascotas',
          'Deportes, fitness y outdoor',
          'Accesorios y Moda',
          'Salud y Belleza',
          'Vestuario y Calzado',
          'Hogar y Diseño',
          'Construcción y Ferretería',
          'Infantil',
          'Alimentos y Bebidas',
          'Tecnología',
          'Entretención y Cultura',
          'Viajes y Turismo',
          'Equipajes, Bolsos y Mochilas',
          'Servicios',
          'Arte, Decoración y Artesanía',
          'Vehículos y Accesorios',
          'Bares y Restaurantes',
          'Libros y papelería',
          'Transportistas y Mudanzas',
          'Otros',
        ].map<DropdownMenuItem<String>>(
          (String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          },
        ).toList(),
        value: dropdownValue,
        onChanged: (String newValue) {
          this.widget.onChanged(newValue);

          setState(() {
            dropdownValue = newValue;
            this.hint = newValue;
          });
        },
      ),
    );
  }
}
