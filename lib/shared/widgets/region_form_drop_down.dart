import 'package:flutter/material.dart';

class RegionFormDropDown extends StatefulWidget {
  final Function onChanged;
  final String initValue;

  RegionFormDropDown({@required this.onChanged, @required this.initValue});

  @override
  _RegionFormDropDownState createState() => _RegionFormDropDownState();
}

class _RegionFormDropDownState extends State<RegionFormDropDown> {
  String hint = 'Región';
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    dropdownValue = (widget.initValue == null)
        ? 'Región de Arica y Parinacota'
        : (widget.initValue == "Todas")
            ? "Región de Arica y Parinacota"
            : widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DropdownButton(
        isExpanded: true,
        items: <String>[
          'Región de Arica y Parinacota',
          'Región de Tarapacá',
          'Región de Antofagasta',
          'Región de Atacama',
          'Región de Coquimbo',
          'Región de Valparaíso',
          'Región Metropolitana de Santiago',
          'Región del Libertador General Bernardo O`Higgins',
          'Región del Maule',
          'Región de Ñuble',
          'Región del Biobío',
          'Región de La Araucanía',
          'Región de Los Ríos',
          'Región de Los Lagos',
          'Región de Aysén del General Carlos Ibáñez del Campo',
          'Región de Magallanes y de la Antártica Chilena'
        ].map<DropdownMenuItem<String>>(
          (String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value, overflow: TextOverflow.ellipsis),
            );
          },
        ).toList(),
        value: dropdownValue,
        onChanged: (String newValue) {
          this.widget.onChanged(newValue);
          //logger.i("[REGION CHANGED] "+newValue);
          setState(() {
            dropdownValue = newValue;
            this.hint = newValue;
          });
        },
      ),
    );
  }
}
