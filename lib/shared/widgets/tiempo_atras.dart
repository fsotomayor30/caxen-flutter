import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TiempoAtrasWidget extends StatefulWidget {
  final String fecha;

  TiempoAtrasWidget({@required this.fecha});

  @override
  _TiempoAtrasWidgetState createState() => _TiempoAtrasWidgetState();
}

class _TiempoAtrasWidgetState extends State<TiempoAtrasWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget fechaPublicacion;
    int ahora = Timestamp.now().millisecondsSinceEpoch;
    int emprendimiento = int.parse(widget.fecha);

    int diferencia = ahora - emprendimiento;
    int segundos = (diferencia / 1000).round();
    int minutos = (segundos / 60).round();
    int horas = (minutos / 60).round();
    int dias = (horas / 60).round();

    if (segundos < 60) {
      fechaPublicacion = Text(
        segundos.toString() + " segundos atrás",
        style: TextStyle(fontSize: 10),
      );
    } else {
      if (minutos < 60) {
        fechaPublicacion = Text(
          minutos.toString() + " minutos atrás",
          style: TextStyle(fontSize: 10),
        );
      } else {
        if (horas <= 24) {
          fechaPublicacion = Text(
            horas.toString() + " horas atrás",
            style: TextStyle(fontSize: 10),
          );
        } else {
          fechaPublicacion = Text(
            dias.toString() + " dias atrás",
            style: TextStyle(fontSize: 10),
          );
        }
      }
    }
    return fechaPublicacion;
  }
}
