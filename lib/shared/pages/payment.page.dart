import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PaymentScreen extends StatefulWidget {
  final Color color;
  final String title;

  PaymentScreen({@required this.color, @required this.title});

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<PaymentScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: widget.color,
          centerTitle: true,
          title: Text(
            widget.title,
            style: TextStyle(fontSize: 15),
          ),
        ),
        body: Container(
            width: mediaQueryData.width,
            height: 700,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CarouselSlider(
                    options: CarouselOptions(
                      autoPlay: true,
                      //aspectRatio: 1.0,
                      height: mediaQueryData.height - 150,
                      //viewportFraction: 1.0,
                      enlargeCenterPage: true,
                    ),
                    items: imageSliders,
                  ),
                ],
              ),
            )));
  }

  final List<Widget> imageSliders = [
    Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Container(
        width: 300,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        margin: EdgeInsets.all(5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Text(
              'CAXEN  X2',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
                fontFamily: 'BalooDa2-Medium',
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Invierte \$5.000 (+iva \$950)',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'BalooDa2-Medium',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('y'),
            SizedBox(
              height: 10,
            ),
            Text('Caxen invertirá \$10.000 (+iva \$1900)',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            SizedBox(
              height: 20,
            ),
            Text('en publicidad para ti en INSTAGRAM',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            Expanded(child: Container()),
            Builder(
              builder: (context) => GestureDetector(
                onTap: () async {
                  var whatsappUrl;
                  if (Platform.isIOS) {
                    whatsappUrl = "whatsapp://wa.me/+56937024625";
                  } else {
                    whatsappUrl = "whatsapp://send?phone=+56937024625";
                  }

                  if (await canLaunch(whatsappUrl)) {
                    await launch(whatsappUrl);
                  } else {
                    final snackBar = SnackBar(
                      content:
                          Text("Error: Necesitas tener instalado Whatsapp"),
                    );

                    Scaffold.of(context).showSnackBar(snackBar);
                  }
                },
                child: Container(
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                          //topLeft: Radius.circular(10),
                          //topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Más información',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'BalooDa2-Medium',
                        ),
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Container(
        width: 300,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        margin: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Text(
              'CAXEN MÁS',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
                fontFamily: 'BalooDa2-Medium',
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Invierte desde \$3.000 (+iva \$570)',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'BalooDa2-Medium',
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text('y',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            SizedBox(
              height: 20,
            ),
            Text('Caxen destacará tu publicación',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            SizedBox(
              height: 20,
            ),
            Text('en la App',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            Expanded(child: Container()),
            Builder(
              builder: (context) => GestureDetector(
                onTap: () async {
                  var whatsappUrl;
                  if (Platform.isIOS) {
                    whatsappUrl = "whatsapp://wa.me/+56937024625";
                  } else {
                    whatsappUrl = "whatsapp://send?phone=+56937024625";
                  }

                  if (await canLaunch(whatsappUrl)) {
                    await launch(whatsappUrl);
                  } else {
                    final snackBar = SnackBar(
                      content:
                          Text("Error: Necesitas tener instalado Whatsapp"),
                    );

                    Scaffold.of(context).showSnackBar(snackBar);
                  }
                },
                child: Container(
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                          //topLeft: Radius.circular(10),
                          //topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Más información',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'BalooDa2-Medium',
                        ),
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    ),
    Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Container(
        width: 300,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        margin: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Text(
              'CAXEN PLUS',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
                fontFamily: 'BalooDa2-Medium',
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text('Invierte desde \$9.000 (iva incluido)',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            SizedBox(
              height: 20,
            ),
            Text('y',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            SizedBox(
              height: 20,
            ),
            Text('obtendrás CAXEN X2/PLUS',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'BalooDa2-Medium',
                )),
            Expanded(child: Container()),
            Builder(
              builder: (context) => GestureDetector(
                onTap: () async {
                  var whatsappUrl;
                  if (Platform.isIOS) {
                    whatsappUrl = "whatsapp://wa.me/+56937024625";
                  } else {
                    whatsappUrl = "whatsapp://send?phone=+56937024625";
                  }

                  if (await canLaunch(whatsappUrl)) {
                    await launch(whatsappUrl);
                  } else {
                    final snackBar = SnackBar(
                      content:
                          Text("Error: Necesitas tener instalado Whatsapp"),
                    );

                    Scaffold.of(context).showSnackBar(snackBar);
                  }
                },
                child: Container(
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                          //topLeft: Radius.circular(10),
                          //topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Más información',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'BalooDa2-Medium',
                        ),
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    )
  ];
}
