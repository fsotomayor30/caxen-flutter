import 'dart:async';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

class TakePictureScreen extends StatefulWidget {
  final Function onChanged;
  final String orientation;
  final String message;
  final Color color;
  TakePictureScreen(
      {@required this.onChanged,
      @required this.orientation,
      @required this.message,
      @required this.color});

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  bool isCameraReady = false;

  int cameraSelected = 1;

  @override
  void initState() {
    super.initState();
    _initializeCamera();
  }

  Future<void> _initializeCamera() async {
    final cameras = await availableCameras();
    final firstCamera = cameras[cameraSelected];
    _controller = CameraController(firstCamera, ResolutionPreset.high);
    _initializeControllerFuture = _controller.initialize();
    if (!mounted) {
      return;
    }
    setState(() {
      isCameraReady = true;
    });
  }

  Future<void> _changeCamera() async {
    setState(() {
      isCameraReady = false;
    });

    final cameras = await availableCameras();
    if (cameraSelected == 0) {
      setState(() {
        cameraSelected = 1;
      });
    } else {
      if (cameraSelected == 1) {
        setState(() {
          cameraSelected = 0;
        });
      }
    }

    final firstCamera = cameras[cameraSelected];

    _controller = CameraController(firstCamera, ResolutionPreset.high);
    _initializeControllerFuture = _controller.initialize();
    if (!mounted) {
      return;
    }
    setState(() {
      isCameraReady = true;
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: FutureBuilder<void>(
              future: _initializeControllerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return ClipRect(
                      child: Container(
                    child: Transform.scale(
                      scale: _controller.value.aspectRatio / size.aspectRatio,
                      child: Center(
                        child: AspectRatio(
                          aspectRatio: _controller.value.aspectRatio,
                          child: CameraPreview(_controller),
                        ),
                      ),
                    ),
                  ));
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ),
          widget.orientation == 'vertical'
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Text(widget.message,
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black87.withOpacity(.3))),
                  ),
                )
              : RotatedBox(
                  quarterTurns: 3,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Wrap(
                      direction: Axis.horizontal,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(30.0),
                          child: Text(
                            widget.message,
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black87.withOpacity(.3)),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
          widget.orientation == 'vertical'
              ? Positioned(
                  right: 20,
                  bottom: 200,
                  child: GestureDetector(
                    onTap: () async {
                      _changeCamera();
                    },
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: widget.color,
                      ),
                      child: Center(
                          child: Icon(
                        Icons.cached,
                        size: 60,
                        color: Colors.white,
                      )),
                    ),
                  ),
                )
              : Positioned(
                  right: 140,
                  top: 50,
                  child: GestureDetector(
                    onTap: () async {
                      _changeCamera();
                    },
                    child: RotationTransition(
                      turns: new AlwaysStoppedAnimation(-90 / 360),
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          color: widget.color,
                        ),
                        child: Center(
                            child: Icon(
                          Icons.cached,
                          size: 60,
                          color: Colors.white,
                        )),
                      ),
                    ),
                  ),
                ),
          widget.orientation == 'vertical'
              ? Positioned(
                  right: 20,
                  bottom: 90,
                  child: GestureDetector(
                    onTap: () async {
                      try {
                        await _initializeControllerFuture;
                        final path = join(
                          (await getTemporaryDirectory()).path,
                          '${DateTime.now()}.png',
                        );
                        await _controller.takePicture(path);
                        this.widget.onChanged(path);
                        Navigator.of(context).pop();
                      } catch (e) {
                        print(e);
                      }
                    },
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        color: widget.color,
                      ),
                      child: Center(
                          child: Icon(
                        Icons.camera_alt,
                        size: 60,
                        color: Colors.white,
                      )),
                    ),
                  ),
                )
              : Positioned(
                  right: 20,
                  top: 50,
                  child: GestureDetector(
                    onTap: () async {
                      try {
                        await _initializeControllerFuture;
                        final path = join(
                          (await getTemporaryDirectory()).path,
                          '${DateTime.now()}.png',
                        );
                        await _controller.takePicture(path);
                        this.widget.onChanged(path);
                        Navigator.of(context).pop();
                      } catch (e) {
                        print(e);
                      }
                    },
                    child: RotationTransition(
                      turns: new AlwaysStoppedAnimation(-90 / 360),
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          color: widget.color,
                        ),
                        child: Center(
                            child: Icon(
                          Icons.camera_alt,
                          size: 60,
                          color: Colors.white,
                        )),
                      ),
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
